$(document).ready( function () {
	$('#issueCategoryTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		deferRender:true,
		ajax: {
			type: 'GET',
			url: '/issue/data',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'name', name: 'name',searchable:true,orderable:true},
			{data: 'description', name: 'description',searchable:true,orderable:true},
			{data: 'type', name: 'type',searchable:true,orderable:true},
			{data: 'dept_name', name: 'dept_name',searchable:true,orderable:true},
			{data: 'sub_dept_name', name: 'sub_dept_name',searchable:true,orderable:true},
			{data: 'action', name: 'action',searchable:true,orderable:true},
		]
	});

	var dtable = $('#issueCategoryTable').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();
    $('#createButton').click(function () {
        $.ajax({
            type: 'GET',
            url: '/issue/dataDepartment'
        })
        .done(function(response) {
            console.log(response);
            var issue = response.department;
            console.log(issue);
            var department = $('#select_department');
            department.empty();
            department.append('<option value="">--Pilih Department--</option>');
            console.log('berhasil append pertama');
            console.log(response.department.length);
            for (var i = 0; i < issue.length; i++){
                console.log(i);
                console.log(issue[i]);
                department.append('<option value="'+issue[i].id+'">'+issue[i].dept_name+'</option>');
            }
        })
    });
    $('#select_department').on('change',function () {
        var id = $('#select_department').val();
        console.log('id: '+id);
        $.ajax({
            type: 'GET',
            url: '/issue/dataSubDepartment/'+id
        })
        .done(function(data) {
            console.log(data);
            var issue = data.sub_department;
            console.log(issue);
            var department = $('#select_sub_department');
            department.empty();
            department.append('<option value="">--Pilih Department--</option>');
            console.log('berhasil append pertama');
            console.log(data.sub_department.length);
            for (var i = 0; i < issue.length; i++){
                console.log(i);
                console.log(issue[i]);
                department.append('<option value="'+issue[i].id+'">'+issue[i].sub_dept_name+'</option>');
            }
        })
    });

    $('#update_department').on('change',function () {
        var id = $('#update_department').val();
        console.log('id: '+id);
        $.ajax({
            type: 'GET',
            url: '/issue/dataSubDepartment/'+id
        })
        .done(function(data) {
            console.log(data);
            var issue = data.sub_department;
            console.log(issue);
            var department = $('#update_sub_department');
            department.empty();
            department.append('<option value="">--Pilih Department--</option>');
            console.log('berhasil append pertama');
            console.log(data.sub_department.length);
            for (var i = 0; i < issue.length; i++){
                console.log(i);
                console.log(issue[i]);
                department.append('<option value="'+issue[i].id+'">'+issue[i].sub_dept_name+'</option>');
            }
        })
    });
	$('#insert_issue').submit(function (event){
		event.preventDefault();
		var name = $('#name').val();

		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}
        console.log($('#insert_issue').serialize());
		$('#insertIssueModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_issue').attr('action'),
					data: $('#insert_issue').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#insertPermissionModal').modal('hide');
						$('#insert_issue').trigger("reset");
						$('#issueCategoryTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#insertPermissionModal').modal();
					}
				});
			}
		});
	});

	$('#update_issue').submit(function (event){
		event.preventDefault();
		var name = $('#update_name').val();

		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}

		$('#updateIssueModal').modal('hide');
        console.log($('#update_issue').serialize());
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "PUT",
					url: $('#update_issue').attr('action'),
					data: $('#update_issue').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#updateIssueModal').modal('hide');
						$('#update_issue').trigger("reset");
						$('#issueCategoryTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#updateIssueModal').modal();
					}
				});
			}
		});
	});

});

function edit(url)
{
	$.ajax({
		type: "get",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();

		}
	})
	.done(function (response) {
        $.ajax({
            type: 'GET',
            url: '/issue/dataDepartment'
        })
        .done(function(data) {
            // console.log(data);
            console.log('department'+response.department);
            var issue = data.department;
            console.log(issue);
            var department = $('#update_department');
            department.empty();
            department.append('<option value="">--Pilih Department--</option>');
            console.log('berhasil append pertama');
            console.log(data.department.length);
            for (var i = 0; i < issue.length; i++){
                console.log(i);
                console.log(issue[i]);
                if(response.department == issue[i].id){
                    console.log('Masuk if');
                    department.append('<option value="'+issue[i].id+'" selected="selected">'+issue[i].dept_name+'</option>');
                }
                else{
                    console.log('Masuk Else');
                    department.append('<option value="'+issue[i].id+'">'+issue[i].dept_name+'</option>');
                }
            }
        })
        var id_sub = response.department;
        $.ajax({
            type: 'GET',
            url: '/issue/dataSubDepartment/'+id_sub
        })
        .done(function(subdata) {
            console.log('masuk sub data');
            console.log(subdata);
            // console.log('department'+response.department);
            var issue = subdata.sub_department;
            console.log(issue);
            var department = $('#update_sub_department');
            department.empty();
            department.append('<option value="">--Pilih Sub Department--</option>');
            console.log('berhasil append pertama');
            console.log(issue.length);
            for (var i = 0; i < issue.length; i++){
                console.log(i);
                console.log(issue[i]);
                if(response.sub_department == issue[i].id){
                    console.log('Masuk if');
                    department.append('<option value="'+issue[i].id+'" selected="selected">'+issue[i].sub_dept_name+'</option>');
                }
                else{
                    console.log('Masuk Else');
                    department.append('<option value="'+issue[i].id+'">'+issue[i].sub_dept_name+'</option>');
                }
            }
        })
		$('#update_issue').attr('action', response.url_update);
		$('#update_name').val(response.name);
		$('#update_description').val(response.description);
		$('#update_type').val(response.type).trigger('change');
		$('#updateIssueModal').modal();


	});
}

function hapus(url)
{
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: "delete",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function (response) {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
		}
	}).done(function ($result) {
		$('#issueCategoryTable').DataTable().ajax.reload();
		$("#alert_success").trigger("click", 'Data Berhasil hapus');
	});
}
