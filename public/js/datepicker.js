/* =========================================================
 * bootstrap-datepicker.js
 * Repo: https://github.com/eternicode/bootstrap-datepicker/
 * Demo: http://eternicode.github.io/bootstrap-datepicker/
 * Docs: http://bootstrap-datepicker.readthedocs.org/
 * Forked from http://www.eyecon.ro/bootstrap-datepicker
 * =========================================================
 * Started by Stefan Petre; improvements by Andrew Rowls + contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */

(function($, undefined){

	var $window = $(window);

	function UTCDate(){
		return new Date(Date.UTC.apply(Date, arguments));
	}
	function UTCToday(){
		var today = new Date();
		return UTCDate(today.getFullYear(), today.getMonth(), today.getDate());
	}
	function alias(method){
		return function(){
			return this[method].apply(this, arguments);
		};
	}

	var DateArray = (function(){
		var extras = {
			get: function(i){
				return this.slice(i)[0];
			},
			contains: function(d){
				// Array.indexOf is not cross-browser;
				// $.inArray doesn't work with Dates
				var val = d && d.valueOf();
				for (var i=0, l=this.length; i < l; i++)
					if (this[i].valueOf() === val)
						return i;
				return -1;
			},
			remove: function(i){
				this.splice(i,1);
			},
			replace: function(new_array){
				if (!new_array)
					return;
				if (!$.isArray(new_array))
					new_array = [new_array];
				this.clear();
				this.push.apply(this, new_array);
			},
			clear: function(){
				this.splice(0);
			},
			copy: function(){
				var a = new DateArray();
				a.replace(this);
				return a;
			}
		};

		return function(){
			var a = [];
			a.push.apply(a, arguments);
			$.extend(a, extras);
			return a;
		};
	})();


	// Picker object

	var Datepicker = function(element, options){
		this.dates = new DateArray();
		this.viewDate = UTCToday();
		this.focusDate = null;

		this._process_options(options);

		this.element = $(element);
		this.isInline = false;
		this.isInput = this.element.is('input');
		this.component = this.element.is('.date') ? this.element.find('.add-on, .input-group-addon, .btn') : false;
		this.hasInput = this.component && this.element.find('input').length;
		if (this.component && this.component.length === 0)
			this.component = false;

		this.picker = $(DPGlobal.template);
		this._buildEvents();
		this._attachEvents();

		if (this.isInline){
			this.picker.addClass('datepicker-inline').appendTo(this.element);
		}
		else {
			this.picker.addClass('datepicker-dropdown dropdown-menu');
		}

		if (this.o.rtl){
			this.picker.addClass('datepicker-rtl');
		}

		this.viewMode = this.o.startView;

		if (this.o.calendarWeeks)
			this.picker.find('tfoot th.today')
						.attr('colspan', function(i, val){
							return parseInt(val) + 1;
						});

		this._allow_update = false;

		this.setStartDate(this._o.startDate);
		this.setEndDate(this._o.endDate);
		this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled);

		this.fillDow();
		this.fillMonths();

		this._allow_update = true;

		this.update();
		this.showMode();

		if (this.isInline){
			this.show();
		}
	};

	Datepicker.prototype = {
		constructor: Datepicker,

		_process_options: function(opts){
			// Store raw options for reference
			this._o = $.extend({}, this._o, opts);
			// Processed options
			var o = this.o = $.extend({}, this._o);

			// Check if "de-DE" style date is available, if not language should
			// fallback to 2 letter code eg "de"
			var lang = o.language;
			if (!dates[lang]){
				lang = lang.split('-')[0];
				if (!dates[lang])
					lang = defaults.language;
			}
			o.language = lang;

			switch (o.startView){
				case 2:
				case 'decade':
					o.startView = 2;
					break;
				case 1:
				case 'year':
					o.startView = 1;
					break;
				default:
					o.startView = 0;
			}

			switch (o.minViewMode){
				case 1:
				case 'months':
					o.minViewMode = 1;
					break;
				case 2:
				case 'years':
					o.minViewMode = 2;
					break;
				default:
					o.minViewMode = 0;
			}

			o.startView = Math.max(o.startView, o.minViewMode);

			// true, false, or Number > 0
			if (o.multidate !== true){
				o.multidate = Number(o.multidate) || false;
				if (o.multidate !== false)
					o.multidate = Math.max(0, o.multidate);
				else
					o.multidate = 1;
			}
			o.multidateSeparator = String(o.multidateSeparator);

			o.weekStart %= 7;
			o.weekEnd = ((o.weekStart + 6) % 7);

			var format = DPGlobal.parseFormat(o.format);
			if (o.startDate !== -Infinity){
				if (!!o.startDate){
					if (o.startDate instanceof Date)
						o.startDate = this._local_to_utc(this._zero_time(o.startDate));
					else
						o.startDate = DPGlobal.parseDate(o.startDate, format, o.language);
				}
				else {
					o.startDate = -Infinity;
				}
			}
			if (o.endDate !== Infinity){
				if (!!o.endDate){
					if (o.endDate instanceof Date)
						o.endDate = this._local_to_utc(this._zero_time(o.endDate));
					else
						o.endDate = DPGlobal.parseDate(o.endDate, format, o.language);
				}
				else {
					o.endDate = Infinity;
				}
			}

			o.daysOfWeekDisabled = o.daysOfWeekDisabled||[];
			if (!$.isArray(o.daysOfWeekDisabled))
				o.daysOfWeekDisabled = o.daysOfWeekDisabled.split(/[,\s]*/);
			o.daysOfWeekDisabled = $.map(o.daysOfWeekDisabled, function(d){
				return parseInt(d, 10);
			});

			var plc = String(o.orientation).toLowerCase().split(/\s+/g),
				_plc = o.orientation.toLowerCase();
			plc = $.grep(plc, function(word){
				return (/^auto|left|right|top|bottom$/).test(word);
			});
			o.orientation = {x: 'auto', y: 'auto'};
			if (!_plc || _plc === 'auto')
				; // no action
			else if (plc.length === 1){
				switch (plc[0]){
					case 'top':
					case 'bottom':
						o.orientation.y = plc[0];
						break;
					case 'left':
					case 'right':
						o.orientation.x = plc[0];
						break;
				}
			}
			else {
				_plc = $.grep(plc, function(word){
					return (/^left|right$/).test(word);
				});
				o.orientation.x = _plc[0] || 'auto';

				_plc = $.grep(plc, function(word){
					return (/^top|bottom$/).test(word);
				});
				o.orientation.y = _plc[0] || 'auto';
			}
		},
		_events: [],
		_secondaryEvents: [],
		_applyEvents: function(evs){
			for (var i=0, el, ch, ev; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				}
				else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.on(ev, ch);
			}
		},
		_unapplyEvents: function(evs){
			for (var i=0, el, ev, ch; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				}
				else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.off(ev, ch);
			}
		},
		_buildEvents: function(){
			if (this.isInput){ // single input
				this._events = [
					[this.element, {
						focus: $.proxy(this.show, this),
						keyup: $.proxy(function(e){
							if ($.inArray(e.keyCode, [27,37,39,38,40,32,13,9]) === -1)
								this.update();
						}, this),
						keydown: $.proxy(this.keydown, this)
					}]
				];
			}
			else if (this.component && this.hasInput){ // component: input + button
				this._events = [
					// For components that are not readonly, allow keyboard nav
					[this.element.find('input'), {
						focus: $.proxy(this.show, this),
						keyup: $.proxy(function(e){
							if ($.inArray(e.keyCode, [27,37,39,38,40,32,13,9]) === -1)
								this.update();
						}, this),
						keydown: $.proxy(this.keydown, this)
					}],
					[this.component, {
						click: $.proxy(this.show, this)
					}]
				];
			}
			else if (this.element.is('div')){  // inline datepicker
				this.isInline = true;
			}
			else {
				this._events = [
					[this.element, {
						click: $.proxy(this.show, this)
					}]
				];
			}
			this._events.push(
				// Component: listen for blur on element descendants
				[this.element, '*', {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}],
				// Input: listen for blur on element
				[this.element, {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}]
			);

			this._secondaryEvents = [
				[this.picker, {
					click: $.proxy(this.click, this)
				}],
				[$(window), {
					resize: $.proxy(this.place, this)
				}],
				[$(document), {
					'mousedown touchstart': $.proxy(function(e){
						// Clicked outside the datepicker, hide it
						if (!(
							this.element.is(e.target) ||
							this.element.find(e.target).length ||
							this.picker.is(e.target) ||
							this.picker.find(e.target).length
						)){
							this.hide();
						}
					}, this)
				}]
			];
		},
		_attachEvents: function(){
			this._detachEvents();
			this._applyEvents(this._events);
		},
		_detachEvents: function(){
			this._unapplyEvents(this._events);
		},
		_attachSecondaryEvents: function(){
			this._detachSecondaryEvents();
			this._applyEvents(this._secondaryEvents);
		},
		_detachSecondaryEvents: function(){
			this._unapplyEvents(this._secondaryEvents);
		},
		_trigger: function(event, altdate){
			var date = altdate || this.dates.get(-1),
				local_date = this._utc_to_local(date);

			this.element.trigger({
				type: event,
				date: local_date,
				dates: $.map(this.dates, this._utc_to_local),
				format: $.proxy(function(ix, format){
					if (arguments.length === 0){
						ix = this.dates.length - 1;
						format = this.o.format;
					}
					else if (typeof ix === 'string'){
						format = ix;
						ix = this.dates.length - 1;
					}
					format = format || this.o.format;
					var date = this.dates.get(ix);
					return DPGlobal.formatDate(date, format, this.o.language);
				}, this)
			});
		},

		show: function(){
			if (!this.isInline)
				this.picker.appendTo('body');
			this.picker.show();
			this.place();
			this._attachSecondaryEvents();
			this._trigger('show');
		},

		hide: function(){
			if (this.isInline)
				return;
			if (!this.picker.is(':visible'))
				return;
			this.focusDate = null;
			this.picker.hide().detach();
			this._detachSecondaryEvents();
			this.viewMode = this.o.startView;
			this.showMode();

			if (
				this.o.forceParse &&
				(
					this.isInput && this.element.val() ||
					this.hasInput && this.element.find('input').val()
				)
			)
				this.setValue();
			this._trigger('hide');
		},

		remove: function(){
			this.hide();
			this._detachEvents();
			this._detachSecondaryEvents();
			this.picker.remove();
			delete this.element.data().datepicker;
			if (!this.isInput){
				delete this.element.data().date;
			}
		},

		_utc_to_local: function(utc){
			return utc && new Date(utc.getTime() + (utc.getTimezoneOffset()*60000));
		},
		_local_to_utc: function(local){
			return local && new Date(local.getTime() - (local.getTimezoneOffset()*60000));
		},
		_zero_time: function(local){
			return local && new Date(local.getFullYear(), local.getMonth(), local.getDate());
		},
		_zero_utc_time: function(utc){
			return utc && new Date(Date.UTC(utc.getUTCFullYear(), utc.getUTCMonth(), utc.getUTCDate()));
		},

		getDates: function(){
			return $.map(this.dates, this._utc_to_local);
		},

		getUTCDates: function(){
			return $.map(this.dates, function(d){
				return new Date(d);
			});
		},

		getDate: function(){
			return this._utc_to_local(this.getUTCDate());
		},

		getUTCDate: function(){
			return new Date(this.dates.get(-1));
		},

		setDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.update.apply(this, args);
			this._trigger('changeDate');
			this.setValue();
		},

		setUTCDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.update.apply(this, $.map(args, this._utc_to_local));
			this._trigger('changeDate');
			this.setValue();
		},

		setDate: alias('setDates'),
		setUTCDate: alias('setUTCDates'),

		setValue: function(){
			var formatted = this.getFormattedDate();
			if (!this.isInput){
				if (this.component){
					this.element.find('input').val(formatted).change();
				}
			}
			else {
				this.element.val(formatted).change();
			}
		},

		getFormattedDate: function(format){
			if (format === undefined)
				format = this.o.format;

			var lang = this.o.language;
			return $.map(this.dates, function(d){
				return DPGlobal.formatDate(d, format, lang);
			}).join(this.o.multidateSeparator);
		},

		setStartDate: function(startDate){
			this._process_options({startDate: startDate});
			this.update();
			this.updateNavArrows();
		},

		setEndDate: function(endDate){
			this._process_options({endDate: endDate});
			this.update();
			this.updateNavArrows();
		},

		setDaysOfWeekDisabled: function(daysOfWeekDisabled){
			this._process_options({daysOfWeekDisabled: daysOfWeekDisabled});
			this.update();
			this.updateNavArrows();
		},

		place: function(){
			if (this.isInline)
				return;
			var calendarWidth = this.picker.outerWidth(),
				calendarHeight = this.picker.outerHeight(),
				visualPadding = 10,
				windowWidth = $window.width(),
				windowHeight = $window.height(),
				scrollTop = $window.scrollTop();

			var zIndex = parseInt(this.element.parents().filter(function(){
					return $(this).css('z-index') !== 'auto';
				}).first().css('z-index'))+10;
			var offset = this.component ? this.component.parent().offset() : this.element.offset();
			var height = this.component ? this.component.outerHeight(true) : this.element.outerHeight(false);
			var width = this.component ? this.component.outerWidth(true) : this.element.outerWidth(false);
			var left = offset.left,
				top = offset.top;

			this.picker.removeClass(
				'datepicker-orient-top datepicker-orient-bottom '+
				'datepicker-orient-right datepicker-orient-left'
			);

			if (this.o.orientation.x !== 'auto'){
				this.picker.addClass('datepicker-orient-' + this.o.orientation.x);
				if (this.o.orientation.x === 'right')
					left -= calendarWidth - width;
			}
			// auto x orientation is best-placement: if it crosses a window
			// edge, fudge it sideways
			else {
				// Default to left
				this.picker.addClass('datepicker-orient-left');
				if (offset.left < 0)
					left -= offset.left - visualPadding;
				else if (offset.left + calendarWidth > windowWidth)
					left = windowWidth - calendarWidth - visualPadding;
			}

			// auto y orientation is best-situation: top or bottom, no fudging,
			// decision based on which shows more of the calendar
			var yorient = this.o.orientation.y,
				top_overflow, bottom_overflow;
			if (yorient === 'auto'){
				top_overflow = -scrollTop + offset.top - calendarHeight;
				bottom_overflow = scrollTop + windowHeight - (offset.top + height + calendarHeight);
				if (Math.max(top_overflow, bottom_overflow) === bottom_overflow)
					yorient = 'top';
				else
					yorient = 'bottom';
			}
			this.picker.addClass('datepicker-orient-' + yorient);
			if (yorient === 'top')
				top += height;
			else
				top -= calendarHeight + parseInt(this.picker.css('padding-top'));

			this.picker.css({
				top: top,
				left: left,
				zIndex: zIndex
			});
		},

		_allow_update: true,
		update: function(){
			if (!this._allow_update)
				return;

			var oldDates = this.dates.copy(),
				dates = [],
				fromArgs = false;
			if (arguments.length){
				$.each(arguments, $.proxy(function(i, date){
					if (date instanceof Date)
						date = this._local_to_utc(date);
					dates.push(date);
				}, this));
				fromArgs = true;
			}
			else {
				dates = this.isInput
						? this.element.val()
						: this.element.data('date') || this.element.find('input').val();
				if (dates && this.o.multidate)
					dates = dates.split(this.o.multidateSeparator);
				else
					dates = [dates];
				delete this.element.data().date;
			}

			dates = $.map(dates, $.proxy(function(date){
				return DPGlobal.parseDate(date, this.o.format, this.o.language);
			}, this));
			dates = $.grep(dates, $.proxy(function(date){
				return (
					date < this.o.startDate ||
					date > this.o.endDate ||
					!date
				);
			}, this), true);
			this.dates.replace(dates);

			if (this.dates.length)
				this.viewDate = new Date(this.dates.get(-1));
			else if (this.viewDate < this.o.startDate)
				this.viewDate = new Date(this.o.startDate);
			else if (this.viewDate > this.o.endDate)
				this.viewDate = new Date(this.o.endDate);

			if (fromArgs){
				// setting date by clicking
				this.setValue();
			}
			else if (dates.length){
				// setting date by typing
				if (String(oldDates) !== String(this.dates))
					this._trigger('changeDate');
			}
			if (!this.dates.length && oldDates.length)
				this._trigger('clearDate');

			this.fill();
		},

		fillDow: function(){
			var dowCnt = this.o.weekStart,
				html = '<tr>';
			if (this.o.calendarWeeks){
				var cell = '<th class="cw">&nbsp;</th>';
				html += cell;
				this.picker.find('.datepicker-days thead tr:first-child').prepend(cell);
			}
			while (dowCnt < this.o.weekStart + 7){
				html += '<th class="dow">'+dates[this.o.language].daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
		},

		fillMonths: function(){
			var html = '',
			i = 0;
			while (i < 12){
				html += '<span class="month">'+dates[this.o.language].monthsShort[i++]+'</span>';
			}
			this.picker.find('.datepicker-months td').html(html);
		},

		setRange: function(range){
			if (!range || !range.length)
				delete this.range;
			else
				this.range = $.map(range, function(d){
					return d.valueOf();
				});
			this.fill();
		},

		getClassNames: function(date){
			var cls = [],
				year = this.viewDate.getUTCFullYear(),
				month = this.viewDate.getUTCMonth(),
				today = new Date();
			if (date.getUTCFullYear() < year || (date.getUTCFullYear() === year && date.getUTCMonth() < month)){
				cls.push('old');
			}
			else if (date.getUTCFullYear() > year || (date.getUTCFullYear() === year && date.getUTCMonth() > month)){
				cls.push('new');
			}
			if (this.focusDate && date.valueOf() === this.focusDate.valueOf())
				cls.push('focused');
			// Compare internal UTC date with local today, not UTC today
			if (this.o.todayHighlight &&
				date.getUTCFullYear() === today.getFullYear() &&
				date.getUTCMonth() === today.getMonth() &&
				date.getUTCDate() === today.getDate()){
				cls.push('today');
			}
			if (this.dates.contains(date) !== -1)
				cls.push('active');
			if (date.valueOf() < this.o.startDate || date.valueOf() > this.o.endDate ||
				$.inArray(date.getUTCDay(), this.o.daysOfWeekDisabled) !== -1){
				cls.push('disabled');
			}
			if (this.range){
				if (date > this.range[0] && date < this.range[this.range.length-1]){
					cls.push('range');
				}
				if ($.inArray(date.valueOf(), this.range) !== -1){
					cls.push('selected');
				}
			}
			return cls;
		},

		fill: function(){
			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth(),
				startYear = this.o.startDate !== -Infinity ? this.o.startDate.getUTCFullYear() : -Infinity,
				startMonth = this.o.startDate !== -Infinity ? this.o.startDate.getUTCMonth() : -Infinity,
				endYear = this.o.endDate !== Infinity ? this.o.endDate.getUTCFullYear() : Infinity,
				endMonth = this.o.endDate !== Infinity ? this.o.endDate.getUTCMonth() : Infinity,
				todaytxt = dates[this.o.language].today || dates['en'].today || '',
				cleartxt = dates[this.o.language].clear || dates['en'].clear || '',
				tooltip;
			this.picker.find('.datepicker-days thead th.datepicker-switch')
						.text(dates[this.o.language].months[month]+' '+year);
			this.picker.find('tfoot th.today')
						.text(todaytxt)
						.toggle(this.o.todayBtn !== false);
			this.picker.find('tfoot th.clear')
						.text(cleartxt)
						.toggle(this.o.clearBtn !== false);
			this.updateNavArrows();
			this.fillMonths();
			var prevMonth = UTCDate(year, month-1, 28),
				day = DPGlobal.getDaysInMonth(prevMonth.getUTCFullYear(), prevMonth.getUTCMonth());
			prevMonth.setUTCDate(day);
			prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.o.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			nextMonth.setUTCDate(nextMonth.getUTCDate() + 42);
			nextMonth = nextMonth.valueOf();
			var html = [];
			var clsName;
			while (prevMonth.valueOf() < nextMonth){
				if (prevMonth.getUTCDay() === this.o.weekStart){
					html.push('<tr>');
					if (this.o.calendarWeeks){
						// ISO 8601: First week contains first thursday.
						// ISO also states week starts on Monday, but we can be more abstract here.
						var
							// Start of current week: based on weekstart/current date
							ws = new Date(+prevMonth + (this.o.weekStart - prevMonth.getUTCDay() - 7) % 7 * 864e5),
							// Thursday of this week
							th = new Date(Number(ws) + (7 + 4 - ws.getUTCDay()) % 7 * 864e5),
							// First Thursday of year, year from thursday
							yth = new Date(Number(yth = UTCDate(th.getUTCFullYear(), 0, 1)) + (7 + 4 - yth.getUTCDay())%7*864e5),
							// Calendar week: ms between thursdays, div ms per day, div 7 days
							calWeek =  (th - yth) / 864e5 / 7 + 1;
						html.push('<td class="cw">'+ calWeek +'</td>');

					}
				}
				clsName = this.getClassNames(prevMonth);
				clsName.push('day');

				if (this.o.beforeShowDay !== $.noop){
					var before = this.o.beforeShowDay(this._utc_to_local(prevMonth));
					if (before === undefined)
						before = {};
					else if (typeof(before) === 'boolean')
						before = {enabled: before};
					else if (typeof(before) === 'string')
						before = {classes: before};
					if (before.enabled === false)
						clsName.push('disabled');
					if (before.classes)
						clsName = clsName.concat(before.classes.split(/\s+/));
					if (before.tooltip)
						tooltip = before.tooltip;
				}

				clsName = $.unique(clsName);
				html.push('<td class="'+clsName.join(' ')+'"' + (tooltip ? ' title="'+tooltip+'"' : '') + '>'+prevMonth.getUTCDate() + '</td>');
				if (prevMonth.getUTCDay() === this.o.weekEnd){
					html.push('</tr>');
				}
				prevMonth.setUTCDate(prevMonth.getUTCDate()+1);
			}
			this.picker.find('.datepicker-days tbody').empty().append(html.join(''));

			var months = this.picker.find('.datepicker-months')
						.find('th:eq(1)')
							.text(year)
							.end()
						.find('span').removeClass('active');

			$.each(this.dates, function(i, d){
				if (d.getUTCFullYear() === year)
					months.eq(d.getUTCMonth()).addClass('active');
			});

			if (year < startYear || year > endYear){
				months.addClass('disabled');
			}
			if (year === startYear){
				months.slice(0, startMonth).addClass('disabled');
			}
			if (year === endYear){
				months.slice(endMonth+1).addClass('disabled');
			}

			html = '';
			year = parseInt(year/10, 10) * 10;
			var yearCont = this.picker.find('.datepicker-years')
								.find('th:eq(1)')
									.text(year + '-' + (year + 9))
									.end()
								.find('td');
			year -= 1;
			var years = $.map(this.dates, function(d){
					return d.getUTCFullYear();
				}),
				classes;
			for (var i = -1; i < 11; i++){
				classes = ['year'];
				if (i === -1)
					classes.push('old');
				else if (i === 10)
					classes.push('new');
				if ($.inArray(year, years) !== -1)
					classes.push('active');
				if (year < startYear || year > endYear)
					classes.push('disabled');
				html += '<span class="' + classes.join(' ') + '">'+year+'</span>';
				year += 1;
			}
			yearCont.html(html);
		},

		updateNavArrows: function(){
			if (!this._allow_update)
				return;

			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth();
			switch (this.viewMode){
				case 0:
					if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear() && month <= this.o.startDate.getUTCMonth()){
						this.picker.find('.prev').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.prev').css({visibility: 'visible'});
					}
					if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear() && month >= this.o.endDate.getUTCMonth()){
						this.picker.find('.next').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.next').css({visibility: 'visible'});
					}
					break;
				case 1:
				case 2:
					if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear()){
						this.picker.find('.prev').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.prev').css({visibility: 'visible'});
					}
					if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear()){
						this.picker.find('.next').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.next').css({visibility: 'visible'});
					}
					break;
			}
		},

		click: function(e){
			e.preventDefault();
			var target = $(e.target).closest('span, td, th'),
				year, month, day;
			if (target.length === 1){
				switch (target[0].nodeName.toLowerCase()){
					case 'th':
						switch (target[0].className){
							case 'datepicker-switch':
								this.showMode(1);
								break;
							case 'prev':
							case 'next':
								var dir = DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1);
								switch (this.viewMode){
									case 0:
										this.viewDate = this.moveMonth(this.viewDate, dir);
										this._trigger('changeMonth', this.viewDate);
										break;
									case 1:
									case 2:
										this.viewDate = this.moveYear(this.viewDate, dir);
										if (this.viewMode === 1)
											this._trigger('changeYear', this.viewDate);
										break;
								}
								this.fill();
								break;
							case 'today':
								var date = new Date();
								date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);

								this.showMode(-2);
								var which = this.o.todayBtn === 'linked' ? null : 'view';
								this._setDate(date, which);
								break;
							case 'clear':
								var element;
								if (this.isInput)
									element = this.element;
								else if (this.component)
									element = this.element.find('input');
								if (element)
									element.val("").change();
								this.update();
								this._trigger('changeDate');
								if (this.o.autoclose)
									this.hide();
								break;
						}
						break;
					case 'span':
						if (!target.is('.disabled')){
							this.viewDate.setUTCDate(1);
							if (target.is('.month')){
								day = 1;
								month = target.parent().find('span').index(target);
								year = this.viewDate.getUTCFullYear();
								this.viewDate.setUTCMonth(month);
								this._trigger('changeMonth', this.viewDate);
								if (this.o.minViewMode === 1){
									this._setDate(UTCDate(year, month, day));
								}
							}
							else {
								day = 1;
								month = 0;
								year = parseInt(target.text(), 10)||0;
								this.viewDate.setUTCFullYear(year);
								this._trigger('changeYear', this.viewDate);
								if (this.o.minViewMode === 2){
									this._setDate(UTCDate(year, month, day));
								}
							}
							this.showMode(-1);
							this.fill();
						}
						break;
					case 'td':
						if (target.is('.day') && !target.is('.disabled')){
							day = parseInt(target.text(), 10)||1;
							year = this.viewDate.getUTCFullYear();
							month = this.viewDate.getUTCMonth();
							if (target.is('.old')){
								if (month === 0){
									month = 11;
									year -= 1;
								}
								else {
									month -= 1;
								}
							}
							else if (target.is('.new')){
								if (month === 11){
									month = 0;
									year += 1;
								}
								else {
									month += 1;
								}
							}
							this._setDate(UTCDate(year, month, day));
						}
						break;
				}
			}
			if (this.picker.is(':visible') && this._focused_from){
				$(this._focused_from).focus();
			}
			delete this._focused_from;
		},

		_toggle_multidate: function(date){
			var ix = this.dates.contains(date);
			if (!date){
				this.dates.clear();
			}
			else if (ix !== -1){
				this.dates.remove(ix);
			}
			else {
				this.dates.push(date);
			}
			if (typeof this.o.multidate === 'number')
				while (this.dates.length > this.o.multidate)
					this.dates.remove(0);
		},

		_setDate: function(date, which){
			if (!which || which === 'date')
				this._toggle_multidate(date && new Date(date));
			if (!which || which  === 'view')
				this.viewDate = date && new Date(date);

			this.fill();
			this.setValue();
			this._trigger('changeDate');
			var element;
			if (this.isInput){
				element = this.element;
			}
			else if (this.component){
				element = this.element.find('input');
			}
			if (element){
				element.change();
			}
			if (this.o.autoclose && (!which || which === 'date')){
				this.hide();
			}
		},

		moveMonth: function(date, dir){
			if (!date)
				return undefined;
			if (!dir)
				return date;
			var new_date = new Date(date.valueOf()),
				day = new_date.getUTCDate(),
				month = new_date.getUTCMonth(),
				mag = Math.abs(dir),
				new_month, test;
			dir = dir > 0 ? 1 : -1;
			if (mag === 1){
				test = dir === -1
					// If going back one month, make sure month is not current month
					// (eg, Mar 31 -> Feb 31 == Feb 28, not Mar 02)
					? function(){
						return new_date.getUTCMonth() === month;
					}
					// If going forward one month, make sure month is as expected
					// (eg, Jan 31 -> Feb 31 == Feb 28, not Mar 02)
					: function(){
						return new_date.getUTCMonth() !== new_month;
					};
				new_month = month + dir;
				new_date.setUTCMonth(new_month);
				// Dec -> Jan (12) or Jan -> Dec (-1) -- limit expected date to 0-11
				if (new_month < 0 || new_month > 11)
					new_month = (new_month + 12) % 12;
			}
			else {
				// For magnitudes >1, move one month at a time...
				for (var i=0; i < mag; i++)
					// ...which might decrease the day (eg, Jan 31 to Feb 28, etc)...
					new_date = this.moveMonth(new_date, dir);
				// ...then reset the day, keeping it in the new month
				new_month = new_date.getUTCMonth();
				new_date.setUTCDate(day);
				test = function(){
					return new_month !== new_date.getUTCMonth();
				};
			}
			// Common date-resetting loop -- if date is beyond end of month, make it
			// end of month
			while (test()){
				new_date.setUTCDate(--day);
				new_date.setUTCMonth(new_month);
			}
			return new_date;
		},

		moveYear: function(date, dir){
			return this.moveMonth(date, dir*12);
		},

		dateWithinRange: function(date){
			return date >= this.o.startDate && date <= this.o.endDate;
		},

		keydown: function(e){
			if (this.picker.is(':not(:visible)')){
				if (e.keyCode === 27) // allow escape to hide and re-show picker
					this.show();
				return;
			}
			var dateChanged = false,
				dir, newDate, newViewDate,
				focusDate = this.focusDate || this.viewDate;
			switch (e.keyCode){
				case 27: // escape
					if (this.focusDate){
						this.focusDate = null;
						this.viewDate = this.dates.get(-1) || this.viewDate;
						this.fill();
					}
					else
						this.hide();
					e.preventDefault();
					break;
				case 37: // left
				case 39: // right
					if (!this.o.keyboardNavigation)
						break;
					dir = e.keyCode === 37 ? -1 : 1;
					if (e.ctrlKey){
						newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir);
						newViewDate = this.moveYear(focusDate, dir);
						this._trigger('changeYear', this.viewDate);
					}
					else if (e.shiftKey){
						newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir);
						newViewDate = this.moveMonth(focusDate, dir);
						this._trigger('changeMonth', this.viewDate);
					}
					else {
						newDate = new Date(this.dates.get(-1) || UTCToday());
						newDate.setUTCDate(newDate.getUTCDate() + dir);
						newViewDate = new Date(focusDate);
						newViewDate.setUTCDate(focusDate.getUTCDate() + dir);
					}
					if (this.dateWithinRange(newDate)){
						this.focusDate = this.viewDate = newViewDate;
						this.setValue();
						this.fill();
						e.preventDefault();
					}
					break;
				case 38: // up
				case 40: // down
					if (!this.o.keyboardNavigation)
						break;
					dir = e.keyCode === 38 ? -1 : 1;
					if (e.ctrlKey){
						newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir);
						newViewDate = this.moveYear(focusDate, dir);
						this._trigger('changeYear', this.viewDate);
					}
					else if (e.shiftKey){
						newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir);
						newViewDate = this.moveMonth(focusDate, dir);
						this._trigger('changeMonth', this.viewDate);
					}
					else {
						newDate = new Date(this.dates.get(-1) || UTCToday());
						newDate.setUTCDate(newDate.getUTCDate() + dir * 7);
						newViewDate = new Date(focusDate);
						newViewDate.setUTCDate(focusDate.getUTCDate() + dir * 7);
					}
					if (this.dateWithinRange(newDate)){
						this.focusDate = this.viewDate = newViewDate;
						this.setValue();
						this.fill();
						e.preventDefault();
					}
					break;
				case 32: // spacebar
					// Spacebar is used in manually typing dates in some formats.
					// As such, its behavior should not be hijacked.
					break;
				case 13: // enter
					focusDate = this.focusDate || this.dates.get(-1) || this.viewDate;
					this._toggle_multidate(focusDate);
					dateChanged = true;
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.setValue();
					this.fill();
					if (this.picker.is(':visible')){
						e.preventDefault();
						if (this.o.autoclose)
							this.hide();
					}
					break;
				case 9: // tab
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.fill();
					this.hide();
					break;
			}
			if (dateChanged){
				if (this.dates.length)
					this._trigger('changeDate');
				else
					this._trigger('clearDate');
				var element;
				if (this.isInput){
					element = this.element;
				}
				else if (this.component){
					element = this.element.find('input');
				}
				if (element){
					element.change();
				}
			}
		},

		showMode: function(dir){
			if (dir){
				this.viewMode = Math.max(this.o.minViewMode, Math.min(2, this.viewMode + dir));
			}
			this.picker
				.find('>div')
				.hide()
				.filter('.datepicker-'+DPGlobal.modes[this.viewMode].clsName)
					.css('display', 'block');
			this.updateNavArrows();
		}
	};

	var DateRangePicker = function(element, options){
		this.element = $(element);
		this.inputs = $.map(options.inputs, function(i){
			return i.jquery ? i[0] : i;
		});
		delete options.inputs;

		$(this.inputs)
			.datepicker(options)
			.bind('changeDate', $.proxy(this.dateUpdated, this));

		this.pickers = $.map(this.inputs, function(i){
			return $(i).data('datepicker');
		});
		this.updateDates();
	};
	DateRangePicker.prototype = {
		updateDates: function(){
			this.dates = $.map(this.pickers, function(i){
				return i.getUTCDate();
			});
			this.updateRanges();
		},
		updateRanges: function(){
			var range = $.map(this.dates, function(d){
				return d.valueOf();
			});
			$.each(this.pickers, function(i, p){
				p.setRange(range);
			});
		},
		dateUpdated: function(e){
			// `this.updating` is a workaround for preventing infinite recursion
			// between `changeDate` triggering and `setUTCDate` calling.  Until
			// there is a better mechanism.
			if (this.updating)
				return;
			this.updating = true;

			var dp = $(e.target).data('datepicker'),
				new_date = dp.getUTCDate(),
				i = $.inArray(e.target, this.inputs),
				l = this.inputs.length;
			if (i === -1)
				return;

			$.each(this.pickers, function(i, p){
				if (!p.getUTCDate())
					p.setUTCDate(new_date);
			});

			if (new_date < this.dates[i]){
				// Date being moved earlier/left
				while (i >= 0 && new_date < this.dates[i]){
					this.pickers[i--].setUTCDate(new_date);
				}
			}
			else if (new_date > this.dates[i]){
				// Date being moved later/right
				while (i < l && new_date > this.dates[i]){
					this.pickers[i++].setUTCDate(new_date);
				}
			}
			this.updateDates();

			delete this.updating;
		},
		remove: function(){
			$.map(this.pickers, function(p){ p.remove(); });
			delete this.element.data().datepicker;
		}
	};

	function opts_from_el(el, prefix){
		// Derive options from element data-attrs
		var data = $(el).data(),
			out = {}, inkey,
			replace = new RegExp('^' + prefix.toLowerCase() + '([A-Z])');
		prefix = new RegExp('^' + prefix.toLowerCase());
		function re_lower(_,a){
			return a.toLowerCase();
		}
		for (var key in data)
			if (prefix.test(key)){
				inkey = key.replace(replace, re_lower);
				out[inkey] = data[key];
			}
		return out;
	}

	function opts_from_locale(lang){
		// Derive options from locale plugins
		var out = {};
		// Check if "de-DE" style date is available, if not language should
		// fallback to 2 letter code eg "de"
		if (!dates[lang]){
			lang = lang.split('-')[0];
			if (!dates[lang])
				return;
		}
		var d = dates[lang];
		$.each(locale_opts, function(i,k){
			if (k in d)
				out[k] = d[k];
		});
		return out;
	}

	var old = $.fn.datepicker;
	$.fn.datepicker = function(option){
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function(){
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data){
				var elopts = opts_from_el(this, 'date'),
					// Preliminary otions
					xopts = $.extend({}, defaults, elopts, options),
					locopts = opts_from_locale(xopts.language),
					// Options priority: js args, data-attrs, locales, defaults
					opts = $.extend({}, defaults, locopts, elopts, options);
				if ($this.is('.input-daterange') || opts.inputs){
					var ropts = {
						inputs: opts.inputs || $this.find('input').toArray()
					};
					$this.data('datepicker', (data = new DateRangePicker(this, $.extend(opts, ropts))));
				}
				else {
					$this.data('datepicker', (data = new Datepicker(this, opts)));
				}
			}
			if (typeof option === 'string' && typeof data[option] === 'function'){
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined)
					return false;
			}
		});
		if (internal_return !== undefined)
			return internal_return;
		else
			return this;
	};

	var defaults = $.fn.datepicker.defaults = {
		autoclose: false,
		beforeShowDay: $.noop,
		calendarWeeks: false,
		clearBtn: false,
		daysOfWeekDisabled: [],
		endDate: Infinity,
		forceParse: true,
		format: 'mm/dd/yyyy',
		keyboardNavigation: true,
		language: 'en',
		minViewMode: 0,
		multidate: false,
		multidateSeparator: ',',
		orientation: "auto",
		rtl: false,
		startDate: -Infinity,
		startView: 0,
		todayBtn: false,
		todayHighlight: false,
		weekStart: 0
	};
	var locale_opts = $.fn.datepicker.locale_opts = [
		'format',
		'rtl',
		'weekStart'
	];
	$.fn.datepicker.Constructor = Datepicker;
	var dates = $.fn.datepicker.dates = {
		en: {
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			today: "Today",
			clear: "Clear"
		}
	};

	var DPGlobal = {
		modes: [
			{
				clsName: 'days',
				navFnc: 'Month',
				navStep: 1
			},
			{
				clsName: 'months',
				navFnc: 'FullYear',
				navStep: 1
			},
			{
				clsName: 'years',
				navFnc: 'FullYear',
				navStep: 10
		}],
		isLeapYear: function(year){
			return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
		},
		getDaysInMonth: function(year, month){
			return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
		},
		validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
		nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,
		parseFormat: function(format){
			// IE treats \0 as a string end in inputs (truncating the value),
			// so it's a bad format delimiter, anyway
			var separators = format.replace(this.validParts, '\0').split('\0'),
				parts = format.match(this.validParts);
			if (!separators || !separators.length || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separators: separators, parts: parts};
		},
		parseDate: function(date, format, language){
			if (!date)
				return undefined;
			if (date instanceof Date)
				return date;
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			var part_re = /([\-+]\d+)([dmwy])/,
				parts = date.match(/([\-+]\d+)([dmwy])/g),
				part, dir, i;
			if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)){
				date = new Date();
				for (i=0; i < parts.length; i++){
					part = part_re.exec(parts[i]);
					dir = parseInt(part[1]);
					switch (part[2]){
						case 'd':
							date.setUTCDate(date.getUTCDate() + dir);
							break;
						case 'm':
							date = Datepicker.prototype.moveMonth.call(Datepicker.prototype, date, dir);
							break;
						case 'w':
							date.setUTCDate(date.getUTCDate() + dir * 7);
							break;
						case 'y':
							date = Datepicker.prototype.moveYear.call(Datepicker.prototype, date, dir);
							break;
					}
				}
				return UTCDate(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0);
			}
			parts = date && date.match(this.nonpunctuation) || [];
			date = new Date();
			var parsed = {},
				setters_order = ['yyyy', 'yy', 'M', 'MM', 'm', 'mm', 'd', 'dd'],
				setters_map = {
					yyyy: function(d,v){
						return d.setUTCFullYear(v);
					},
					yy: function(d,v){
						return d.setUTCFullYear(2000+v);
					},
					m: function(d,v){
						if (isNaN(d))
							return d;
						v -= 1;
						while (v < 0) v += 12;
						v %= 12;
						d.setUTCMonth(v);
						while (d.getUTCMonth() !== v)
							d.setUTCDate(d.getUTCDate()-1);
						return d;
					},
					d: function(d,v){
						return d.setUTCDate(v);
					}
				},
				val, filtered;
			setters_map['M'] = setters_map['MM'] = setters_map['mm'] = setters_map['m'];
			setters_map['dd'] = setters_map['d'];
			date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
			var fparts = format.parts.slice();
			// Remove noop parts
			if (parts.length !== fparts.length){
				fparts = $(fparts).filter(function(i,p){
					return $.inArray(p, setters_order) !== -1;
				}).toArray();
			}
			// Process remainder
			function match_part(){
				var m = this.slice(0, parts[i].length),
					p = parts[i].slice(0, m.length);
				return m === p;
			}
			if (parts.length === fparts.length){
				var cnt;
				for (i=0, cnt = fparts.length; i < cnt; i++){
					val = parseInt(parts[i], 10);
					part = fparts[i];
					if (isNaN(val)){
						switch (part){
							case 'MM':
								filtered = $(dates[language].months).filter(match_part);
								val = $.inArray(filtered[0], dates[language].months) + 1;
								break;
							case 'M':
								filtered = $(dates[language].monthsShort).filter(match_part);
								val = $.inArray(filtered[0], dates[language].monthsShort) + 1;
								break;
						}
					}
					parsed[part] = val;
				}
				var _date, s;
				for (i=0; i < setters_order.length; i++){
					s = setters_order[i];
					if (s in parsed && !isNaN(parsed[s])){
						_date = new Date(date);
						setters_map[s](_date, parsed[s]);
						if (!isNaN(_date))
							date = _date;
					}
				}
			}
			return date;
		},
		formatDate: function(date, format, language){
			if (!date)
				return '';
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			var val = {
				d: date.getUTCDate(),
				D: dates[language].daysShort[date.getUTCDay()],
				DD: dates[language].days[date.getUTCDay()],
				m: date.getUTCMonth() + 1,
				M: dates[language].monthsShort[date.getUTCMonth()],
				MM: dates[language].months[date.getUTCMonth()],
				yy: date.getUTCFullYear().toString().substring(2),
				yyyy: date.getUTCFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			date = [];
			var seps = $.extend([], format.separators);
			for (var i=0, cnt = format.parts.length; i <= cnt; i++){
				if (seps.length)
					date.push(seps.shift());
				date.push(val[format.parts[i]]);
			}
			return date.join('');
		},
		headTemplate: '<thead>'+
							'<tr>'+
								'<th class="prev">&laquo;</th>'+
								'<th colspan="5" class="datepicker-switch"></th>'+
								'<th class="next">&raquo;</th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
		footTemplate: '<tfoot>'+
							'<tr>'+
								'<th colspan="7" class="today"></th>'+
							'</tr>'+
							'<tr>'+
								'<th colspan="7" class="clear"></th>'+
							'</tr>'+
						'</tfoot>'
	};
	DPGlobal.template = '<div class="datepicker">'+
							'<div class="datepicker-days">'+
								'<table class=" table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
						'</div>';

	$.fn.datepicker.DPGlobal = DPGlobal;


	/* DATEPICKER NO CONFLICT
	* =================== */

	$.fn.datepicker.noConflict = function(){
		$.fn.datepicker = old;
		return this;
	};


	/* DATEPICKER DATA-API
	* ================== */

	$(document).on(
		'focus.datepicker.data-api click.datepicker.data-api',
		'[data-provide="datepicker"]',
		function(e){
			var $this = $(this);
			if ($this.data('datepicker'))
				return;
			e.preventDefault();
			// component click requires us to explicitly show it
			$this.datepicker('show');
		}
	);
	$(function(){
		$('[data-provide="datepicker-inline"]').datepicker();
	});

}(window.jQuery));

/*!
 * pickadate.js v3.5.6, 2015/04/20
 * By Amsul, http://amsul.ca
 * Hosted on http://amsul.github.io/pickadate.js
 * Licensed under MIT
 */
!function(a){"function"==typeof define&&define.amd?define("picker",["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):this.Picker=a(jQuery)}(function(a){function b(f,g,i,m){function n(){return b._.node("div",b._.node("div",b._.node("div",b._.node("div",B.component.nodes(w.open),y.box),y.wrap),y.frame),y.holder,'tabindex="-1"')}function o(){z.data(g,B).addClass(y.input).val(z.data("value")?B.get("select",x.format):f.value),x.editable||z.on("focus."+w.id+" click."+w.id,function(a){a.preventDefault(),B.open()}).on("keydown."+w.id,u),e(f,{haspopup:!0,expanded:!1,readonly:!1,owns:f.id+"_root"})}function p(){e(B.$root[0],"hidden",!0)}function q(){B.$holder.on({keydown:u,"focus.toOpen":t,blur:function(){z.removeClass(y.target)},focusin:function(a){B.$root.removeClass(y.focused),a.stopPropagation()},"mousedown click":function(b){var c=b.target;c!=B.$holder[0]&&(b.stopPropagation(),"mousedown"!=b.type||a(c).is("input, select, textarea, button, option")||(b.preventDefault(),B.$holder[0].focus()))}}).on("click","[data-pick], [data-nav], [data-clear], [data-close]",function(){var b=a(this),c=b.data(),d=b.hasClass(y.navDisabled)||b.hasClass(y.disabled),e=h();e=e&&(e.type||e.href),(d||e&&!a.contains(B.$root[0],e))&&B.$holder[0].focus(),!d&&c.nav?B.set("highlight",B.component.item.highlight,{nav:c.nav}):!d&&"pick"in c?(B.set("select",c.pick),x.closeOnSelect&&B.close(!0)):c.clear?(B.clear(),x.closeOnClear&&B.close(!0)):c.close&&B.close(!0)})}function r(){var b;x.hiddenName===!0?(b=f.name,f.name=""):(b=["string"==typeof x.hiddenPrefix?x.hiddenPrefix:"","string"==typeof x.hiddenSuffix?x.hiddenSuffix:"_submit"],b=b[0]+f.name+b[1]),B._hidden=a('<input type=hidden name="'+b+'"'+(z.data("value")||f.value?' value="'+B.get("select",x.formatSubmit)+'"':"")+">")[0],z.on("change."+w.id,function(){B._hidden.value=f.value?B.get("select",x.formatSubmit):""})}function s(){v&&l?B.$holder.find("."+y.frame).one("transitionend",function(){B.$holder[0].focus()}):B.$holder[0].focus()}function t(a){a.stopPropagation(),z.addClass(y.target),B.$root.addClass(y.focused),B.open()}function u(a){var b=a.keyCode,c=/^(8|46)$/.test(b);return 27==b?(B.close(!0),!1):void((32==b||c||!w.open&&B.component.key[b])&&(a.preventDefault(),a.stopPropagation(),c?B.clear().close():B.open()))}if(!f)return b;var v=!1,w={id:f.id||"P"+Math.abs(~~(Math.random()*new Date))},x=i?a.extend(!0,{},i.defaults,m):m||{},y=a.extend({},b.klasses(),x.klass),z=a(f),A=function(){return this.start()},B=A.prototype={constructor:A,$node:z,start:function(){return w&&w.start?B:(w.methods={},w.start=!0,w.open=!1,w.type=f.type,f.autofocus=f==h(),f.readOnly=!x.editable,f.id=f.id||w.id,"text"!=f.type&&(f.type="text"),B.component=new i(B,x),B.$root=a('<div class="'+y.picker+'" id="'+f.id+'_root" />'),p(),B.$holder=a(n()).appendTo(B.$root),q(),x.formatSubmit&&r(),o(),x.containerHidden?a(x.containerHidden).append(B._hidden):z.after(B._hidden),x.container?a(x.container).append(B.$root):z.after(B.$root),B.on({start:B.component.onStart,render:B.component.onRender,stop:B.component.onStop,open:B.component.onOpen,close:B.component.onClose,set:B.component.onSet}).on({start:x.onStart,render:x.onRender,stop:x.onStop,open:x.onOpen,close:x.onClose,set:x.onSet}),v=c(B.$holder[0]),f.autofocus&&B.open(),B.trigger("start").trigger("render"))},render:function(b){return b?(B.$holder=a(n()),q(),B.$root.html(B.$holder)):B.$root.find("."+y.box).html(B.component.nodes(w.open)),B.trigger("render")},stop:function(){return w.start?(B.close(),B._hidden&&B._hidden.parentNode.removeChild(B._hidden),B.$root.remove(),z.removeClass(y.input).removeData(g),setTimeout(function(){z.off("."+w.id)},0),f.type=w.type,f.readOnly=!1,B.trigger("stop"),w.methods={},w.start=!1,B):B},open:function(c){return w.open?B:(z.addClass(y.active),e(f,"expanded",!0),setTimeout(function(){B.$root.addClass(y.opened),e(B.$root[0],"hidden",!1)},0),c!==!1&&(w.open=!0,v&&k.css("overflow","hidden").css("padding-right","+="+d()),s(),j.on("click."+w.id+" focusin."+w.id,function(a){var b=a.target;b!=f&&b!=document&&3!=a.which&&B.close(b===B.$holder[0])}).on("keydown."+w.id,function(c){var d=c.keyCode,e=B.component.key[d],f=c.target;27==d?B.close(!0):f!=B.$holder[0]||!e&&13!=d?a.contains(B.$root[0],f)&&13==d&&(c.preventDefault(),f.click()):(c.preventDefault(),e?b._.trigger(B.component.key.go,B,[b._.trigger(e)]):B.$root.find("."+y.highlighted).hasClass(y.disabled)||(B.set("select",B.component.item.highlight),x.closeOnSelect&&B.close(!0)))})),B.trigger("open"))},close:function(a){return a&&(x.editable?f.focus():(B.$holder.off("focus.toOpen").focus(),setTimeout(function(){B.$holder.on("focus.toOpen",t)},0))),z.removeClass(y.active),e(f,"expanded",!1),setTimeout(function(){B.$root.removeClass(y.opened+" "+y.focused),e(B.$root[0],"hidden",!0)},0),w.open?(w.open=!1,v&&k.css("overflow","").css("padding-right","-="+d()),j.off("."+w.id),B.trigger("close")):B},clear:function(a){return B.set("clear",null,a)},set:function(b,c,d){var e,f,g=a.isPlainObject(b),h=g?b:{};if(d=g&&a.isPlainObject(c)?c:d||{},b){g||(h[b]=c);for(e in h)f=h[e],e in B.component.item&&(void 0===f&&(f=null),B.component.set(e,f,d)),("select"==e||"clear"==e)&&z.val("clear"==e?"":B.get(e,x.format)).trigger("change");B.render()}return d.muted?B:B.trigger("set",h)},get:function(a,c){if(a=a||"value",null!=w[a])return w[a];if("valueSubmit"==a){if(B._hidden)return B._hidden.value;a="value"}if("value"==a)return f.value;if(a in B.component.item){if("string"==typeof c){var d=B.component.get(a);return d?b._.trigger(B.component.formats.toString,B.component,[c,d]):""}return B.component.get(a)}},on:function(b,c,d){var e,f,g=a.isPlainObject(b),h=g?b:{};if(b){g||(h[b]=c);for(e in h)f=h[e],d&&(e="_"+e),w.methods[e]=w.methods[e]||[],w.methods[e].push(f)}return B},off:function(){var a,b,c=arguments;for(a=0,namesCount=c.length;a<namesCount;a+=1)b=c[a],b in w.methods&&delete w.methods[b];return B},trigger:function(a,c){var d=function(a){var d=w.methods[a];d&&d.map(function(a){b._.trigger(a,B,[c])})};return d("_"+a),d(a),B}};return new A}function c(a){var b,c="position";return a.currentStyle?b=a.currentStyle[c]:window.getComputedStyle&&(b=getComputedStyle(a)[c]),"fixed"==b}function d(){if(k.height()<=i.height())return 0;var b=a('<div style="visibility:hidden;width:100px" />').appendTo("body"),c=b[0].offsetWidth;b.css("overflow","scroll");var d=a('<div style="width:100%" />').appendTo(b),e=d[0].offsetWidth;return b.remove(),c-e}function e(b,c,d){if(a.isPlainObject(c))for(var e in c)f(b,e,c[e]);else f(b,c,d)}function f(a,b,c){a.setAttribute(("role"==b?"":"aria-")+b,c)}function g(b,c){a.isPlainObject(b)||(b={attribute:c}),c="";for(var d in b){var e=("role"==d?"":"aria-")+d,f=b[d];c+=null==f?"":e+'="'+b[d]+'"'}return c}function h(){try{return document.activeElement}catch(a){}}var i=a(window),j=a(document),k=a(document.documentElement),l=null!=document.documentElement.style.transition;return b.klasses=function(a){return a=a||"picker",{picker:a,opened:a+"--opened",focused:a+"--focused",input:a+"__input",active:a+"__input--active",target:a+"__input--target",holder:a+"__holder",frame:a+"__frame",wrap:a+"__wrap",box:a+"__box"}},b._={group:function(a){for(var c,d="",e=b._.trigger(a.min,a);e<=b._.trigger(a.max,a,[e]);e+=a.i)c=b._.trigger(a.item,a,[e]),d+=b._.node(a.node,c[0],c[1],c[2]);return d},node:function(b,c,d,e){return c?(c=a.isArray(c)?c.join(""):c,d=d?' class="'+d+'"':"",e=e?" "+e:"","<"+b+d+e+">"+c+"</"+b+">"):""},lead:function(a){return(10>a?"0":"")+a},trigger:function(a,b,c){return"function"==typeof a?a.apply(b,c||[]):a},digits:function(a){return/\d/.test(a[1])?2:1},isDate:function(a){return{}.toString.call(a).indexOf("Date")>-1&&this.isInteger(a.getDate())},isInteger:function(a){return{}.toString.call(a).indexOf("Number")>-1&&a%1===0},ariaAttr:g},b.extend=function(c,d){a.fn[c]=function(e,f){var g=this.data(c);return"picker"==e?g:g&&"string"==typeof e?b._.trigger(g[e],g,[f]):this.each(function(){var f=a(this);f.data(c)||new b(this,c,d,e)})},a.fn[c].defaults=d.defaults},b});
/*!
 * Date picker for pickadate.js v3.5.6
 * http://amsul.github.io/pickadate.js/date.htm
 */
!function(a){"function"==typeof define&&define.amd?define(["picker","jquery"],a):"object"==typeof exports?module.exports=a(require("./picker.js"),require("jquery")):a(Picker,jQuery)}(function(a,b){function c(a,b){var c=this,d=a.$node[0],e=d.value,f=a.$node.data("value"),g=f||e,h=f?b.formatSubmit:b.format,i=function(){return d.currentStyle?"rtl"==d.currentStyle.direction:"rtl"==getComputedStyle(a.$root[0]).direction};c.settings=b,c.$node=a.$node,c.queue={min:"measure create",max:"measure create",now:"now create",select:"parse create validate",highlight:"parse navigate create validate",view:"parse create validate viewset",disable:"deactivate",enable:"activate"},c.item={},c.item.clear=null,c.item.disable=(b.disable||[]).slice(0),c.item.enable=-function(a){return a[0]===!0?a.shift():-1}(c.item.disable),c.set("min",b.min).set("max",b.max).set("now"),g?c.set("select",g,{format:h,defaultValue:!0}):c.set("select",null).set("highlight",c.item.now),c.key={40:7,38:-7,39:function(){return i()?-1:1},37:function(){return i()?1:-1},go:function(a){var b=c.item.highlight,d=new Date(b.year,b.month,b.date+a);c.set("highlight",d,{interval:a}),this.render()}},a.on("render",function(){a.$root.find("."+b.klass.selectMonth).on("change",function(){var c=this.value;c&&(a.set("highlight",[a.get("view").year,c,a.get("highlight").date]),a.$root.find("."+b.klass.selectMonth).trigger("focus"))}),a.$root.find("."+b.klass.selectYear).on("change",function(){var c=this.value;c&&(a.set("highlight",[c,a.get("view").month,a.get("highlight").date]),a.$root.find("."+b.klass.selectYear).trigger("focus"))})},1).on("open",function(){var d="";c.disabled(c.get("now"))&&(d=":not(."+b.klass.buttonToday+")"),a.$root.find("button"+d+", select").attr("disabled",!1)},1).on("close",function(){a.$root.find("button, select").attr("disabled",!0)},1)}var d=7,e=6,f=a._;c.prototype.set=function(a,b,c){var d=this,e=d.item;return null===b?("clear"==a&&(a="select"),e[a]=b,d):(e["enable"==a?"disable":"flip"==a?"enable":a]=d.queue[a].split(" ").map(function(e){return b=d[e](a,b,c)}).pop(),"select"==a?d.set("highlight",e.select,c):"highlight"==a?d.set("view",e.highlight,c):a.match(/^(flip|min|max|disable|enable)$/)&&(e.select&&d.disabled(e.select)&&d.set("select",e.select,c),e.highlight&&d.disabled(e.highlight)&&d.set("highlight",e.highlight,c)),d)},c.prototype.get=function(a){return this.item[a]},c.prototype.create=function(a,c,d){var e,g=this;return c=void 0===c?a:c,c==-(1/0)||c==1/0?e=c:b.isPlainObject(c)&&f.isInteger(c.pick)?c=c.obj:b.isArray(c)?(c=new Date(c[0],c[1],c[2]),c=f.isDate(c)?c:g.create().obj):c=f.isInteger(c)||f.isDate(c)?g.normalize(new Date(c),d):g.now(a,c,d),{year:e||c.getFullYear(),month:e||c.getMonth(),date:e||c.getDate(),day:e||c.getDay(),obj:e||c,pick:e||c.getTime()}},c.prototype.createRange=function(a,c){var d=this,e=function(a){return a===!0||b.isArray(a)||f.isDate(a)?d.create(a):a};return f.isInteger(a)||(a=e(a)),f.isInteger(c)||(c=e(c)),f.isInteger(a)&&b.isPlainObject(c)?a=[c.year,c.month,c.date+a]:f.isInteger(c)&&b.isPlainObject(a)&&(c=[a.year,a.month,a.date+c]),{from:e(a),to:e(c)}},c.prototype.withinRange=function(a,b){return a=this.createRange(a.from,a.to),b.pick>=a.from.pick&&b.pick<=a.to.pick},c.prototype.overlapRanges=function(a,b){var c=this;return a=c.createRange(a.from,a.to),b=c.createRange(b.from,b.to),c.withinRange(a,b.from)||c.withinRange(a,b.to)||c.withinRange(b,a.from)||c.withinRange(b,a.to)},c.prototype.now=function(a,b,c){return b=new Date,c&&c.rel&&b.setDate(b.getDate()+c.rel),this.normalize(b,c)},c.prototype.navigate=function(a,c,d){var e,f,g,h,i=b.isArray(c),j=b.isPlainObject(c),k=this.item.view;if(i||j){for(j?(f=c.year,g=c.month,h=c.date):(f=+c[0],g=+c[1],h=+c[2]),d&&d.nav&&k&&k.month!==g&&(f=k.year,g=k.month),e=new Date(f,g+(d&&d.nav?d.nav:0),1),f=e.getFullYear(),g=e.getMonth();new Date(f,g,h).getMonth()!==g;)h-=1;c=[f,g,h]}return c},c.prototype.normalize=function(a){return a.setHours(0,0,0,0),a},c.prototype.measure=function(a,b){var c=this;return b?"string"==typeof b?b=c.parse(a,b):f.isInteger(b)&&(b=c.now(a,b,{rel:b})):b="min"==a?-(1/0):1/0,b},c.prototype.viewset=function(a,b){return this.create([b.year,b.month,1])},c.prototype.validate=function(a,c,d){var e,g,h,i,j=this,k=c,l=d&&d.interval?d.interval:1,m=-1===j.item.enable,n=j.item.min,o=j.item.max,p=m&&j.item.disable.filter(function(a){if(b.isArray(a)){var d=j.create(a).pick;d<c.pick?e=!0:d>c.pick&&(g=!0)}return f.isInteger(a)}).length;if((!d||!d.nav&&!d.defaultValue)&&(!m&&j.disabled(c)||m&&j.disabled(c)&&(p||e||g)||!m&&(c.pick<=n.pick||c.pick>=o.pick)))for(m&&!p&&(!g&&l>0||!e&&0>l)&&(l*=-1);j.disabled(c)&&(Math.abs(l)>1&&(c.month<k.month||c.month>k.month)&&(c=k,l=l>0?1:-1),c.pick<=n.pick?(h=!0,l=1,c=j.create([n.year,n.month,n.date+(c.pick===n.pick?0:-1)])):c.pick>=o.pick&&(i=!0,l=-1,c=j.create([o.year,o.month,o.date+(c.pick===o.pick?0:1)])),!h||!i);)c=j.create([c.year,c.month,c.date+l]);return c},c.prototype.disabled=function(a){var c=this,d=c.item.disable.filter(function(d){return f.isInteger(d)?a.day===(c.settings.firstDay?d:d-1)%7:b.isArray(d)||f.isDate(d)?a.pick===c.create(d).pick:b.isPlainObject(d)?c.withinRange(d,a):void 0});return d=d.length&&!d.filter(function(a){return b.isArray(a)&&"inverted"==a[3]||b.isPlainObject(a)&&a.inverted}).length,-1===c.item.enable?!d:d||a.pick<c.item.min.pick||a.pick>c.item.max.pick},c.prototype.parse=function(a,b,c){var d=this,e={};return b&&"string"==typeof b?(c&&c.format||(c=c||{},c.format=d.settings.format),d.formats.toArray(c.format).map(function(a){var c=d.formats[a],g=c?f.trigger(c,d,[b,e]):a.replace(/^!/,"").length;c&&(e[a]=b.substr(0,g)),b=b.substr(g)}),[e.yyyy||e.yy,+(e.mm||e.m)-1,e.dd||e.d]):b},c.prototype.formats=function(){function a(a,b,c){var d=a.match(/[^\x00-\x7F]+|\w+/)[0];return c.mm||c.m||(c.m=b.indexOf(d)+1),d.length}function b(a){return a.match(/\w+/)[0].length}return{d:function(a,b){return a?f.digits(a):b.date},dd:function(a,b){return a?2:f.lead(b.date)},ddd:function(a,c){return a?b(a):this.settings.weekdaysShort[c.day]},dddd:function(a,c){return a?b(a):this.settings.weekdaysFull[c.day]},m:function(a,b){return a?f.digits(a):b.month+1},mm:function(a,b){return a?2:f.lead(b.month+1)},mmm:function(b,c){var d=this.settings.monthsShort;return b?a(b,d,c):d[c.month]},mmmm:function(b,c){var d=this.settings.monthsFull;return b?a(b,d,c):d[c.month]},yy:function(a,b){return a?2:(""+b.year).slice(2)},yyyy:function(a,b){return a?4:b.year},toArray:function(a){return a.split(/(d{1,4}|m{1,4}|y{4}|yy|!.)/g)},toString:function(a,b){var c=this;return c.formats.toArray(a).map(function(a){return f.trigger(c.formats[a],c,[0,b])||a.replace(/^!/,"")}).join("")}}}(),c.prototype.isDateExact=function(a,c){var d=this;return f.isInteger(a)&&f.isInteger(c)||"boolean"==typeof a&&"boolean"==typeof c?a===c:(f.isDate(a)||b.isArray(a))&&(f.isDate(c)||b.isArray(c))?d.create(a).pick===d.create(c).pick:b.isPlainObject(a)&&b.isPlainObject(c)?d.isDateExact(a.from,c.from)&&d.isDateExact(a.to,c.to):!1},c.prototype.isDateOverlap=function(a,c){var d=this,e=d.settings.firstDay?1:0;return f.isInteger(a)&&(f.isDate(c)||b.isArray(c))?(a=a%7+e,a===d.create(c).day+1):f.isInteger(c)&&(f.isDate(a)||b.isArray(a))?(c=c%7+e,c===d.create(a).day+1):b.isPlainObject(a)&&b.isPlainObject(c)?d.overlapRanges(a,c):!1},c.prototype.flipEnable=function(a){var b=this.item;b.enable=a||(-1==b.enable?1:-1)},c.prototype.deactivate=function(a,c){var d=this,e=d.item.disable.slice(0);return"flip"==c?d.flipEnable():c===!1?(d.flipEnable(1),e=[]):c===!0?(d.flipEnable(-1),e=[]):c.map(function(a){for(var c,g=0;g<e.length;g+=1)if(d.isDateExact(a,e[g])){c=!0;break}c||(f.isInteger(a)||f.isDate(a)||b.isArray(a)||b.isPlainObject(a)&&a.from&&a.to)&&e.push(a)}),e},c.prototype.activate=function(a,c){var d=this,e=d.item.disable,g=e.length;return"flip"==c?d.flipEnable():c===!0?(d.flipEnable(1),e=[]):c===!1?(d.flipEnable(-1),e=[]):c.map(function(a){var c,h,i,j;for(i=0;g>i;i+=1){if(h=e[i],d.isDateExact(h,a)){c=e[i]=null,j=!0;break}if(d.isDateOverlap(h,a)){b.isPlainObject(a)?(a.inverted=!0,c=a):b.isArray(a)?(c=a,c[3]||c.push("inverted")):f.isDate(a)&&(c=[a.getFullYear(),a.getMonth(),a.getDate(),"inverted"]);break}}if(c)for(i=0;g>i;i+=1)if(d.isDateExact(e[i],a)){e[i]=null;break}if(j)for(i=0;g>i;i+=1)if(d.isDateOverlap(e[i],a)){e[i]=null;break}c&&e.push(c)}),e.filter(function(a){return null!=a})},c.prototype.nodes=function(a){var b=this,c=b.settings,g=b.item,h=g.now,i=g.select,j=g.highlight,k=g.view,l=g.disable,m=g.min,n=g.max,o=function(a,b){return c.firstDay&&(a.push(a.shift()),b.push(b.shift())),f.node("thead",f.node("tr",f.group({min:0,max:d-1,i:1,node:"th",item:function(d){return[a[d],c.klass.weekdays,'scope=col title="'+b[d]+'"']}})))}((c.showWeekdaysFull?c.weekdaysFull:c.weekdaysShort).slice(0),c.weekdaysFull.slice(0)),p=function(a){return f.node("div"," ",c.klass["nav"+(a?"Next":"Prev")]+(a&&k.year>=n.year&&k.month>=n.month||!a&&k.year<=m.year&&k.month<=m.month?" "+c.klass.navDisabled:""),"data-nav="+(a||-1)+" "+f.ariaAttr({role:"button",controls:b.$node[0].id+"_table"})+' title="'+(a?c.labelMonthNext:c.labelMonthPrev)+'"')},q=function(){var d=c.showMonthsShort?c.monthsShort:c.monthsFull;return c.selectMonths?f.node("select",f.group({min:0,max:11,i:1,node:"option",item:function(a){return[d[a],0,"value="+a+(k.month==a?" selected":"")+(k.year==m.year&&a<m.month||k.year==n.year&&a>n.month?" disabled":"")]}}),c.klass.selectMonth,(a?"":"disabled")+" "+f.ariaAttr({controls:b.$node[0].id+"_table"})+' title="'+c.labelMonthSelect+'"'):f.node("div",d[k.month],c.klass.month)},r=function(){var d=k.year,e=c.selectYears===!0?5:~~(c.selectYears/2);if(e){var g=m.year,h=n.year,i=d-e,j=d+e;if(g>i&&(j+=g-i,i=g),j>h){var l=i-g,o=j-h;i-=l>o?o:l,j=h}return f.node("select",f.group({min:i,max:j,i:1,node:"option",item:function(a){return[a,0,"value="+a+(d==a?" selected":"")]}}),c.klass.selectYear,(a?"":"disabled")+" "+f.ariaAttr({controls:b.$node[0].id+"_table"})+' title="'+c.labelYearSelect+'"')}return f.node("div",d,c.klass.year)};return f.node("div",(c.selectYears?r()+q():q()+r())+p()+p(1),c.klass.header)+f.node("table",o+f.node("tbody",f.group({min:0,max:e-1,i:1,node:"tr",item:function(a){var e=c.firstDay&&0===b.create([k.year,k.month,1]).day?-7:0;return[f.group({min:d*a-k.day+e+1,max:function(){return this.min+d-1},i:1,node:"td",item:function(a){a=b.create([k.year,k.month,a+(c.firstDay?1:0)]);var d=i&&i.pick==a.pick,e=j&&j.pick==a.pick,g=l&&b.disabled(a)||a.pick<m.pick||a.pick>n.pick,o=f.trigger(b.formats.toString,b,[c.format,a]);return[f.node("div",a.date,function(b){return b.push(k.month==a.month?c.klass.infocus:c.klass.outfocus),h.pick==a.pick&&b.push(c.klass.now),d&&b.push(c.klass.selected),e&&b.push(c.klass.highlighted),g&&b.push(c.klass.disabled),b.join(" ")}([c.klass.day]),"data-pick="+a.pick+" "+f.ariaAttr({role:"gridcell",label:o,selected:d&&b.$node.val()===o?!0:null,activedescendant:e?!0:null,disabled:g?!0:null})),"",f.ariaAttr({role:"presentation"})]}})]}})),c.klass.table,'id="'+b.$node[0].id+'_table" '+f.ariaAttr({role:"grid",controls:b.$node[0].id,readonly:!0}))+f.node("div",f.node("button",c.today,c.klass.buttonToday,"type=button data-pick="+h.pick+(a&&!b.disabled(h)?"":" disabled")+" "+f.ariaAttr({controls:b.$node[0].id}))+f.node("button",c.clear,c.klass.buttonClear,"type=button data-clear=1"+(a?"":" disabled")+" "+f.ariaAttr({controls:b.$node[0].id}))+f.node("button",c.close,c.klass.buttonClose,"type=button data-close=true "+(a?"":" disabled")+" "+f.ariaAttr({controls:b.$node[0].id})),c.klass.footer)},c.defaults=function(a){return{labelMonthNext:"Next month",labelMonthPrev:"Previous month",labelMonthSelect:"Select a month",labelYearSelect:"Select a year",monthsFull:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],weekdaysFull:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],weekdaysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],today:"Today",clear:"Clear",close:"Close",closeOnSelect:!0,closeOnClear:!0,format:"d mmmm, yyyy",klass:{table:a+"table",header:a+"header",navPrev:a+"nav--prev",navNext:a+"nav--next",navDisabled:a+"nav--disabled",month:a+"month",year:a+"year",selectMonth:a+"select--month",selectYear:a+"select--year",weekdays:a+"weekday",day:a+"day",disabled:a+"day--disabled",selected:a+"day--selected",highlighted:a+"day--highlighted",now:a+"day--today",infocus:a+"day--infocus",outfocus:a+"day--outfocus",footer:a+"footer",buttonClear:a+"button--clear",buttonToday:a+"button--today",buttonClose:a+"button--close"}}}(a.klasses().picker+"__"),a.extend("pickadate",c)});
/**
* @version: 2.1.23
* @author: Dan Grossman http://www.dangrossman.info/
* @copyright: Copyright (c) 2012-2016 Dan Grossman. All rights reserved.
* @license: Licensed under the MIT license. See http://www.opensource.org/licenses/mit-license.php
* @website: https://www.improvely.com/
*/
// Follow the UMD template https://github.com/umdjs/umd/blob/master/templates/returnExportsGlobal.js
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Make globaly available as well
        define(['moment', 'jquery'], function (moment, jquery) {
            return (root.daterangepicker = factory(moment, jquery));
        });
    } else if (typeof module === 'object' && module.exports) {
        // Node / Browserify
        //isomorphic issue
        var jQuery = (typeof window != 'undefined') ? window.jQuery : undefined;
        if (!jQuery) {
            jQuery = require('jquery');
            if (!jQuery.fn) jQuery.fn = {};
        }
        module.exports = factory(require('moment'), jQuery);
    } else {
        // Browser globals
        root.daterangepicker = factory(root.moment, root.jQuery);
    }
}(this, function(moment, $) {
    var DateRangePicker = function(element, options, cb) {

        //default settings for options
        this.parentEl = 'body';
        this.element = $(element);
        this.startDate = moment().startOf('day');
        this.endDate = moment().endOf('day');
        this.minDate = false;
        this.maxDate = false;
        this.dateLimit = false;
        this.autoApply = false;
        this.singleDatePicker = false;
        this.showDropdowns = false;
        this.showWeekNumbers = false;
        this.showISOWeekNumbers = false;
        this.timePicker = false;
        this.timePicker24Hour = false;
        this.timePickerIncrement = 1;
        this.timePickerSeconds = false;
        this.linkedCalendars = true;
        this.autoUpdateInput = true;
        this.alwaysShowCalendars = false;
        this.ranges = {};

        this.opens = 'right';
        if (this.element.hasClass('pull-right'))
            this.opens = 'left';

        this.drops = 'down';
        if (this.element.hasClass('dropup'))
            this.drops = 'up';

        this.buttonClasses = 'btn btn-sm';
        this.applyClass = 'btn-success';
        this.cancelClass = 'btn-default';

        this.locale = {
            direction: 'ltr',
            format: 'MM/DD/YYYY',
            separator: ' - ',
            applyLabel: 'Apply',
            startLabel: 'Start date:',
            endLabel: 'End date:',
            cancelLabel: 'Cancel',
            weekLabel: 'W',
            customRangeLabel: 'Custom Range',
            daysOfWeek: moment.weekdaysMin(),
            monthNames: moment.monthsShort(),
            firstDay: moment.localeData().firstDayOfWeek()
        };

        this.callback = function() { };

        //some state information
        this.isShowing = false;
        this.leftCalendar = {};
        this.rightCalendar = {};

        //custom options from user
        if (typeof options !== 'object' || options === null)
            options = {};

        //allow setting options with data attributes
        //data-api options will be overwritten with custom javascript options
        options = $.extend(this.element.data(), options);

        //html template for the picker UI
        if (typeof options.template !== 'string' && !(options.template instanceof $))
            options.template = '<div class="daterangepicker dropdown-menu">' +
                '<div class="calendars">' +
                    '<div class="calendar left">' +
                        '<div class="calendar-table"></div>' +
                        '<div class="daterangepicker_input">' +
                            '<div class="calendar-time">' +
                                '<div></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +

                    '<div class="calendar right">' +
                        '<div class="calendar-table"></div>' +
                        '<div class="daterangepicker_input">' +
                            '<div class="calendar-time">' +
                                '<div></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +

                '<div class="ranges">' +
                    '<div class="daterangepicker-inputs">' +
                        '<div class="daterangepicker_input">' +
                            '<span class="start-date-label"></span>' +
                            '<input class="form-control" type="text" name="daterangepicker_start" value="" />' +
                            '<i class="icon-calendar3"></i>' +
                        '</div>' +
                        '<div class="daterangepicker_input">' +
                            '<span class="end-date-label"></span>' +
                            '<input class="form-control" type="text" name="daterangepicker_end" value="" />' +
                            '<i class="icon-calendar3"></i>' +
                        '</div>' +
                    '</div>' +
                    '<div class="range_inputs">' +
                        '<button class="applyBtn" disabled="disabled" type="button"></button> ' +
                        '<button class="cancelBtn" type="button"></button>' +
                    '</div>' +
                '</div>' +
            '</div>';

        this.parentEl = (options.parentEl && $(options.parentEl).length) ? $(options.parentEl) : $(this.parentEl);
        this.container = $(options.template).appendTo(this.parentEl);

        //
        // handle all the possible options overriding defaults
        //

        if (typeof options.locale === 'object') {

            if (typeof options.locale.direction === 'string')
                this.locale.direction = options.locale.direction;

            if (typeof options.locale.format === 'string')
                this.locale.format = options.locale.format;

            if (typeof options.locale.separator === 'string')
                this.locale.separator = options.locale.separator;

            if (typeof options.locale.daysOfWeek === 'object')
                this.locale.daysOfWeek = options.locale.daysOfWeek.slice();

            if (typeof options.locale.monthNames === 'object')
              this.locale.monthNames = options.locale.monthNames.slice();

            if (typeof options.locale.firstDay === 'number')
              this.locale.firstDay = options.locale.firstDay;

            if (typeof options.locale.applyLabel === 'string')
              this.locale.applyLabel = options.locale.applyLabel;

            if (typeof options.locale.startLabel === 'string')
              this.locale.startLabel = options.locale.startLabel;

            if (typeof options.locale.endLabel === 'string')
              this.locale.endLabel = options.locale.endLabel;

            if (typeof options.locale.cancelLabel === 'string')
              this.locale.cancelLabel = options.locale.cancelLabel;

            if (typeof options.locale.weekLabel === 'string')
              this.locale.weekLabel = options.locale.weekLabel;

            if (typeof options.locale.customRangeLabel === 'string')
              this.locale.customRangeLabel = options.locale.customRangeLabel;

        }
        this.container.addClass(this.locale.direction);

        if (typeof options.startDate === 'string')
            this.startDate = moment(options.startDate, this.locale.format);

        if (typeof options.endDate === 'string')
            this.endDate = moment(options.endDate, this.locale.format);

        if (typeof options.minDate === 'string')
            this.minDate = moment(options.minDate, this.locale.format);

        if (typeof options.maxDate === 'string')
            this.maxDate = moment(options.maxDate, this.locale.format);

        if (typeof options.startDate === 'object')
            this.startDate = moment(options.startDate);

        if (typeof options.endDate === 'object')
            this.endDate = moment(options.endDate);

        if (typeof options.minDate === 'object')
            this.minDate = moment(options.minDate);

        if (typeof options.maxDate === 'object')
            this.maxDate = moment(options.maxDate);

        // sanity check for bad options
        if (this.minDate && this.startDate.isBefore(this.minDate))
            this.startDate = this.minDate.clone();

        // sanity check for bad options
        if (this.maxDate && this.endDate.isAfter(this.maxDate))
            this.endDate = this.maxDate.clone();

        if (typeof options.applyClass === 'string')
            this.applyClass = options.applyClass;

        if (typeof options.cancelClass === 'string')
            this.cancelClass = options.cancelClass;

        if (typeof options.dateLimit === 'object')
            this.dateLimit = options.dateLimit;

        if (typeof options.opens === 'string')
            this.opens = options.opens;

        if (typeof options.drops === 'string')
            this.drops = options.drops;

        if (typeof options.showWeekNumbers === 'boolean')
            this.showWeekNumbers = options.showWeekNumbers;

        if (typeof options.showISOWeekNumbers === 'boolean')
            this.showISOWeekNumbers = options.showISOWeekNumbers;

        if (typeof options.buttonClasses === 'string')
            this.buttonClasses = options.buttonClasses;

        if (typeof options.buttonClasses === 'object')
            this.buttonClasses = options.buttonClasses.join(' ');

        if (typeof options.showDropdowns === 'boolean')
            this.showDropdowns = options.showDropdowns;

        if (typeof options.singleDatePicker === 'boolean') {
            this.singleDatePicker = options.singleDatePicker;
            if (this.singleDatePicker)
                this.endDate = this.startDate.clone();
        }

        if (typeof options.timePicker === 'boolean')
            this.timePicker = options.timePicker;

        if (typeof options.timePickerSeconds === 'boolean')
            this.timePickerSeconds = options.timePickerSeconds;

        if (typeof options.timePickerIncrement === 'number')
            this.timePickerIncrement = options.timePickerIncrement;

        if (typeof options.timePicker24Hour === 'boolean')
            this.timePicker24Hour = options.timePicker24Hour;

        if (typeof options.autoApply === 'boolean')
            this.autoApply = options.autoApply;

        if (typeof options.autoUpdateInput === 'boolean')
            this.autoUpdateInput = options.autoUpdateInput;

        if (typeof options.linkedCalendars === 'boolean')
            this.linkedCalendars = options.linkedCalendars;

        if (typeof options.isInvalidDate === 'function')
            this.isInvalidDate = options.isInvalidDate;

        if (typeof options.isCustomDate === 'function')
            this.isCustomDate = options.isCustomDate;

        if (typeof options.alwaysShowCalendars === 'boolean')
            this.alwaysShowCalendars = options.alwaysShowCalendars;

        // update day names order to firstDay
        if (this.locale.firstDay != 0) {
            var iterator = this.locale.firstDay;
            while (iterator > 0) {
                this.locale.daysOfWeek.push(this.locale.daysOfWeek.shift());
                iterator--;
            }
        }

        var start, end, range;

        //if no start/end dates set, check if an input element contains initial values
        if (typeof options.startDate === 'undefined' && typeof options.endDate === 'undefined') {
            if ($(this.element).is('input[type=text]')) {
                var val = $(this.element).val(),
                    split = val.split(this.locale.separator);

                start = end = null;

                if (split.length == 2) {
                    start = moment(split[0], this.locale.format);
                    end = moment(split[1], this.locale.format);
                } else if (this.singleDatePicker && val !== "") {
                    start = moment(val, this.locale.format);
                    end = moment(val, this.locale.format);
                }
                if (start !== null && end !== null) {
                    this.setStartDate(start);
                    this.setEndDate(end);
                }
            }
        }

        if (typeof options.ranges === 'object') {
            for (range in options.ranges) {

                if (typeof options.ranges[range][0] === 'string')
                    start = moment(options.ranges[range][0], this.locale.format);
                else
                    start = moment(options.ranges[range][0]);

                if (typeof options.ranges[range][1] === 'string')
                    end = moment(options.ranges[range][1], this.locale.format);
                else
                    end = moment(options.ranges[range][1]);

                // If the start or end date exceed those allowed by the minDate or dateLimit
                // options, shorten the range to the allowable period.
                if (this.minDate && start.isBefore(this.minDate))
                    start = this.minDate.clone();

                var maxDate = this.maxDate;
                if (this.dateLimit && maxDate && start.clone().add(this.dateLimit).isAfter(maxDate))
                    maxDate = start.clone().add(this.dateLimit);
                if (maxDate && end.isAfter(maxDate))
                    end = maxDate.clone();

                // If the end of the range is before the minimum or the start of the range is
                // after the maximum, don't display this range option at all.
                if ((this.minDate && end.isBefore(this.minDate, this.timepicker ? 'minute' : 'day'))
                  || (maxDate && start.isAfter(maxDate, this.timepicker ? 'minute' : 'day')))
                    continue;

                //Support unicode chars in the range names.
                var elem = document.createElement('textarea');
                elem.innerHTML = range;
                var rangeHtml = elem.value;

                this.ranges[rangeHtml] = [start, end];
            }

            var list = '<ul>';
            for (range in this.ranges) {
                list += '<li data-range-key="' + range + '">' + range + '</li>';
            }
            list += '<li data-range-key="' + this.locale.customRangeLabel + '">' + this.locale.customRangeLabel + '</li>';
            list += '</ul>';
            this.container.find('.ranges').prepend(list);
        }

        if (typeof cb === 'function') {
            this.callback = cb;
        }

        if (!this.timePicker) {
            this.startDate = this.startDate.startOf('day');
            this.endDate = this.endDate.endOf('day');
            this.container.find('.calendar-time').hide();
        }

        //can't be used together for now
        if (this.timePicker && this.autoApply)
            this.autoApply = false;

        if (this.autoApply && typeof options.ranges !== 'object') {
            this.container.find('.ranges').hide();
        } else if (this.autoApply) {
            this.container.find('.applyBtn, .cancelBtn').addClass('hide');
        }

        if (this.singleDatePicker) {
            this.container.addClass('single');
            this.container.find('.calendar.left').addClass('single');
            this.container.find('.calendar.left').show();
            this.container.find('.calendar.right').hide();
            this.container.find('.daterangepicker_input input, .daterangepicker_input > i').hide();
            if (this.timePicker) {
                this.container.find('.ranges ul').hide();
            } else {
                this.container.find('.ranges').hide();
            }
        }

        if ((typeof options.ranges === 'undefined' && !this.singleDatePicker) || this.alwaysShowCalendars) {
            this.container.addClass('show-calendar');
        }

        this.container.addClass('opens' + this.opens);

        //swap the position of the predefined ranges if opens right
        if (typeof options.ranges !== 'undefined' && this.opens == 'right') {
            this.container.find('.ranges').prependTo( this.container.find('.calendar.left').parent() );
        }

        //apply CSS classes and labels to buttons
        this.container.find('.applyBtn, .cancelBtn').addClass(this.buttonClasses);
        if (this.applyClass.length)
            this.container.find('.applyBtn').addClass(this.applyClass);
        if (this.cancelClass.length)
            this.container.find('.cancelBtn').addClass(this.cancelClass);
        this.container.find('.applyBtn').html(this.locale.applyLabel);
        this.container.find('.cancelBtn').html(this.locale.cancelLabel);

        //apply CSS classes and labels to text labels
        this.container.find('.start-date-label').html(this.locale.startLabel);
        this.container.find('.end-date-label').html(this.locale.endLabel);

        //
        // event listeners
        //

        this.container.find('.calendar')
            .on('click.daterangepicker', '.prev', $.proxy(this.clickPrev, this))
            .on('click.daterangepicker', '.next', $.proxy(this.clickNext, this))
            .on('click.daterangepicker', 'td.available', $.proxy(this.clickDate, this))
            .on('mouseenter.daterangepicker', 'td.available', $.proxy(this.hoverDate, this))
            .on('mouseleave.daterangepicker', 'td.available', $.proxy(this.updateFormInputs, this))
            .on('change.daterangepicker', 'select.yearselect', $.proxy(this.monthOrYearChanged, this))
            .on('change.daterangepicker', 'select.monthselect', $.proxy(this.monthOrYearChanged, this))
            .on('change.daterangepicker', 'select.hourselect,select.minuteselect,select.secondselect,select.ampmselect', $.proxy(this.timeChanged, this))

        this.container.find('.ranges')
            .on('click.daterangepicker', '.daterangepicker_input input', $.proxy(this.showCalendars, this))
            .on('focus.daterangepicker', '.daterangepicker_input input', $.proxy(this.formInputsFocused, this))
            .on('blur.daterangepicker', '.daterangepicker_input input', $.proxy(this.formInputsBlurred, this))
            .on('change.daterangepicker', '.daterangepicker_input input', $.proxy(this.formInputsChanged, this))
            .on('click.daterangepicker', 'button.applyBtn', $.proxy(this.clickApply, this))
            .on('click.daterangepicker', 'button.cancelBtn', $.proxy(this.clickCancel, this))
            .on('click.daterangepicker', 'li', $.proxy(this.clickRange, this))
            .on('mouseenter.daterangepicker', 'li', $.proxy(this.hoverRange, this))
            .on('mouseleave.daterangepicker', 'li', $.proxy(this.updateFormInputs, this));

        if (this.element.is('input') || this.element.is('button')) {
            this.element.on({
                'click.daterangepicker': $.proxy(this.show, this),
                'focus.daterangepicker': $.proxy(this.show, this),
                'keyup.daterangepicker': $.proxy(this.elementChanged, this),
                'keydown.daterangepicker': $.proxy(this.keydown, this)
            });
        } else {
            this.element.on('click.daterangepicker', $.proxy(this.toggle, this));
        }

        //
        // if attached to a text input, set the initial value
        //

        if (this.element.is('input') && !this.singleDatePicker && this.autoUpdateInput) {
            this.element.val(this.startDate.format(this.locale.format) + this.locale.separator + this.endDate.format(this.locale.format));
            this.element.trigger('change');
        } else if (this.element.is('input') && this.autoUpdateInput) {
            this.element.val(this.startDate.format(this.locale.format));
            this.element.trigger('change');
        }

    };

    DateRangePicker.prototype = {

        constructor: DateRangePicker,

        setStartDate: function(startDate) {
            if (typeof startDate === 'string')
                this.startDate = moment(startDate, this.locale.format);

            if (typeof startDate === 'object')
                this.startDate = moment(startDate);

            if (!this.timePicker)
                this.startDate = this.startDate.startOf('day');

            if (this.timePicker && this.timePickerIncrement)
                this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);

            if (this.minDate && this.startDate.isBefore(this.minDate)) {
                this.startDate = this.minDate;
                if (this.timePicker && this.timePickerIncrement)
                    this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
            }

            if (this.maxDate && this.startDate.isAfter(this.maxDate)) {
                this.startDate = this.maxDate;
                if (this.timePicker && this.timePickerIncrement)
                    this.startDate.minute(Math.floor(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
            }

            if (!this.isShowing)
                this.updateElement();

            this.updateMonthsInView();
        },

        setEndDate: function(endDate) {
            if (typeof endDate === 'string')
                this.endDate = moment(endDate, this.locale.format);

            if (typeof endDate === 'object')
                this.endDate = moment(endDate);

            if (!this.timePicker)
                this.endDate = this.endDate.endOf('day');

            if (this.timePicker && this.timePickerIncrement)
                this.endDate.minute(Math.round(this.endDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);

            if (this.endDate.isBefore(this.startDate))
                this.endDate = this.startDate.clone();

            if (this.maxDate && this.endDate.isAfter(this.maxDate))
                this.endDate = this.maxDate;

            if (this.dateLimit && this.startDate.clone().add(this.dateLimit).isBefore(this.endDate))
                this.endDate = this.startDate.clone().add(this.dateLimit);

            this.previousRightTime = this.endDate.clone();

            if (!this.isShowing)
                this.updateElement();

            this.updateMonthsInView();
        },

        isInvalidDate: function() {
            return false;
        },

        isCustomDate: function() {
            return false;
        },

        updateView: function() {
            if (this.timePicker) {
                this.renderTimePicker('left');
                this.renderTimePicker('right');
                if (!this.endDate) {
                    this.container.find('.right .calendar-time select').attr('disabled', 'disabled').addClass('disabled');
                } else {
                    this.container.find('.right .calendar-time select').removeAttr('disabled').removeClass('disabled');
                }
            }
            if (this.endDate) {
                this.container.find('input[name="daterangepicker_end"]').removeClass('active');
                this.container.find('input[name="daterangepicker_start"]').addClass('active');
            } else {
                this.container.find('input[name="daterangepicker_end"]').addClass('active');
                this.container.find('input[name="daterangepicker_start"]').removeClass('active');
            }
            this.updateMonthsInView();
            this.updateCalendars();
            this.updateFormInputs();
        },

        updateMonthsInView: function() {
            if (this.endDate) {

                //if both dates are visible already, do nothing
                if (!this.singleDatePicker && this.leftCalendar.month && this.rightCalendar.month &&
                    (this.startDate.format('YYYY-MM') == this.leftCalendar.month.format('YYYY-MM') || this.startDate.format('YYYY-MM') == this.rightCalendar.month.format('YYYY-MM'))
                    &&
                    (this.endDate.format('YYYY-MM') == this.leftCalendar.month.format('YYYY-MM') || this.endDate.format('YYYY-MM') == this.rightCalendar.month.format('YYYY-MM'))
                    ) {
                    return;
                }

                this.leftCalendar.month = this.startDate.clone().date(2);
                if (!this.linkedCalendars && (this.endDate.month() != this.startDate.month() || this.endDate.year() != this.startDate.year())) {
                    this.rightCalendar.month = this.endDate.clone().date(2);
                } else {
                    this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
                }

            } else {
                if (this.leftCalendar.month.format('YYYY-MM') != this.startDate.format('YYYY-MM') && this.rightCalendar.month.format('YYYY-MM') != this.startDate.format('YYYY-MM')) {
                    this.leftCalendar.month = this.startDate.clone().date(2);
                    this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
                }
            }
            if (this.maxDate && this.linkedCalendars && !this.singleDatePicker && this.rightCalendar.month > this.maxDate) {
              this.rightCalendar.month = this.maxDate.clone().date(2);
              this.leftCalendar.month = this.maxDate.clone().date(2).subtract(1, 'month');
            }
        },

        updateCalendars: function() {

            if (this.timePicker) {
                var hour, minute, second;
                if (this.endDate) {
                    hour = parseInt(this.container.find('.left .hourselect').val(), 10);
                    minute = parseInt(this.container.find('.left .minuteselect').val(), 10);
                    second = this.timePickerSeconds ? parseInt(this.container.find('.left .secondselect').val(), 10) : 0;
                    if (!this.timePicker24Hour) {
                        var ampm = this.container.find('.left .ampmselect').val();
                        if (ampm === 'PM' && hour < 12)
                            hour += 12;
                        if (ampm === 'AM' && hour === 12)
                            hour = 0;
                    }
                } else {
                    hour = parseInt(this.container.find('.right .hourselect').val(), 10);
                    minute = parseInt(this.container.find('.right .minuteselect').val(), 10);
                    second = this.timePickerSeconds ? parseInt(this.container.find('.right .secondselect').val(), 10) : 0;
                    if (!this.timePicker24Hour) {
                        var ampm = this.container.find('.right .ampmselect').val();
                        if (ampm === 'PM' && hour < 12)
                            hour += 12;
                        if (ampm === 'AM' && hour === 12)
                            hour = 0;
                    }
                }
                this.leftCalendar.month.hour(hour).minute(minute).second(second);
                this.rightCalendar.month.hour(hour).minute(minute).second(second);
            }

            this.renderCalendar('left');
            this.renderCalendar('right');

            //highlight any predefined range matching the current start and end dates
            this.container.find('.ranges li').removeClass('active');
            if (this.endDate == null) return;

            this.calculateChosenLabel();
        },

        renderCalendar: function(side) {

            //
            // Build the matrix of dates that will populate the calendar
            //

            var calendar = side == 'left' ? this.leftCalendar : this.rightCalendar;
            var month = calendar.month.month();
            var year = calendar.month.year();
            var hour = calendar.month.hour();
            var minute = calendar.month.minute();
            var second = calendar.month.second();
            var daysInMonth = moment([year, month]).daysInMonth();
            var firstDay = moment([year, month, 1]);
            var lastDay = moment([year, month, daysInMonth]);
            var lastMonth = moment(firstDay).subtract(1, 'month').month();
            var lastYear = moment(firstDay).subtract(1, 'month').year();
            var daysInLastMonth = moment([lastYear, lastMonth]).daysInMonth();
            var dayOfWeek = firstDay.day();

            //initialize a 6 rows x 7 columns array for the calendar
            var calendar = [];
            calendar.firstDay = firstDay;
            calendar.lastDay = lastDay;

            for (var i = 0; i < 6; i++) {
                calendar[i] = [];
            }

            //populate the calendar with date objects
            var startDay = daysInLastMonth - dayOfWeek + this.locale.firstDay + 1;
            if (startDay > daysInLastMonth)
                startDay -= 7;

            if (dayOfWeek == this.locale.firstDay)
                startDay = daysInLastMonth - 6;

            var curDate = moment([lastYear, lastMonth, startDay, 12, minute, second]);

            var col, row;
            for (var i = 0, col = 0, row = 0; i < 42; i++, col++, curDate = moment(curDate).add(24, 'hour')) {
                if (i > 0 && col % 7 === 0) {
                    col = 0;
                    row++;
                }
                calendar[row][col] = curDate.clone().hour(hour).minute(minute).second(second);
                curDate.hour(12);

                if (this.minDate && calendar[row][col].format('YYYY-MM-DD') == this.minDate.format('YYYY-MM-DD') && calendar[row][col].isBefore(this.minDate) && side == 'left') {
                    calendar[row][col] = this.minDate.clone();
                }

                if (this.maxDate && calendar[row][col].format('YYYY-MM-DD') == this.maxDate.format('YYYY-MM-DD') && calendar[row][col].isAfter(this.maxDate) && side == 'right') {
                    calendar[row][col] = this.maxDate.clone();
                }

            }

            //make the calendar object available to hoverDate/clickDate
            if (side == 'left') {
                this.leftCalendar.calendar = calendar;
            } else {
                this.rightCalendar.calendar = calendar;
            }

            //
            // Display the calendar
            //

            var minDate = side == 'left' ? this.minDate : this.startDate;
            var maxDate = this.maxDate;
            var selected = side == 'left' ? this.startDate : this.endDate;
            var arrow = this.locale.direction == 'ltr' ? {left: 'arrow-left32', right: 'arrow-right32'} : {left: 'arrow-right32', right: 'arrow-left32'};

            var html = '<table class="table-condensed">';
            html += '<thead>';
            html += '<tr>';

            // add empty cell for week number
            if (this.showWeekNumbers || this.showISOWeekNumbers)
                html += '<th></th>';

            if ((!minDate || minDate.isBefore(calendar.firstDay)) && (!this.linkedCalendars || side == 'left')) {
                html += '<th class="prev available"><i class="icon-' + arrow.left + '"></i></th>';
            } else {
                html += '<th></th>';
            }

            var dateHtml = this.locale.monthNames[calendar[1][1].month()] + calendar[1][1].format(" YYYY");

            if (this.showDropdowns) {
                var currentMonth = calendar[1][1].month();
                var currentYear = calendar[1][1].year();
                var maxYear = (maxDate && maxDate.year()) || (currentYear + 5);
                var minYear = (minDate && minDate.year()) || (currentYear - 50);
                var inMinYear = currentYear == minYear;
                var inMaxYear = currentYear == maxYear;

                var monthHtml = '<select class="monthselect form-control input-sm">';
                for (var m = 0; m < 12; m++) {
                    if ((!inMinYear || m >= minDate.month()) && (!inMaxYear || m <= maxDate.month())) {
                        monthHtml += "<option value='" + m + "'" +
                            (m === currentMonth ? " selected='selected'" : "") +
                            ">" + this.locale.monthNames[m] + "</option>";
                    } else {
                        monthHtml += "<option value='" + m + "'" +
                            (m === currentMonth ? " selected='selected'" : "") +
                            " disabled='disabled'>" + this.locale.monthNames[m] + "</option>";
                    }
                }
                monthHtml += "</select>";

                var yearHtml = '<select class="yearselect form-control input-sm">';
                for (var y = minYear; y <= maxYear; y++) {
                    yearHtml += '<option value="' + y + '"' +
                        (y === currentYear ? ' selected="selected"' : '') +
                        '>' + y + '</option>';
                }
                yearHtml += '</select>';

                dateHtml = monthHtml + yearHtml;
            }

            html += '<th colspan="5" class="month">' + dateHtml + '</th>';
            if ((!maxDate || maxDate.isAfter(calendar.lastDay)) && (!this.linkedCalendars || side == 'right' || this.singleDatePicker)) {
                html += '<th class="next available"><i class="icon-' + arrow.right + '"></i></th>';
            } else {
                html += '<th></th>';
            }

            html += '</tr>';
            html += '<tr>';

            // add week number label
            if (this.showWeekNumbers || this.showISOWeekNumbers)
                html += '<th class="week">' + this.locale.weekLabel + '</th>';

            $.each(this.locale.daysOfWeek, function(index, dayOfWeek) {
                html += '<th>' + dayOfWeek + '</th>';
            });

            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //adjust maxDate to reflect the dateLimit setting in order to
            //grey out end dates beyond the dateLimit
            if (this.endDate == null && this.dateLimit) {
                var maxLimit = this.startDate.clone().add(this.dateLimit).endOf('day');
                if (!maxDate || maxLimit.isBefore(maxDate)) {
                    maxDate = maxLimit;
                }
            }

            for (var row = 0; row < 6; row++) {
                html += '<tr>';

                // add week number
                if (this.showWeekNumbers)
                    html += '<td class="week">' + calendar[row][0].week() + '</td>';
                else if (this.showISOWeekNumbers)
                    html += '<td class="week">' + calendar[row][0].isoWeek() + '</td>';

                for (var col = 0; col < 7; col++) {

                    var classes = [];

                    //highlight today's date
                    if (calendar[row][col].isSame(new Date(), "day"))
                        classes.push('today');

                    //highlight weekends
                    if (calendar[row][col].isoWeekday() > 5)
                        classes.push('weekend');

                    //grey out the dates in other months displayed at beginning and end of this calendar
                    if (calendar[row][col].month() != calendar[1][1].month())
                        classes.push('off');

                    //don't allow selection of dates before the minimum date
                    if (this.minDate && calendar[row][col].isBefore(this.minDate, 'day'))
                        classes.push('off', 'disabled');

                    //don't allow selection of dates after the maximum date
                    if (maxDate && calendar[row][col].isAfter(maxDate, 'day'))
                        classes.push('off', 'disabled');

                    //don't allow selection of date if a custom function decides it's invalid
                    if (this.isInvalidDate(calendar[row][col]))
                        classes.push('off', 'disabled');

                    //highlight the currently selected start date
                    if (calendar[row][col].format('YYYY-MM-DD') == this.startDate.format('YYYY-MM-DD'))
                        classes.push('active', 'start-date');

                    //highlight the currently selected end date
                    if (this.endDate != null && calendar[row][col].format('YYYY-MM-DD') == this.endDate.format('YYYY-MM-DD'))
                        classes.push('active', 'end-date');

                    //highlight dates in-between the selected dates
                    if (this.endDate != null && calendar[row][col] > this.startDate && calendar[row][col] < this.endDate)
                        classes.push('in-range');

                    //apply custom classes for this date
                    var isCustom = this.isCustomDate(calendar[row][col]);
                    if (isCustom !== false) {
                        if (typeof isCustom === 'string')
                            classes.push(isCustom);
                        else
                            Array.prototype.push.apply(classes, isCustom);
                    }

                    var cname = '', disabled = false;
                    for (var i = 0; i < classes.length; i++) {
                        cname += classes[i] + ' ';
                        if (classes[i] == 'disabled')
                            disabled = true;
                    }
                    if (!disabled)
                        cname += 'available';

                    html += '<td class="' + cname.replace(/^\s+|\s+$/g, '') + '" data-title="' + 'r' + row + 'c' + col + '">' + calendar[row][col].date() + '</td>';

                }
                html += '</tr>';
            }

            html += '</tbody>';
            html += '</table>';

            this.container.find('.calendar.' + side + ' .calendar-table').html(html);

        },

        renderTimePicker: function(side) {

            // Don't bother updating the time picker if it's currently disabled
            // because an end date hasn't been clicked yet
            if (side == 'right' && !this.endDate) return;

            var html, selected, minDate, maxDate = this.maxDate;

            if (this.dateLimit && (!this.maxDate || this.startDate.clone().add(this.dateLimit).isAfter(this.maxDate)))
                maxDate = this.startDate.clone().add(this.dateLimit);

            if (side == 'left') {
                selected = this.startDate.clone();
                minDate = this.minDate;
            } else if (side == 'right') {
                selected = this.endDate.clone();
                minDate = this.startDate;

                //Preserve the time already selected
                var timeSelector = this.container.find('.calendar.right .calendar-time div');
                if (!this.endDate && timeSelector.html() != '') {

                    selected.hour(timeSelector.find('.hourselect option:selected').val() || selected.hour());
                    selected.minute(timeSelector.find('.minuteselect option:selected').val() || selected.minute());
                    selected.second(timeSelector.find('.secondselect option:selected').val() || selected.second());

                    if (!this.timePicker24Hour) {
                        var ampm = timeSelector.find('.ampmselect option:selected').val();
                        if (ampm === 'PM' && selected.hour() < 12)
                            selected.hour(selected.hour() + 12);
                        if (ampm === 'AM' && selected.hour() === 12)
                            selected.hour(0);
                    }

                }

                if (selected.isBefore(this.startDate))
                    selected = this.startDate.clone();

                if (maxDate && selected.isAfter(maxDate))
                    selected = maxDate.clone();

            }

            //
            // hours
            //

            html = '<select class="hourselect form-control input-sm">';

            var start = this.timePicker24Hour ? 0 : 1;
            var end = this.timePicker24Hour ? 23 : 12;

            for (var i = start; i <= end; i++) {
                var i_in_24 = i;
                if (!this.timePicker24Hour)
                    i_in_24 = selected.hour() >= 12 ? (i == 12 ? 12 : i + 12) : (i == 12 ? 0 : i);

                var time = selected.clone().hour(i_in_24);
                var disabled = false;
                if (minDate && time.minute(59).isBefore(minDate))
                    disabled = true;
                if (maxDate && time.minute(0).isAfter(maxDate))
                    disabled = true;

                if (i_in_24 == selected.hour() && !disabled) {
                    html += '<option value="' + i + '" selected="selected">' + i + '</option>';
                } else if (disabled) {
                    html += '<option value="' + i + '" disabled="disabled" class="disabled">' + i + '</option>';
                } else {
                    html += '<option value="' + i + '">' + i + '</option>';
                }
            }

            html += '</select> ';

            //
            // minutes
            //

            html += ': <select class="minuteselect form-control input-sm">';

            for (var i = 0; i < 60; i += this.timePickerIncrement) {
                var padded = i < 10 ? '0' + i : i;
                var time = selected.clone().minute(i);

                var disabled = false;
                if (minDate && time.second(59).isBefore(minDate))
                    disabled = true;
                if (maxDate && time.second(0).isAfter(maxDate))
                    disabled = true;

                if (selected.minute() == i && !disabled) {
                    html += '<option value="' + i + '" selected="selected">' + padded + '</option>';
                } else if (disabled) {
                    html += '<option value="' + i + '" disabled="disabled" class="disabled">' + padded + '</option>';
                } else {
                    html += '<option value="' + i + '">' + padded + '</option>';
                }
            }

            html += '</select> ';

            //
            // seconds
            //

            if (this.timePickerSeconds) {
                html += ': <select class="secondselect form-control input-sm">';

                for (var i = 0; i < 60; i++) {
                    var padded = i < 10 ? '0' + i : i;
                    var time = selected.clone().second(i);

                    var disabled = false;
                    if (minDate && time.isBefore(minDate))
                        disabled = true;
                    if (maxDate && time.isAfter(maxDate))
                        disabled = true;

                    if (selected.second() == i && !disabled) {
                        html += '<option value="' + i + '" selected="selected">' + padded + '</option>';
                    } else if (disabled) {
                        html += '<option value="' + i + '" disabled="disabled" class="disabled">' + padded + '</option>';
                    } else {
                        html += '<option value="' + i + '">' + padded + '</option>';
                    }
                }

                html += '</select> ';
            }

            //
            // AM/PM
            //

            if (!this.timePicker24Hour) {
                html += '<select class="ampmselect form-control input-sm">';

                var am_html = '';
                var pm_html = '';

                if (minDate && selected.clone().hour(12).minute(0).second(0).isBefore(minDate))
                    am_html = ' disabled="disabled" class="disabled"';

                if (maxDate && selected.clone().hour(0).minute(0).second(0).isAfter(maxDate))
                    pm_html = ' disabled="disabled" class="disabled"';

                if (selected.hour() >= 12) {
                    html += '<option value="AM"' + am_html + '>AM</option><option value="PM" selected="selected"' + pm_html + '>PM</option>';
                } else {
                    html += '<option value="AM" selected="selected"' + am_html + '>AM</option><option value="PM"' + pm_html + '>PM</option>';
                }

                html += '</select>';
            }

            this.container.find('.calendar.' + side + ' .calendar-time div').html(html);

        },

        updateFormInputs: function() {

            //ignore mouse movements while an above-calendar text input has focus
            if (this.container.find('input[name=daterangepicker_start]').is(":focus") || this.container.find('input[name=daterangepicker_end]').is(":focus"))
                return;

            this.container.find('input[name=daterangepicker_start]').val(this.startDate.format(this.locale.format));
            if (this.endDate)
                this.container.find('input[name=daterangepicker_end]').val(this.endDate.format(this.locale.format));

            if (this.singleDatePicker || (this.endDate && (this.startDate.isBefore(this.endDate) || this.startDate.isSame(this.endDate)))) {
                this.container.find('button.applyBtn').removeAttr('disabled');
            } else {
                this.container.find('button.applyBtn').attr('disabled', 'disabled');
            }

        },

        move: function() {
            var parentOffset = { top: 0, left: 0 },
                containerTop;
            var parentRightEdge = $(window).width();
            if (!this.parentEl.is('body')) {
                parentOffset = {
                    top: this.parentEl.offset().top - this.parentEl.scrollTop(),
                    left: this.parentEl.offset().left - this.parentEl.scrollLeft()
                };
                parentRightEdge = this.parentEl[0].clientWidth + this.parentEl.offset().left;
            }

            if (this.drops == 'up')
                containerTop = this.element.offset().top - this.container.outerHeight() - parentOffset.top;
            else
                containerTop = this.element.offset().top + this.element.outerHeight() - parentOffset.top;
            this.container[this.drops == 'up' ? 'addClass' : 'removeClass']('dropup');

            if (this.opens == 'left') {
                this.container.css({
                    top: containerTop,
                    right: parentRightEdge - this.element.offset().left - this.element.outerWidth(),
                    left: 'auto'
                });
                if (this.container.offset().left < 0) {
                    this.container.css({
                        right: 'auto',
                        left: 9
                    });
                }
            } else if (this.opens == 'center') {
                this.container.css({
                    top: containerTop,
                    left: this.element.offset().left - parentOffset.left + this.element.outerWidth() / 2
                            - this.container.outerWidth() / 2,
                    right: 'auto'
                });
                if (this.container.offset().left < 0) {
                    this.container.css({
                        right: 'auto',
                        left: 9
                    });
                }
            } else {
                this.container.css({
                    top: containerTop,
                    left: this.element.offset().left - parentOffset.left,
                    right: 'auto'
                });
                if (this.container.offset().left + this.container.outerWidth() > $(window).width()) {
                    this.container.css({
                        left: 'auto',
                        right: 0
                    });
                }
            }
        },

        show: function(e) {
            if (this.isShowing) return;

            // Create a click proxy that is private to this instance of datepicker, for unbinding
            this._outsideClickProxy = $.proxy(function(e) { this.outsideClick(e); }, this);

            // Bind global datepicker mousedown for hiding and
            $(document)
              .on('mousedown.daterangepicker', this._outsideClickProxy)
              // also support mobile devices
              .on('touchend.daterangepicker', this._outsideClickProxy)
              // also explicitly play nice with Bootstrap dropdowns, which stopPropagation when clicking them
              .on('click.daterangepicker', '[data-toggle=dropdown]', this._outsideClickProxy)
              // and also close when focus changes to outside the picker (eg. tabbing between controls)
              .on('focusin.daterangepicker', this._outsideClickProxy);

            // Reposition the picker if the window is resized while it's open
            $(window).on('resize.daterangepicker', $.proxy(function(e) { this.move(e); }, this));

            this.oldStartDate = this.startDate.clone();
            this.oldEndDate = this.endDate.clone();
            this.previousRightTime = this.endDate.clone();

            this.updateView();
            this.container.show();
            this.move();
            this.element.trigger('show.daterangepicker', this);
            this.isShowing = true;
        },

        hide: function(e) {
            if (!this.isShowing) return;

            //incomplete date selection, revert to last values
            if (!this.endDate) {
                this.startDate = this.oldStartDate.clone();
                this.endDate = this.oldEndDate.clone();
            }

            //if a new date range was selected, invoke the user callback function
            if (!this.startDate.isSame(this.oldStartDate) || !this.endDate.isSame(this.oldEndDate))
                this.callback(this.startDate, this.endDate, this.chosenLabel);

            //if picker is attached to a text input, update it
            this.updateElement();

            $(document).off('.daterangepicker');
            $(window).off('.daterangepicker');
            this.container.hide();
            this.element.trigger('hide.daterangepicker', this);
            this.isShowing = false;
        },

        toggle: function(e) {
            if (this.isShowing) {
                this.hide();
            } else {
                this.show();
            }
        },

        outsideClick: function(e) {
            var target = $(e.target);
            // if the page is clicked anywhere except within the daterangerpicker/button
            // itself then call this.hide()
            if (
                // ie modal dialog fix
                e.type == "focusin" ||
                target.closest(this.element).length ||
                target.closest(this.container).length ||
                target.closest('.calendar-table').length
                ) return;
            this.hide();
        },

        showCalendars: function() {
            this.container.addClass('show-calendar');
            this.move();
            this.element.trigger('showCalendar.daterangepicker', this);
        },

        hideCalendars: function() {
            this.container.removeClass('show-calendar');
            this.element.trigger('hideCalendar.daterangepicker', this);
        },

        hoverRange: function(e) {

            //ignore mouse movements while an above-calendar text input has focus
            if (this.container.find('input[name=daterangepicker_start]').is(":focus") || this.container.find('input[name=daterangepicker_end]').is(":focus"))
                return;

            var label = e.target.getAttribute('data-range-key');

            if (label == this.locale.customRangeLabel) {
                this.updateView();
            } else {
                var dates = this.ranges[label];
                this.container.find('input[name=daterangepicker_start]').val(dates[0].format(this.locale.format));
                this.container.find('input[name=daterangepicker_end]').val(dates[1].format(this.locale.format));
            }

        },

        clickRange: function(e) {
            var label = e.target.getAttribute('data-range-key');
            this.chosenLabel = label;
            if (label == this.locale.customRangeLabel) {
                this.showCalendars();
            } else {
                var dates = this.ranges[label];
                this.startDate = dates[0];
                this.endDate = dates[1];

                if (!this.timePicker) {
                    this.startDate.startOf('day');
                    this.endDate.endOf('day');
                }

                if (!this.alwaysShowCalendars)
                    this.hideCalendars();
                this.clickApply();
            }
        },

        clickPrev: function(e) {
            var cal = $(e.target).parents('.calendar');
            if (cal.hasClass('left')) {
                this.leftCalendar.month.subtract(1, 'month');
                if (this.linkedCalendars)
                    this.rightCalendar.month.subtract(1, 'month');
            } else {
                this.rightCalendar.month.subtract(1, 'month');
            }
            this.updateCalendars();
        },

        clickNext: function(e) {
            var cal = $(e.target).parents('.calendar');
            if (cal.hasClass('left')) {
                this.leftCalendar.month.add(1, 'month');
            } else {
                this.rightCalendar.month.add(1, 'month');
                if (this.linkedCalendars)
                    this.leftCalendar.month.add(1, 'month');
            }
            this.updateCalendars();
        },

        hoverDate: function(e) {

            //ignore mouse movements while an above-calendar text input has focus
            //if (this.container.find('input[name=daterangepicker_start]').is(":focus") || this.container.find('input[name=daterangepicker_end]').is(":focus"))
            //    return;

            //ignore dates that can't be selected
            if (!$(e.target).hasClass('available')) return;

            //have the text inputs above calendars reflect the date being hovered over
            var title = $(e.target).attr('data-title');
            var row = title.substr(1, 1);
            var col = title.substr(3, 1);
            var cal = $(e.target).parents('.calendar');
            var date = cal.hasClass('left') ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];

            if (this.endDate && !this.container.find('input[name=daterangepicker_start]').is(":focus")) {
                this.container.find('input[name=daterangepicker_start]').val(date.format(this.locale.format));
            } else if (!this.endDate && !this.container.find('input[name=daterangepicker_end]').is(":focus")) {
                this.container.find('input[name=daterangepicker_end]').val(date.format(this.locale.format));
            }

            //highlight the dates between the start date and the date being hovered as a potential end date
            var leftCalendar = this.leftCalendar;
            var rightCalendar = this.rightCalendar;
            var startDate = this.startDate;
            if (!this.endDate) {
                this.container.find('.calendar td').each(function(index, el) {

                    //skip week numbers, only look at dates
                    if ($(el).hasClass('week')) return;

                    var title = $(el).attr('data-title');
                    var row = title.substr(1, 1);
                    var col = title.substr(3, 1);
                    var cal = $(el).parents('.calendar');
                    var dt = cal.hasClass('left') ? leftCalendar.calendar[row][col] : rightCalendar.calendar[row][col];

                    if ((dt.isAfter(startDate) && dt.isBefore(date)) || dt.isSame(date, 'day')) {
                        $(el).addClass('in-range');
                    } else {
                        $(el).removeClass('in-range');
                    }

                });
            }

        },

        clickDate: function(e) {

            if (!$(e.target).hasClass('available')) return;

            var title = $(e.target).attr('data-title');
            var row = title.substr(1, 1);
            var col = title.substr(3, 1);
            var cal = $(e.target).parents('.calendar');
            var date = cal.hasClass('left') ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];

            //
            // this function needs to do a few things:
            // * alternate between selecting a start and end date for the range,
            // * if the time picker is enabled, apply the hour/minute/second from the select boxes to the clicked date
            // * if autoapply is enabled, and an end date was chosen, apply the selection
            // * if single date picker mode, and time picker isn't enabled, apply the selection immediately
            //

            if (this.endDate || date.isBefore(this.startDate, 'day')) { //picking start
                if (this.timePicker) {
                    var hour = parseInt(this.container.find('.left .hourselect').val(), 10);
                    if (!this.timePicker24Hour) {
                        var ampm = this.container.find('.left .ampmselect').val();
                        if (ampm === 'PM' && hour < 12)
                            hour += 12;
                        if (ampm === 'AM' && hour === 12)
                            hour = 0;
                    }
                    var minute = parseInt(this.container.find('.left .minuteselect').val(), 10);
                    var second = this.timePickerSeconds ? parseInt(this.container.find('.left .secondselect').val(), 10) : 0;
                    date = date.clone().hour(hour).minute(minute).second(second);
                }
                this.endDate = null;
                this.setStartDate(date.clone());
            } else if (!this.endDate && date.isBefore(this.startDate)) {
                //special case: clicking the same date for start/end,
                //but the time of the end date is before the start date
                this.setEndDate(this.startDate.clone());
            } else { // picking end
                if (this.timePicker) {
                    var hour = parseInt(this.container.find('.right .hourselect').val(), 10);
                    if (!this.timePicker24Hour) {
                        var ampm = this.container.find('.right .ampmselect').val();
                        if (ampm === 'PM' && hour < 12)
                            hour += 12;
                        if (ampm === 'AM' && hour === 12)
                            hour = 0;
                    }
                    var minute = parseInt(this.container.find('.right .minuteselect').val(), 10);
                    var second = this.timePickerSeconds ? parseInt(this.container.find('.right .secondselect').val(), 10) : 0;
                    date = date.clone().hour(hour).minute(minute).second(second);
                }
                this.setEndDate(date.clone());
                if (this.autoApply) {
                  this.calculateChosenLabel();
                  this.clickApply();
                }
            }

            if (this.singleDatePicker) {
                this.setEndDate(this.startDate);
                if (!this.timePicker)
                    this.clickApply();
            }

            this.updateView();

        },

        calculateChosenLabel: function() {
          var customRange = true;
          var i = 0;
          for (var range in this.ranges) {
              if (this.timePicker) {
                  if (this.startDate.isSame(this.ranges[range][0]) && this.endDate.isSame(this.ranges[range][1])) {
                      customRange = false;
                      this.chosenLabel = this.container.find('.ranges li:eq(' + i + ')').addClass('active').html();
                      break;
                  }
              } else {
                  //ignore times when comparing dates if time picker is not enabled
                  if (this.startDate.format('YYYY-MM-DD') == this.ranges[range][0].format('YYYY-MM-DD') && this.endDate.format('YYYY-MM-DD') == this.ranges[range][1].format('YYYY-MM-DD')) {
                      customRange = false;
                      this.chosenLabel = this.container.find('.ranges li:eq(' + i + ')').addClass('active').html();
                      break;
                  }
              }
              i++;
          }
          if (customRange) {
              this.chosenLabel = this.container.find('.ranges li:last').addClass('active').html();
              this.showCalendars();
          }
        },

        clickApply: function(e) {
            this.hide();
            this.element.trigger('apply.daterangepicker', this);
        },

        clickCancel: function(e) {
            this.startDate = this.oldStartDate;
            this.endDate = this.oldEndDate;
            this.hide();
            this.element.trigger('cancel.daterangepicker', this);
        },

        monthOrYearChanged: function(e) {
            var isLeft = $(e.target).closest('.calendar').hasClass('left'),
                leftOrRight = isLeft ? 'left' : 'right',
                cal = this.container.find('.calendar.'+leftOrRight);

            // Month must be Number for new moment versions
            var month = parseInt(cal.find('.monthselect').val(), 10);
            var year = cal.find('.yearselect').val();

            if (!isLeft) {
                if (year < this.startDate.year() || (year == this.startDate.year() && month < this.startDate.month())) {
                    month = this.startDate.month();
                    year = this.startDate.year();
                }
            }

            if (this.minDate) {
                if (year < this.minDate.year() || (year == this.minDate.year() && month < this.minDate.month())) {
                    month = this.minDate.month();
                    year = this.minDate.year();
                }
            }

            if (this.maxDate) {
                if (year > this.maxDate.year() || (year == this.maxDate.year() && month > this.maxDate.month())) {
                    month = this.maxDate.month();
                    year = this.maxDate.year();
                }
            }

            if (isLeft) {
                this.leftCalendar.month.month(month).year(year);
                if (this.linkedCalendars)
                    this.rightCalendar.month = this.leftCalendar.month.clone().add(1, 'month');
            } else {
                this.rightCalendar.month.month(month).year(year);
                if (this.linkedCalendars)
                    this.leftCalendar.month = this.rightCalendar.month.clone().subtract(1, 'month');
            }
            this.updateCalendars();
        },

        timeChanged: function(e) {

            var cal = $(e.target).closest('.calendar'),
                isLeft = cal.hasClass('left');

            var hour = parseInt(cal.find('.hourselect').val(), 10);
            var minute = parseInt(cal.find('.minuteselect').val(), 10);
            var second = this.timePickerSeconds ? parseInt(cal.find('.secondselect').val(), 10) : 0;

            if (!this.timePicker24Hour) {
                var ampm = cal.find('.ampmselect').val();
                if (ampm === 'PM' && hour < 12)
                    hour += 12;
                if (ampm === 'AM' && hour === 12)
                    hour = 0;
            }

            if (isLeft) {
                var start = this.startDate.clone();
                start.hour(hour);
                start.minute(minute);
                start.second(second);
                this.setStartDate(start);
                if (this.singleDatePicker) {
                    this.endDate = this.startDate.clone();
                } else if (this.endDate && this.endDate.format('YYYY-MM-DD') == start.format('YYYY-MM-DD') && this.endDate.isBefore(start)) {
                    this.setEndDate(start.clone());
                }
            } else if (this.endDate) {
                var end = this.endDate.clone();
                end.hour(hour);
                end.minute(minute);
                end.second(second);
                this.setEndDate(end);
            }

            //update the calendars so all clickable dates reflect the new time component
            this.updateCalendars();

            //update the form inputs above the calendars with the new time
            this.updateFormInputs();

            //re-render the time pickers because changing one selection can affect what's enabled in another
            this.renderTimePicker('left');
            this.renderTimePicker('right');

        },

        formInputsChanged: function(e) {
            var isRight = $(e.target).closest('.calendar').hasClass('right');
            var start = moment(this.container.find('input[name="daterangepicker_start"]').val(), this.locale.format);
            var end = moment(this.container.find('input[name="daterangepicker_end"]').val(), this.locale.format);

            if (start.isValid() && end.isValid()) {

                if (isRight && end.isBefore(start))
                    start = end.clone();

                this.setStartDate(start);
                this.setEndDate(end);

                if (isRight) {
                    this.container.find('input[name="daterangepicker_start"]').val(this.startDate.format(this.locale.format));
                } else {
                    this.container.find('input[name="daterangepicker_end"]').val(this.endDate.format(this.locale.format));
                }

            }

            this.updateView();
        },

        formInputsFocused: function(e) {

            // Highlight the focused input
            this.container.find('input[name="daterangepicker_start"], input[name="daterangepicker_end"]').removeClass('active');
            $(e.target).addClass('active');

            // Set the state such that if the user goes back to using a mouse,
            // the calendars are aware we're selecting the end of the range, not
            // the start. This allows someone to edit the end of a date range without
            // re-selecting the beginning, by clicking on the end date input then
            // using the calendar.
            var isRight = $(e.target).closest('.calendar').hasClass('right');
            if (isRight) {
                this.endDate = null;
                this.setStartDate(this.startDate.clone());
                this.updateView();
            }

        },

        formInputsBlurred: function(e) {

            // this function has one purpose right now: if you tab from the first
            // text input to the second in the UI, the endDate is nulled so that
            // you can click another, but if you tab out without clicking anything
            // or changing the input value, the old endDate should be retained

            if (!this.endDate) {
                var val = this.container.find('input[name="daterangepicker_end"]').val();
                var end = moment(val, this.locale.format);
                if (end.isValid()) {
                    this.setEndDate(end);
                    this.updateView();
                }
            }

        },

        elementChanged: function() {
            if (!this.element.is('input')) return;
            if (!this.element.val().length) return;
            if (this.element.val().length < this.locale.format.length) return;

            var dateString = this.element.val().split(this.locale.separator),
                start = null,
                end = null;

            if (dateString.length === 2) {
                start = moment(dateString[0], this.locale.format);
                end = moment(dateString[1], this.locale.format);
            }

            if (this.singleDatePicker || start === null || end === null) {
                start = moment(this.element.val(), this.locale.format);
                end = start;
            }

            if (!start.isValid() || !end.isValid()) return;

            this.setStartDate(start);
            this.setEndDate(end);
            this.updateView();
        },

        keydown: function(e) {
            //hide on tab or enter
            if ((e.keyCode === 9) || (e.keyCode === 13)) {
                this.hide();
            }
        },

        updateElement: function() {
            if (this.element.is('input') && !this.singleDatePicker && this.autoUpdateInput) {
                this.element.val(this.startDate.format(this.locale.format) + this.locale.separator + this.endDate.format(this.locale.format));
                this.element.trigger('change');
            } else if (this.element.is('input') && this.autoUpdateInput) {
                this.element.val(this.startDate.format(this.locale.format));
                this.element.trigger('change');
            }
        },

        remove: function() {
            this.container.remove();
            this.element.off('.daterangepicker');
            this.element.removeData();
        }

    };

    $.fn.daterangepicker = function(options, callback) {
        this.each(function() {
            var el = $(this);
            if (el.data('daterangepicker'))
                el.data('daterangepicker').remove();
            el.data('daterangepicker', new DateRangePicker(el, options, callback));
        });
        return this;
    };

    return DateRangePicker;

}));

/* anytime.5.2.0.min.js
Copyright Andrew M. Andrews III and other contributors.
License: MIT-Style (see uncompressed anytime.js source) */
var AnyTime={version:"5.2.0",pad:function(a,b){for(var c=String(Math.abs(a));c.length<b;)c="0"+c;return 0>a&&(c="-"+c),c}};!function(a){var b=[31,28,31,30,31,30,31,31,30,31,30,31],c=!1,d=[];a.fn.AnyTime_picker=function(a){return this.each(function(){AnyTime.picker(this.id,a)})},a.fn.AnyTime_noPicker=function(){return this.each(function(){AnyTime.noPicker(this.id)})},a.fn.AnyTime_setCurrent=function(a){return this.each(function(){AnyTime.setCurrent(this.id,a)})},a.fn.AnyTime_setEarliest=function(a){return this.each(function(){AnyTime.setEarliest(this.id,a)})},a.fn.AnyTime_setLatest=function(a){return this.each(function(){AnyTime.setLatest(this.id,a)})},a.fn.AnyTime_current=function(a,b){a?(this.removeClass("AnyTime-out-btn ui-state-default ui-state-disabled ui-state-active"),this.addClass("AnyTime-cur-btn ui-state-default ui-state-active")):(this.removeClass("AnyTime-cur-btn ui-state-active"),b?this.removeClass("AnyTime-out-btn ui-state-disabled"):this.addClass("AnyTime-out-btn ui-state-disabled"))},a.fn.AnyTime_clickCurrent=function(){this.find(".AnyTime-cur-btn").triggerHandler("click")},a(document).ready(function(){for(var a in d)Array.prototype[a]||d[a].onReady();c=!0}),AnyTime.Converter=function(b){var c=0,d=9,e=9,f=6,g=3,h=Number.MIN_VALUE,i=Number.MIN_VALUE,j=Number.MIN_VALUE,k=-1,l=Number.MIN_VALUE,m=-1,n=!1;this.fmt="%Y-%m-%d %T",this.dAbbr=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],this.dNames=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],this.eAbbr=["BCE","CE"],this.mAbbr=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],this.mNames=["January","February","March","April","May","June","July","August","September","October","November","December"],this.baseYear=null,this.dAt=function(a,b){return a.charCodeAt(b)>="0".charCodeAt(0)&&a.charCodeAt(b)<="9".charCodeAt(0)},this.format=function(a){var b=new Date(a.getTime());h==Number.MIN_VALUE&&j!=Number.MIN_VALUE&&b.setTime(b.getTime()+6e4*b.getTimezoneOffset()+6e4*j);for(var d,e="",f=0;c>f;f++)if("%"!=this.fmt.charAt(f))e+=this.fmt.charAt(f);else{var g=this.fmt.charAt(f+1);switch(g){case"a":e+=this.dAbbr[b.getDay()];break;case"B":b.getFullYear()<0&&(e+=this.eAbbr[0]);break;case"b":e+=this.mAbbr[b.getMonth()];break;case"C":b.getFullYear()>0&&(e+=this.eAbbr[1]);break;case"c":e+=b.getMonth()+1;break;case"d":d=b.getDate(),10>d&&(e+="0"),e+=String(d);break;case"D":if(d=String(b.getDate()),e+=d,2==d.length&&"1"==d.charAt(0))e+="th";else switch(d.charAt(d.length-1)){case"1":e+="st";break;case"2":e+="nd";break;case"3":e+="rd";break;default:e+="th"}break;case"E":e+=this.eAbbr[b.getFullYear()<0?0:1];break;case"e":e+=b.getDate();break;case"H":d=b.getHours(),10>d&&(e+="0"),e+=String(d);break;case"h":case"I":d=b.getHours()%12,0==d?e+="12":(10>d&&(e+="0"),e+=String(d));break;case"i":d=b.getMinutes(),10>d&&(e+="0"),e+=String(d);break;case"k":e+=b.getHours();break;case"l":d=b.getHours()%12,e+=0==d?"12":String(d);break;case"M":e+=this.mNames[b.getMonth()];break;case"m":d=b.getMonth()+1,10>d&&(e+="0"),e+=String(d);break;case"p":e+=b.getHours()<12?"AM":"PM";break;case"r":d=b.getHours()%12,0==d?e+="12:":(10>d&&(e+="0"),e+=String(d)+":"),d=b.getMinutes(),10>d&&(e+="0"),e+=String(d)+":",d=b.getSeconds(),10>d&&(e+="0"),e+=String(d),e+=b.getHours()<12?"AM":"PM";break;case"S":case"s":d=b.getSeconds(),10>d&&(e+="0"),e+=String(d);break;case"T":d=b.getHours(),10>d&&(e+="0"),e+=String(d)+":",d=b.getMinutes(),10>d&&(e+="0"),e+=String(d)+":",d=b.getSeconds(),10>d&&(e+="0"),e+=String(d);break;case"W":e+=this.dNames[b.getDay()];break;case"w":e+=b.getDay();break;case"Y":e+=AnyTime.pad(b.getFullYear(),4);break;case"y":d=b.getFullYear()%100,e+=AnyTime.pad(d,2);break;case"Z":e+=AnyTime.pad(Math.abs(b.getFullYear()),4);break;case"z":e+=Math.abs(b.getFullYear());break;case"%":e+="%";break;case"#":d=h!=Number.MIN_VALUE?h:j==Number.MIN_VALUE?0-b.getTimezoneOffset():j,d>=0&&(e+="+"),e+=d;break;case"@":if(d=h!=Number.MIN_VALUE?h:j==Number.MIN_VALUE?0-b.getTimezoneOffset():j,AnyTime.utcLabel&&AnyTime.utcLabel[d]){e+=k>0&&k<AnyTime.utcLabel[d].length?AnyTime.utcLabel[d][k]:AnyTime.utcLabel[d][0];break}e+="UTC",g=":";case"+":case"-":case":":case";":d=h!=Number.MIN_VALUE?h:j==Number.MIN_VALUE?0-b.getTimezoneOffset():j,e+=0>d?"-":"+",d=Math.abs(d),e+="+"==g||":"==g?AnyTime.pad(Math.floor(d/60),2):Math.floor(d/60),(":"==g||";"==g)&&(e+=":"),e+=AnyTime.pad(d%60,2);break;case"f":case"j":case"U":case"u":case"V":case"v":case"X":case"x":throw"%"+g+" not implemented by AnyTime.Converter";default:e+=this.fmt.substr(f,2)}f++}return e},this.getUtcParseOffsetCaptured=function(){return i},this.getUtcParseOffsetSubIndex=function(){return m},this.parse=function(a){i=l,m=-1;for(var b,h,j,k,o,p=1,q=new Date(4,0,1,0,0,0,0),r=a.length,s=0,t=1,u=l,v=0;c>v;v++)if("%"==this.fmt.charAt(v)){var w=this.fmt.charAt(v+1);switch(w){case"a":for(h=!1,k=0;r>s+k;k++){for(j=a.substr(s,k),b=0;12>b;b++)if(this.dAbbr[b]==j){h=!0,s+=k;break}if(h)break}if(!h)throw"unknown weekday: "+a.substr(s);break;case"B":k=this.eAbbr[0].length,r>=s+k&&a.substr(s,k)==this.eAbbr[0]&&(p=-1,s+=k);break;case"b":for(h=!1,k=0;r>s+k;k++){for(j=a.substr(s,k),b=0;12>b;b++)if(this.mAbbr[b]==j){q.setMonth(b),h=!0,s+=k;break}if(h)break}if(!h)throw"unknown month: "+a.substr(s);break;case"C":k=this.eAbbr[1].length,r>=s+k&&a.substr(s,k)==this.eAbbr[1]&&(s+=k);break;case"c":r>s+1&&this.dAt(a,s+1)?(q.setMonth((Number(a.substr(s,2))-1)%12),s+=2):(q.setMonth((Number(a.substr(s,1))-1)%12),s++);break;case"D":r>s+1&&this.dAt(a,s+1)?(q.setDate(Number(a.substr(s,2))),s+=4):(q.setDate(Number(a.substr(s,1))),s+=3);break;case"d":q.setDate(Number(a.substr(s,2))),s+=2;break;case"E":if(k=this.eAbbr[0].length,r>=s+k&&a.substr(s,k)==this.eAbbr[0])p=-1,s+=k;else{if(!(s+(k=this.eAbbr[1].length)<=r&&a.substr(s,k)==this.eAbbr[1]))throw"unknown era: "+a.substr(s);s+=k}break;case"e":r>s+1&&this.dAt(a,s+1)?(q.setDate(Number(a.substr(s,2))),s+=2):(q.setDate(Number(a.substr(s,1))),s++);break;case"f":s+=6;break;case"H":q.setHours(Number(a.substr(s,2))),s+=2;break;case"h":case"I":q.setHours(Number(a.substr(s,2))),s+=2;break;case"i":q.setMinutes(Number(a.substr(s,2))),s+=2;break;case"k":r>s+1&&this.dAt(a,s+1)?(q.setHours(Number(a.substr(s,2))),s+=2):(q.setHours(Number(a.substr(s,1))),s++);break;case"l":r>s+1&&this.dAt(a,s+1)?(q.setHours(Number(a.substr(s,2))),s+=2):(q.setHours(Number(a.substr(s,1))),s++);break;case"M":for(h=!1,k=g;r>=s+k&&!(k>e);k++){for(j=a.substr(s,k),b=0;12>b;b++)if(this.mNames[b]==j){q.setMonth(b),h=!0,s+=k;break}if(h)break}break;case"m":q.setMonth((Number(a.substr(s,2))-1)%12),s+=2;break;case"p":12==q.getHours()?"A"==a.charAt(s)&&q.setHours(0):"P"==a.charAt(s)&&q.setHours(q.getHours()+12),s+=2;break;case"r":q.setHours(Number(a.substr(s,2))),q.setMinutes(Number(a.substr(s+3,2))),q.setSeconds(Number(a.substr(s+6,2))),12==q.getHours()?"A"==a.charAt(s+8)&&q.setHours(0):"P"==a.charAt(s+8)&&q.setHours(q.getHours()+12),s+=10;break;case"S":case"s":q.setSeconds(Number(a.substr(s,2))),s+=2;break;case"T":q.setHours(Number(a.substr(s,2))),q.setMinutes(Number(a.substr(s+3,2))),q.setSeconds(Number(a.substr(s+6,2))),s+=8;break;case"W":for(h=!1,k=f;r>=s+k&&!(k>d);k++){for(j=a.substr(s,k),b=0;7>b;b++)if(this.dNames[b]==j){h=!0,s+=k;break}if(h)break}break;case"w":s+=1;break;case"Y":b=4,"-"==a.substr(s,1)&&b++,q.setFullYear(Number(a.substr(s,b))),s+=b;break;case"y":b=2,"-"==a.substr(s,1)&&b++,o=Number(a.substr(s,b)),o+="number"==typeof this.baseYear?this.baseYear:70>o?2e3:1900,q.setFullYear(o),s+=b;break;case"Z":q.setFullYear(Number(a.substr(s,4))),s+=4;break;case"z":for(b=0;r>s&&this.dAt(a,s);)b=10*b+Number(a.charAt(s++));q.setFullYear(b);break;case"#":for("-"==a.charAt(s++)&&(t=-1),u=0;r>s&&String(b=Number(a.charAt(s)))==a.charAt(s);s++)u=10*u+b;u*=t;break;case"@":if(m=-1,AnyTime.utcLabel){h=!1;for(u in AnyTime.utcLabel)if(!Array.prototype[u]){for(b=0;b<AnyTime.utcLabel[u].length;b++)if(j=AnyTime.utcLabel[u][b],k=j.length,r>=s+k&&a.substr(s,k)==j){s+=k,h=!0;break}if(h)break}if(h){m=b,u=Number(u);break}}if(r>s+9||"UTC"!=a.substr(s,3))throw"unknown time zone: "+a.substr(s);s+=3,w=":";case"-":case"+":case":":case";":"-"==a.charAt(s++)&&(t=-1),u=Number(a.charAt(s)),("+"==w||":"==w||r>s+3&&String(Number(a.charAt(s+3)))!==a.charAt(s+3))&&(u=10*u+Number(a.charAt(++s))),u*=60,(":"==w||";"==w)&&s++,u=(u+Number(a.substr(++s,2)))*t,s+=2;break;case"j":case"U":case"u":case"V":case"v":case"X":case"x":throw"%"+this.fmt.charAt(v+1)+" not implemented by AnyTime.Converter";case"%":default:throw"%"+this.fmt.charAt(v+1)+" reserved for future use"}v++}else{if(this.fmt.charAt(v)!=a.charAt(s))throw a+' is not in "'+this.fmt+'" format';s++}return 0>p&&q.setFullYear(0-q.getFullYear()),u!=Number.MIN_VALUE&&(n?i=u:q.setTime(q.getTime()-6e4*u-6e4*q.getTimezoneOffset())),q},this.setUtcFormatOffsetAlleged=function(a){var b=h;return h=a,b},this.setUtcFormatOffsetSubIndex=function(a){var b=k;return k=a,b},function(h){var i,k;if(b=jQuery.extend(!0,{},b||{}),b.baseYear&&(h.baseYear=Number(b.baseYear)),b.format&&(h.fmt=b.format),c=h.fmt.length,b.dayAbbreviations&&(h.dAbbr=a.makeArray(b.dayAbbreviations)),b.dayNames)for(h.dNames=a.makeArray(b.dayNames),d=1,f=1e3,i=0;7>i;i++)k=h.dNames[i].length,k>d&&(d=k),f>k&&(f=k);if(b.eraAbbreviations&&(h.eAbbr=a.makeArray(b.eraAbbreviations)),b.monthAbbreviations&&(h.mAbbr=a.makeArray(b.monthAbbreviations)),b.monthNames)for(h.mNames=a.makeArray(b.monthNames),e=1,g=1e3,i=0;12>i;i++)k=h.mNames[i].length,k>e&&(e=k),g>k&&(g=k);"undefined"!=typeof b.utcFormatOffsetImposed&&(j=b.utcFormatOffsetImposed),"undefined"!=typeof b.utcParseOffsetAssumed&&(l=b.utcParseOffsetAssumed),b.utcParseOffsetCapture&&(n=!0)}(this)},AnyTime.noPicker=function(a){d[a]&&(d[a].cleanup(),delete d[a])},AnyTime.picker=function(e,f){if(d[e])throw'Cannot create another AnyTime.picker for "'+e+'"';var g=null;d[e]={twelveHr:!1,ajaxOpts:null,denyTab:!0,askEra:!1,cloak:null,conv:null,div:null,dB:null,dD:null,dY:null,dMo:null,dDoM:null,hDoM:null,hMo:null,hTitle:null,hY:null,dT:null,dH:null,dM:null,dS:null,dO:null,earliest:null,fBtn:null,fDOW:0,hBlur:null,hClick:null,hFocus:null,hKeydown:null,hResize:null,id:null,inp:null,latest:null,lastAjax:null,lostFocus:!1,lX:"X",lY:"Year",lO:"Time Zone",oBody:null,oConv:null,oCur:null,oDiv:null,oLab:null,oList:null,oSel:null,offMin:Number.MIN_VALUE,offSI:-1,offStr:"",pop:!0,ro:!1,time:null,url:null,yAhead:null,y0XXX:null,yCur:null,yDiv:null,yLab:null,yNext:null,yPast:null,yPrior:null,initialize:function(d){if(g=this,this.id="AnyTime--"+d.replace(/[^-_.A-Za-z0-9]/g,"--AnyTime--"),f=jQuery.extend(!0,{},f||{}),f.utcParseOffsetCapture=!0,this.conv=new AnyTime.Converter(f),f.placement)if("inline"==f.placement)this.pop=!1;else if("popup"!=f.placement)throw"unknown placement: "+f.placement;if(f.ajaxOptions&&(this.ajaxOpts=jQuery.extend({},f.ajaxOptions),this.ajaxOpts.success||(this.ajaxOpts.success=function(a){g.updVal(a)})),f.earliest&&(this.earliest=this.makeDate(f.earliest)),f.firstDOW){if(f.firstDOW<0||f.firstDOW>6)throw"illegal firstDOW: "+f.firstDOW;this.fDOW=f.firstDOW}f.latest&&(this.latest=this.makeDate(f.latest)),this.lX=f.labelDismiss||"X",this.lY=f.labelYear||"Year",this.lO=f.labelTimeZone||"Time Zone";var e,h,i,j=0,k=this.conv.fmt;this.askEra="undefined"!=typeof f.askEra?f.askEra:k.indexOf("%B")>=0||k.indexOf("%C")>=0||k.indexOf("%E")>=0;var l=k.indexOf("%Y")>=0||k.indexOf("%y")>=0||k.indexOf("%Z")>=0||k.indexOf("%z")>=0,m=k.indexOf("%b")>=0||k.indexOf("%c")>=0||k.indexOf("%M")>=0||k.indexOf("%m")>=0,n=k.indexOf("%D")>=0||k.indexOf("%d")>=0||k.indexOf("%e")>=0,o=l||m||n;this.twelveHr=k.indexOf("%h")>=0||k.indexOf("%I")>=0||k.indexOf("%l")>=0||k.indexOf("%r")>=0;var p=this.twelveHr||k.indexOf("%H")>=0||k.indexOf("%k")>=0||k.indexOf("%T")>=0,q=k.indexOf("%i")>=0||k.indexOf("%r")>=0||k.indexOf("%T")>=0,r=k.indexOf("%r")>=0||k.indexOf("%S")>=0||k.indexOf("%s")>=0||k.indexOf("%T")>=0;r&&"undefined"!=typeof f.askSecond&&(r=f.askSecond);var s=k.indexOf("%#")>=0||k.indexOf("%+")>=0||k.indexOf("%-")>=0||k.indexOf("%:")>=0||k.indexOf("%;")>=0||k.indexOf("%<")>=0||k.indexOf("%>")>=0||k.indexOf("%@")>=0,t=p||q||r||s;s&&(this.oConv=new AnyTime.Converter({format:f.formatUtcOffset||k.match(/\S*%[-+:;<>#@]\S*/g).join(" ")})),this.inp=a(document.getElementById(d)),this.ro=this.inp.prop("readonly"),this.inp.prop("readonly",!0),this.div=a('<div class="AnyTime-win AnyTime-pkr ui-widget ui-widget-content ui-corner-all" id="'+this.id+'" aria-live="off"></div>'),this.inp.after(this.div),this.hTitle=a('<h5 class="AnyTime-hdr ui-widget-header ui-corner-top"/>'),this.div.append(this.hTitle),this.dB=a('<div class="AnyTime-body"></div>'),this.div.append(this.dB),f.hideInput&&this.inp.css({border:0,height:"1px",margin:0,padding:0,width:"1px"}),h=null;var u=null;if(this.pop&&(u=a('<div class="AnyTime-x-btn ui-state-default">'+this.lX+"</div>"),this.hTitle.append(u),u.click(function(a){g.dismiss(a)})),i="",o){if(this.dD=a('<div class="AnyTime-date"></div>'),this.dB.append(this.dD),l&&(this.yLab=a('<h6 class="AnyTime-lbl AnyTime-lbl-yr">'+this.lY+"</h6>"),this.dD.append(this.yLab),this.dY=a('<ul class="AnyTime-yrs ui-helper-reset" />'),this.dD.append(this.dY),this.yPast=this.btn(this.dY,"&lt;",this.newYear,["yrs-past"],"- "+this.lY),this.yPrior=this.btn(this.dY,"1",this.newYear,["yr-prior"],"-1 "+this.lY),this.yCur=this.btn(this.dY,"2",this.newYear,["yr-cur"],this.lY),this.yCur.removeClass("ui-state-default"),this.yCur.addClass("AnyTime-cur-btn ui-state-default ui-state-active"),this.yNext=this.btn(this.dY,"3",this.newYear,["yr-next"],"+1 "+this.lY),this.yAhead=this.btn(this.dY,"&gt;",this.newYear,["yrs-ahead"],"+ "+this.lY),j++),m){for(i=f.labelMonth||"Month",this.hMo=a('<h6 class="AnyTime-lbl AnyTime-lbl-month">'+i+"</h6>"),this.dD.append(this.hMo),this.dMo=a('<ul class="AnyTime-mons" />'),this.dD.append(this.dMo),e=0;12>e;e++){var v=this.btn(this.dMo,this.conv.mAbbr[e],function(c){var d=a(c.target);if(!d.hasClass("AnyTime-out-btn")){var e=c.target.AnyTime_month,f=new Date(this.time.getTime());f.getDate()>b[e]&&f.setDate(b[e]),f.setMonth(e),this.set(f),this.upd(d)}},["mon","mon"+String(e+1)],i+" "+this.conv.mNames[e]);v[0].AnyTime_month=e}j++}if(n){i=f.labelDayOfMonth||"Day of Month",this.hDoM=a('<h6 class="AnyTime-lbl AnyTime-lbl-dom">'+i+"</h6>"),this.dD.append(this.hDoM),this.dDoM=a('<table border="0" cellpadding="0" cellspacing="0" class="AnyTime-dom-table"/>'),this.dD.append(this.dDoM),h=a('<thead class="AnyTime-dom-head"/>'),this.dDoM.append(h);var w=a('<tr class="AnyTime-dow"/>');for(h.append(w),e=0;7>e;e++)w.append('<th class="AnyTime-dow AnyTime-dow'+String(e+1)+'">'+this.conv.dAbbr[(this.fDOW+e)%7]+"</th>");var x=a('<tbody class="AnyTime-dom-body" />');this.dDoM.append(x);for(var y=0;6>y;y++)for(w=a('<tr class="AnyTime-wk AnyTime-wk'+String(y+1)+'"/>'),x.append(w),e=0;7>e;e++)this.btn(w,"x",function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=Number(c.html());if(d){var e=new Date(this.time.getTime());e.setDate(d),this.set(e),this.upd(c)}}},["dom"],i);j++}}if(t){var z,A;if(this.dT=a('<div class="AnyTime-time"></div>'),this.dB.append(this.dT),p){this.dH=a('<div class="AnyTime-hrs"></div>'),this.dT.append(this.dH),i=f.labelHour||"Hour",this.dH.append(a('<h6 class="AnyTime-lbl AnyTime-lbl-hr">'+i+"</h6>"));var B=a('<ul class="AnyTime-hrs-am"/>');this.dH.append(B);var C=a('<ul class="AnyTime-hrs-pm"/>');for(this.dH.append(C),e=0;12>e;e++)h=this.twelveHr?0==e?"12am":String(e)+"am":AnyTime.pad(e,2),this.btn(B,h,this.newHour,["hr","hr"+String(e)],i+" "+h),h=this.twelveHr?0==e?"12pm":String(e)+"pm":e+12,this.btn(C,h,this.newHour,["hr","hr"+String(e+12)],i+" "+h);j++}if(q){for(this.dM=a('<div class="AnyTime-mins"></div>'),this.dT.append(this.dM),i=f.labelMinute||"Minute",this.dM.append(a('<h6 class="AnyTime-lbl AnyTime-lbl-min">'+i+"</h6>")),z=a('<ul class="AnyTime-mins-tens"/>'),this.dM.append(z),e=0;6>e;e++)this.btn(z,e,function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=new Date(this.time.getTime());d.setMinutes(10*Number(c.text())+this.time.getMinutes()%10),this.set(d),this.upd(c)}},["min-ten","min"+e+"0"],i+" "+e+"0");for(;12>e;e++)this.btn(z,"&#160;",a.noop,["min-ten","min"+e+"0"],i+" "+e+"0").addClass("AnyTime-min-ten-btn-empty ui-state-default ui-state-disabled");for(A=a('<ul class="AnyTime-mins-ones"/>'),this.dM.append(A),e=0;10>e;e++)this.btn(A,e,function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=new Date(this.time.getTime());d.setMinutes(10*Math.floor(this.time.getMinutes()/10)+Number(c.text())),this.set(d),this.upd(c)}},["min-one","min"+e],i+" "+e);for(;12>e;e++)this.btn(A,"&#160;",a.noop,["min-one","min"+e+"0"],i+" "+e).addClass("AnyTime-min-one-btn-empty ui-state-default ui-state-disabled");j++}if(r){for(this.dS=a('<div class="AnyTime-secs"></div>'),this.dT.append(this.dS),i=f.labelSecond||"Second",this.dS.append(a('<h6 class="AnyTime-lbl AnyTime-lbl-sec">'+i+"</h6>")),z=a('<ul class="AnyTime-secs-tens"/>'),this.dS.append(z),e=0;6>e;e++)this.btn(z,e,function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=new Date(this.time.getTime());d.setSeconds(10*Number(c.text())+this.time.getSeconds()%10),this.set(d),this.upd(c)}},["sec-ten","sec"+e+"0"],i+" "+e+"0");for(;12>e;e++)this.btn(z,"&#160;",a.noop,["sec-ten","sec"+e+"0"],i+" "+e+"0").addClass("AnyTime-sec-ten-btn-empty ui-state-default ui-state-disabled");for(A=a('<ul class="AnyTime-secs-ones"/>'),this.dS.append(A),e=0;10>e;e++)this.btn(A,e,function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=new Date(this.time.getTime());d.setSeconds(10*Math.floor(this.time.getSeconds()/10)+Number(c.text())),this.set(d),this.upd(c)}},["sec-one","sec"+e],i+" "+e);for(;12>e;e++)this.btn(A,"&#160;",a.noop,["sec-one","sec"+e+"0"],i+" "+e).addClass("AnyTime-sec-one-btn-empty ui-state-default ui-state-disabled");j++}s&&(this.dO=a('<div class="AnyTime-offs" ></div>'),this.dT.append("<br />"),this.dT.append(this.dO),this.oList=a('<ul class="AnyTime-off-list ui-helper-reset" />'),this.dO.append(this.oList),this.oCur=this.btn(this.oList,"",this.newOffset,["off","off-cur"],i),this.oCur.removeClass("ui-state-default"),this.oCur.addClass("AnyTime-cur-btn ui-state-default ui-state-active"),this.oSel=this.btn(this.oList,"&#177;",this.newOffset,["off","off-select"],"+/- "+this.lO),this.oMinW=this.dO.outerWidth(!0),this.oLab=a('<h6 class="AnyTime-lbl AnyTime-lbl-off">'+this.lO+"</h6>"),this.dO.prepend(this.oLab),j++)}f.labelTitle?this.hTitle.append(f.labelTitle):j>1?this.hTitle.append("Select a "+(o?t?"Date and Time":"Date":"Time")):this.hTitle.append("Select");try{this.time=this.conv.parse(this.inp.val()),this.offMin=this.conv.getUtcParseOffsetCaptured(),this.offSI=this.conv.getUtcParseOffsetSubIndex(),"init"in f&&(this.time=this.makeDate(f.init))}catch(D){this.time=new Date}this.lastAjax=this.time,this.pop&&(this.div.hide(),this.div.css("position","absolute")),this.inp.blur(this.hBlur=function(a){g.inpBlur(a)}),this.inp.click(this.hClick=function(a){g.showPkr(a)}),this.inp.focus(this.hFocus=function(a){g.lostFocus&&g.showPkr(a),g.lostFocus=!1}),this.inp.keydown(this.hKeydown=function(a){g.key(a)}),this.div.click(function(){g.lostFocus=!1,g.inp.focus()}),a(window).resize(this.hResize=function(a){g.pos(a)}),c&&this.onReady()},ajax:function(){if(this.ajaxOpts&&this.time.getTime()!=this.lastAjax.getTime())try{var b=jQuery.extend({},this.ajaxOpts);if("object"==typeof b.data)b.data[this.inp[0].name||this.inp[0].id]=this.inp.val();else{var c=(this.inp[0].name||this.inp[0].id)+"="+encodeURI(this.inp.val());b.data?b.data+="&"+c:b.data=c}a.ajax(b),this.lastAjax=this.time}catch(d){}},askOffset:function(b){if(this.oDiv)this.cloak.show(),this.oDiv.show();else{this.makeCloak(),this.oDiv=a('<div class="AnyTime-win AnyTime-off-selector ui-widget ui-widget-content ui-corner-all"></div>'),this.div.append(this.oDiv);var c=a('<h5 class="AnyTime-hdr AnyTime-hdr-off-selector ui-widget-header ui-corner-top" />');this.oDiv.append(c),this.oBody=a('<div class="AnyTime-body AnyTime-body-off-selector"></div>'),this.oDiv.append(this.oBody);var d=a('<div class="AnyTime-x-btn ui-state-default">'+this.lX+"</div>");c.append(d),d.click(function(a){g.dismissODiv(a)}),c.append(this.lO);var e=a('<ul class="AnyTime-off-off" />'),f=null;this.oBody.append(e);var h=this.oConv.fmt.indexOf("%@")>=0;if(AnyTime.utcLabel)for(var i=-720;840>=i;i++)if(AnyTime.utcLabel[i]){this.oConv.setUtcFormatOffsetAlleged(i);for(var j=0;j<AnyTime.utcLabel[i].length&&(this.oConv.setUtcFormatOffsetSubIndex(j),f=this.btn(e,this.oConv.format(this.time),this.newOPos,["off-off"],i),f[0].AnyTime_offMin=i,f[0].AnyTime_offSI=j,h);j++);}if(f&&f.addClass("AnyTime-off-off-last-btn"),this.oDiv.outerHeight(!0)>this.div.height()){var k=this.oBody.width();this.oBody.css("height","0"),this.oBody.css({height:String(this.div.height()-(this.oDiv.outerHeight(!0)+this.oBody.outerHeight(!1)))+"px",width:String(k+20)+"px"})}this.oDiv.outerWidth(!0)>this.div.width()&&(this.oBody.css("width","0"),this.oBody.css("width",String(this.div.width()-(this.oDiv.outerWidth(!0)+this.oBody.outerWidth(!1)))+"px"))}this.pos(b),this.updODiv(null);var l=this.oDiv.find(".AnyTime-off-off-btn.AnyTime-cur-btn:first");l.length||(l=this.oDiv.find(".AnyTime-off-off-btn:first")),this.setFocus(l)},askYear:function(b){if(this.yDiv)this.cloak.show(),this.yDiv.show();else{this.makeCloak(),this.yDiv=a('<div class="AnyTime-win AnyTime-yr-selector ui-widget ui-widget-content ui-corner-all"></div>'),this.div.append(this.yDiv);var c=a('<h5 class="AnyTime-hdr AnyTime-hdr-yr-selector ui-widget-header ui-corner-top" />');this.yDiv.append(c);var d=a('<div class="AnyTime-x-btn ui-state-default">'+this.lX+"</div>");c.append(d),d.click(function(a){g.dismissYDiv(a)}),c.append(this.lY);var e=a('<div class="AnyTime-body AnyTime-body-yr-selector" ></div>');for(this.yDiv.append(e),cont=a('<ul class="AnyTime-yr-mil" />'),e.append(cont),this.y0XXX=this.btn(cont,0,this.newYPos,["mil","mil0"],this.lY+" 0000"),i=1;10>i;i++)this.btn(cont,i,this.newYPos,["mil","mil"+i],this.lY+" "+i+"000");for(cont=a('<ul class="AnyTime-yr-cent" />'),e.append(cont),i=0;10>i;i++)this.btn(cont,i,this.newYPos,["cent","cent"+i],this.lY+" "+i+"00");for(cont=a('<ul class="AnyTime-yr-dec" />'),e.append(cont),i=0;10>i;i++)this.btn(cont,i,this.newYPos,["dec","dec"+i],this.lY+" "+i+"0");for(cont=a('<ul class="AnyTime-yr-yr" />'),e.append(cont),i=0;10>i;i++)this.btn(cont,i,this.newYPos,["yr","yr"+i],this.lY+" "+i);this.askEra&&(cont=a('<ul class="AnyTime-yr-era" />'),e.append(cont),this.btn(cont,this.conv.eAbbr[0],function(b){var c=new Date(this.time.getTime()),d=c.getFullYear();d>0&&c.setFullYear(0-d),this.set(c),this.updYDiv(a(b.target))},["era","bce"],this.conv.eAbbr[0]),this.btn(cont,this.conv.eAbbr[1],function(b){var c=new Date(this.time.getTime()),d=c.getFullYear();0>d&&c.setFullYear(0-d),this.set(c),this.updYDiv(a(b.target))},["era","ce"],this.conv.eAbbr[1]))}this.pos(b),this.updYDiv(null),this.setFocus(this.yDiv.find(".AnyTime-yr-btn.AnyTime-cur-btn:first"))},inpBlur:function(a){return this.oDiv&&this.oDiv.is(":visible")?(g.inp.focus(),void 0):(this.lostFocus=!0,setTimeout(function(){g.lostFocus&&(g.div.find(".AnyTime-focus-btn").removeClass("AnyTime-focus-btn ui-state-focus"),g.pop?g.dismiss(a):g.ajax())},334),void 0)},btn:function(b,c,d,e,f){for(var h="ul"==b[0].nodeName.toLowerCase()?"li":"td",i="<"+h+' class="AnyTime-btn',j=0;j<e.length;j++)i+=" AnyTime-"+e[j]+"-btn";var k=a(i+' ui-state-default">'+c+"</"+h+">");return b.append(k),k.AnyTime_title=f,k.click(function(a){g.tempFunc=d,g.tempFunc(a)}),k.dblclick(function(b){var c=a(this);c.is(".AnyTime-off-off-btn")?g.dismissODiv(b):c.is(".AnyTime-mil-btn")||c.is(".AnyTime-cent-btn")||c.is(".AnyTime-dec-btn")||c.is(".AnyTime-yr-btn")||c.is(".AnyTime-era-btn")?g.dismissYDiv(b):g.pop&&g.dismiss(b)}),k},cleanup:function(){this.inp.prop("readonly",this.ro).off("blur",this.hBlur).off("click",this.hClick).off("focus",this.hFocus).off("keydown",this.hKeydown),a(window).off("resize",this.hResize),this.div.remove()},dismiss:function(){this.ajax(),this.yDiv&&this.dismissYDiv(),this.oDiv&&this.dismissODiv(),this.div.hide(),this.lostFocus=!0},dismissODiv:function(){this.oDiv.hide(),this.cloak.hide(),this.setFocus(this.oCur)},dismissYDiv:function(){this.yDiv.hide(),this.cloak.hide(),this.setFocus(this.yCur)},setFocus:function(a){if(a.hasClass("AnyTime-focus-btn")||(this.div.find(".AnyTime-focus-btn").removeClass("AnyTime-focus-btn ui-state-focus"),this.fBtn=a,a.removeClass("ui-state-default ui-state-active"),a.addClass("AnyTime-focus-btn ui-state-default ui-state-active ui-state-focus")),a.hasClass("AnyTime-off-off-btn")){var b=this.oBody.offset().top,c=a.offset().top,d=a.outerHeight(!0);b>c-d?this.oBody.scrollTop(c+this.oBody.scrollTop()-(this.oBody.innerHeight()+b)+2*d):c+d>b+this.oBody.innerHeight()&&this.oBody.scrollTop(c+this.oBody.scrollTop()-(b+d))}},key:function(a){var c,d=null,e=this,f=(this.div.find(".AnyTime-focus-btn"),a.keyCode||a.which);if(this.denyTab=!0,16==f);else if(10==f||13==f||27==f)this.oDiv&&this.oDiv.is(":visible")?this.dismissODiv(a):this.yDiv&&this.yDiv.is(":visible")?this.dismissYDiv(a):this.pop&&this.dismiss(a);else if(33==f||9==f&&a.shiftKey){if(this.fBtn.hasClass("AnyTime-off-off-btn"))9==f&&this.dismissODiv(a);else if(this.fBtn.hasClass("AnyTime-mil-btn"))9==f&&this.dismissYDiv(a);else if(this.fBtn.hasClass("AnyTime-cent-btn"))this.yDiv.find(".AnyTime-mil-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-dec-btn"))this.yDiv.find(".AnyTime-cent-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-yr-btn"))this.yDiv.find(".AnyTime-dec-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-era-btn"))this.yDiv.find(".AnyTime-yr-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.parents(".AnyTime-yrs").length){if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-mon-btn")){if(this.dY)this.yCur.triggerHandler("click");else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-dom-btn")){if(9==f&&a.shiftKey)return this.denyTab=!1,void 0;d=new Date(this.time.getTime()),a.shiftKey?d.setFullYear(d.getFullYear()-1):(c=d.getMonth()-1,d.getDate()>b[c]&&d.setDate(b[c]),d.setMonth(c)),this.keyDateChange(d)}else if(this.fBtn.hasClass("AnyTime-hr-btn")){if(d=this.dDoM||this.dMo)d.AnyTime_clickCurrent();else if(this.dY)this.yCur.triggerHandler("click");else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-min-ten-btn")){if(d=this.dH||this.dDoM||this.dMo)d.AnyTime_clickCurrent();else if(this.dY)this.yCur.triggerHandler("click");else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-min-one-btn"))this.dM.AnyTime_clickCurrent();else if(this.fBtn.hasClass("AnyTime-sec-ten-btn")){if(d=this.dM?this.dM.find(".AnyTime-mins-ones"):this.dH||this.dDoM||this.dMo)d.AnyTime_clickCurrent();else if(this.dY)this.yCur.triggerHandler("click");else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-sec-one-btn"))this.dS.AnyTime_clickCurrent();else if(this.fBtn.hasClass("AnyTime-off-btn"))if(d=this.dS?this.dS.find(".AnyTime-secs-ones"):this.dM?this.dM.find(".AnyTime-mins-ones"):this.dH||this.dDoM||this.dMo)d.AnyTime_clickCurrent();else if(this.dY)this.yCur.triggerHandler("click");else if(9==f)return this.denyTab=!1,void 0}else if(34==f||9==f){if(this.fBtn.hasClass("AnyTime-mil-btn"))this.yDiv.find(".AnyTime-cent-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-cent-btn"))this.yDiv.find(".AnyTime-dec-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-dec-btn"))this.yDiv.find(".AnyTime-yr-btn.AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-yr-btn"))d=this.yDiv.find(".AnyTime-era-btn.AnyTime-cur-btn"),d.length?d.triggerHandler("click"):9==f&&this.dismissYDiv(a);else if(this.fBtn.hasClass("AnyTime-era-btn"))9==f&&this.dismissYDiv(a);else if(this.fBtn.hasClass("AnyTime-off-off-btn"))9==f&&this.dismissODiv(a);else if(this.fBtn.parents(".AnyTime-yrs").length){if(d=this.dDoM||this.dMo||this.dH||this.dM||this.dS||this.dO)d.AnyTime_clickCurrent();else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-mon-btn")){if(d=this.dDoM||this.dH||this.dM||this.dS||this.dO)d.AnyTime_clickCurrent();else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-dom-btn"))if(9==f){if(d=this.dH||this.dM||this.dS||this.dO,!d)return this.denyTab=!1,void 0;d.AnyTime_clickCurrent()}else d=new Date(this.time.getTime()),a.shiftKey?d.setFullYear(d.getFullYear()+1):(c=d.getMonth()+1,d.getDate()>b[c]&&d.setDate(b[c]),d.setMonth(c)),this.keyDateChange(d);else if(this.fBtn.hasClass("AnyTime-hr-btn")){if(d=this.dM||this.dS||this.dO)d.AnyTime_clickCurrent();else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-min-ten-btn"))this.dM.find(".AnyTime-mins-ones .AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-min-one-btn")){if(d=this.dS||this.dO)d.AnyTime_clickCurrent();else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-sec-ten-btn"))this.dS.find(".AnyTime-secs-ones .AnyTime-cur-btn").triggerHandler("click");else if(this.fBtn.hasClass("AnyTime-sec-one-btn")){if(this.dO)this.dO.AnyTime_clickCurrent();else if(9==f)return this.denyTab=!1,void 0}else if(this.fBtn.hasClass("AnyTime-off-btn")&&9==f)return this.denyTab=!1,void 0}else if(35==f)this.fBtn.hasClass("AnyTime-mil-btn")||this.fBtn.hasClass("AnyTime-cent-btn")||this.fBtn.hasClass("AnyTime-dec-btn")||this.fBtn.hasClass("AnyTime-yr-btn")||this.fBtn.hasClass("AnyTime-era-btn")?(d=this.yDiv.find(".AnyTime-ce-btn"),d.length||(d=this.yDiv.find(".AnyTime-yr9-btn")),d.triggerHandler("click")):this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(1),d.setMonth(d.getMonth()+1),d.setDate(d.getDate()-1),a.ctrlKey&&d.setMonth(11),this.keyDateChange(d)):this.dS?this.dS.find(".AnyTime-sec9-btn").triggerHandler("click"):this.dM?this.dM.find(".AnyTime-min9-btn").triggerHandler("click"):this.dH?this.dH.find(".AnyTime-hr23-btn").triggerHandler("click"):this.dDoM?this.dDoM.find(".AnyTime-dom-btn-filled:last").triggerHandler("click"):this.dMo?this.dMo.find(".AnyTime-mon12-btn").triggerHandler("click"):this.dY&&this.yAhead.triggerHandler("click");else if(36==f)this.fBtn.hasClass("AnyTime-mil-btn")||this.fBtn.hasClass("AnyTime-cent-btn")||this.fBtn.hasClass("AnyTime-dec-btn")||this.fBtn.hasClass("AnyTime-yr-btn")||this.fBtn.hasClass("AnyTime-era-btn")?this.yDiv.find(".AnyTime-mil0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(1),a.ctrlKey&&d.setMonth(0),this.keyDateChange(d)):this.dY?this.yCur.triggerHandler("click"):this.dMo?this.dMo.find(".AnyTime-mon1-btn").triggerHandler("click"):this.dDoM?this.dDoM.find(".AnyTime-dom-btn-filled:first").triggerHandler("click"):this.dH?this.dH.find(".AnyTime-hr0-btn").triggerHandler("click"):this.dM?this.dM.find(".AnyTime-min00-btn").triggerHandler("click"):this.dS&&this.dS.find(".AnyTime-sec00-btn").triggerHandler("click");else if(37==f)this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(d.getDate()-1),this.keyDateChange(d)):this.keyBack();else if(38==f)this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(d.getDate()-7),this.keyDateChange(d)):this.keyBack();else if(39==f)this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(d.getDate()+1),this.keyDateChange(d)):this.keyAhead();else if(40==f)this.fBtn.hasClass("AnyTime-dom-btn")?(d=new Date(this.time.getTime()),d.setDate(d.getDate()+7),this.keyDateChange(d)):this.keyAhead();else{if((86==f||118==f)&&a.ctrlKey)return this.updVal(""),setTimeout(function(){e.showPkr(null)},100),void 0;this.showPkr(null)}a.preventDefault()},keyAhead:function(){this.fBtn.hasClass("AnyTime-mil9-btn")?this.yDiv.find(".AnyTime-cent0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-cent9-btn")?this.yDiv.find(".AnyTime-dec0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-dec9-btn")?this.yDiv.find(".AnyTime-yr0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-yr9-btn")?this.yDiv.find(".AnyTime-bce-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-sec9-btn")||(this.fBtn.hasClass("AnyTime-sec50-btn")?this.dS.find(".AnyTime-sec0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-min9-btn")?this.dS&&this.dS.find(".AnyTime-sec00-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-min50-btn")?this.dM.find(".AnyTime-min0-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-hr23-btn")?this.dM?this.dM.find(".AnyTime-min00-btn").triggerHandler("click"):this.dS&&this.dS.find(".AnyTime-sec00-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-hr11-btn")?this.dH.find(".AnyTime-hr12-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-mon12-btn")?this.dDoM?this.dDoM.AnyTime_clickCurrent():this.dH?this.dH.find(".AnyTime-hr0-btn").triggerHandler("click"):this.dM?this.dM.find(".AnyTime-min00-btn").triggerHandler("click"):this.dS&&this.dS.find(".AnyTime-sec00-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-yrs-ahead-btn")?this.dMo?this.dMo.find(".AnyTime-mon1-btn").triggerHandler("click"):this.dH?this.dH.find(".AnyTime-hr0-btn").triggerHandler("click"):this.dM?this.dM.find(".AnyTime-min00-btn").triggerHandler("click"):this.dS&&this.dS.find(".AnyTime-sec00-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-yr-cur-btn")?this.yNext.triggerHandler("click"):this.fBtn.next().triggerHandler("click"))
},keyBack:function(){this.fBtn.hasClass("AnyTime-cent0-btn")?this.yDiv.find(".AnyTime-mil9-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-dec0-btn")?this.yDiv.find(".AnyTime-cent9-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-yr0-btn")?this.yDiv.find(".AnyTime-dec9-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-bce-btn")?this.yDiv.find(".AnyTime-yr9-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-yr-cur-btn")?this.yPrior.triggerHandler("click"):this.fBtn.hasClass("AnyTime-mon1-btn")?this.dY&&this.yCur.triggerHandler("click"):this.fBtn.hasClass("AnyTime-hr0-btn")?this.dDoM?this.dDoM.AnyTime_clickCurrent():this.dMo?this.dMo.find(".AnyTime-mon12-btn").triggerHandler("click"):this.dY&&this.yNext.triggerHandler("click"):this.fBtn.hasClass("AnyTime-hr12-btn")?this.dH.find(".AnyTime-hr11-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-min00-btn")?this.dH?this.dH.find(".AnyTime-hr23-btn").triggerHandler("click"):this.dDoM?this.dDoM.AnyTime_clickCurrent():this.dMo?this.dMo.find(".AnyTime-mon12-btn").triggerHandler("click"):this.dY&&this.yNext.triggerHandler("click"):this.fBtn.hasClass("AnyTime-min0-btn")?this.dM.find(".AnyTime-min50-btn").triggerHandler("click"):this.fBtn.hasClass("AnyTime-sec00-btn")?this.dM?this.dM.find(".AnyTime-min9-btn").triggerHandler("click"):this.dH?this.dH.find(".AnyTime-hr23-btn").triggerHandler("click"):this.dDoM?this.dDoM.AnyTime_clickCurrent():this.dMo?this.dMo.find(".AnyTime-mon12-btn").triggerHandler("click"):this.dY&&this.yNext.triggerHandler("click"):this.fBtn.hasClass("AnyTime-sec0-btn")?this.dS.find(".AnyTime-sec50-btn").triggerHandler("click"):this.fBtn.prev().triggerHandler("click")},keyDateChange:function(a){this.fBtn.hasClass("AnyTime-dom-btn")&&(this.set(a),this.upd(null),this.setFocus(this.dDoM.find(".AnyTime-cur-btn")))},makeCloak:function(){this.cloak?this.cloak.show():(this.cloak=a('<div class="AnyTime-cloak"></div>'),this.div.append(this.cloak),this.cloak.click(function(a){g.oDiv&&g.oDiv.is(":visible")?g.dismissODiv(a):g.dismissYDiv(a)}))},makeDate:function(a){if("number"==typeof a?a=new Date(a):"string"==typeof a&&(a=this.conv.parse(a)),"getTime"in a)return a;throw new Exception("cannot make a Date from "+a)},newHour:function(b){var c,d,e=a(b.target);if(!e.hasClass("AnyTime-out-btn")){if(this.twelveHr){var f=e.text();d=f.indexOf("a"),0>d?(d=Number(f.substr(0,f.indexOf("p"))),c=12==d?12:d+12):(d=Number(f.substr(0,d)),c=12==d?0:d)}else c=Number(e.text());d=new Date(this.time.getTime()),d.setHours(c),this.set(d),this.upd(e)}},newOffset:function(a){a.target==this.oSel[0]?this.askOffset(a):this.upd(this.oCur)},newOPos:function(b){var c=a(b.target);this.offMin=c[0].AnyTime_offMin,this.offSI=c[0].AnyTime_offSI;var d=new Date(this.time.getTime());this.set(d),this.updODiv(c)},newYear:function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=c.text();if("<"==d||"&lt;"==d)this.askYear(b);else if(">"==d||"&gt;"==d)this.askYear(b);else{var e=new Date(this.time.getTime());e.setFullYear(Number(d)),this.set(e),this.upd(this.yCur)}}},newYPos:function(b){var c=a(b.target);if(!c.hasClass("AnyTime-out-btn")){var d=1,e=this.time.getFullYear();0>e&&(d=-1,e=0-e),e=AnyTime.pad(e,4),e=c.hasClass("AnyTime-mil-btn")?c.html()+e.substring(1,4):c.hasClass("AnyTime-cent-btn")?e.substring(0,1)+c.html()+e.substring(2,4):c.hasClass("AnyTime-dec-btn")?e.substring(0,2)+c.html()+e.substring(3,4):e.substring(0,3)+c.html(),"0000"==e&&(e=1);var f=new Date(this.time.getTime());f.setFullYear(d*e),this.set(f),this.updYDiv(c)}},onReady:function(){this.lostFocus=!0,this.pop?this.div.parent()!=document.body&&this.div.appendTo(document.body):this.upd(null)},pos:function(){if(this.pop){var b=this.inp.offset(),c=a(document.body).outerWidth(!0),d=this.div.outerWidth(!0),e=b.left;e+d>c-20&&(e=c-(d+20));var f=b.top-this.div.outerHeight(!0);0>f&&(f=b.top+this.inp.outerHeight(!0)),this.div.css({top:String(f)+"px",left:String(0>e?0:e)+"px"})}var g=this.div.offset();if(this.oDiv&&this.oDiv.is(":visible")){var h=this.oLab.offset();"absolute"==this.div.css("position")&&(h.top-=g.top,h.left=h.left-g.left,g={top:0,left:0});var i=this.oDiv.outerWidth(!0),j=this.div.outerWidth(!0);h.left+i>g.left+j&&(h.left=g.left+j-i,h.left<2&&(h.left=2));var k=this.oDiv.outerHeight(!0),l=this.div.outerHeight(!0);h.top+=this.oLab.outerHeight(!0),h.top+k>g.top+l&&(h.top=h.top-k),h.top<g.top&&(h.top=g.top),this.oDiv.css({top:h.top+"px",left:h.left+"px"})}else if(this.yDiv&&this.yDiv.is(":visible")){var m=this.yLab.offset();"absolute"==this.div.css("position")&&(m.top-=g.top,m.left=m.left-g.left,g={top:0,left:0}),m.left+=(this.yLab.outerWidth(!0)-this.yDiv.outerWidth(!0))/2,this.yDiv.css({top:m.top+"px",left:m.left+"px"})}this.cloak&&this.cloak.css({top:g.top+"px",left:g.left+"px",height:String(this.div.outerHeight(!0)-2)+"px",width:String(this.div.outerWidth(!0)-2)+"px"})},set:function(a){var b=a.getTime();this.time=this.earliest&&b<this.earliest.getTime()?new Date(this.earliest.getTime()):this.latest&&b>this.latest.getTime()?new Date(this.latest.getTime()):a},setCurrent:function(a){this.set(this.makeDate(a)),this.upd(null)},setEarliest:function(a){this.earliest=this.makeDate(a),this.set(this.time),this.upd(null)},setLatest:function(a){this.latest=this.makeDate(a),this.set(this.time),this.upd(null)},showPkr:function(a){try{this.time=this.conv.parse(this.inp.val()),this.offMin=this.conv.getUtcParseOffsetCaptured(),this.offSI=this.conv.getUtcParseOffsetSubIndex()}catch(b){this.time=new Date}this.set(this.time),this.upd(null),fBtn=null;var c=".AnyTime-cur-btn:first";this.dDoM?fBtn=this.dDoM.find(c):this.yCur?fBtn=this.yCur:this.dMo?fBtn=this.dMo.find(c):this.dH?fBtn=this.dH.find(c):this.dM?fBtn=this.dM.find(c):this.dS&&(fBtn=this.dS.find(c)),this.setFocus(fBtn),this.pos(a)},upd:function(b){var c=new Date(this.time.getTime());c.setMonth(0,1),c.setHours(0,0,0,0);var d=new Date(this.time.getTime());d.setMonth(11,31),d.setHours(23,59,59,999);var e=this.earliest&&this.earliest.getTime(),f=this.latest&&this.latest.getTime(),h=this.time.getFullYear();this.earliest&&this.yPast&&(d.setFullYear(h-2),d.getTime()<this.earliestTime?this.yPast.addClass("AnyTime-out-btn ui-state-disabled"):this.yPast.removeClass("AnyTime-out-btn ui-state-disabled")),this.yPrior&&(this.yPrior.text(AnyTime.pad(1==h?-1:h-1,4)),this.earliest&&(d.setFullYear(h-1),d.getTime()<this.earliestTime?this.yPrior.addClass("AnyTime-out-btn ui-state-disabled"):this.yPrior.removeClass("AnyTime-out-btn ui-state-disabled"))),this.yCur&&this.yCur.text(AnyTime.pad(h,4)),this.yNext&&(this.yNext.text(AnyTime.pad(-1==h?1:h+1,4)),this.latest&&(c.setFullYear(h+1),c.getTime()>this.latestTime?this.yNext.addClass("AnyTime-out-btn ui-state-disabled"):this.yNext.removeClass("AnyTime-out-btn ui-state-disabled"))),this.latest&&this.yAhead&&(c.setFullYear(h+2),c.getTime()>this.latestTime?this.yAhead.addClass("AnyTime-out-btn ui-state-disabled"):this.yAhead.removeClass("AnyTime-out-btn ui-state-disabled")),c.setFullYear(this.time.getFullYear()),d.setFullYear(this.time.getFullYear());var i=0;h=this.time.getMonth(),a("#"+this.id+" .AnyTime-mon-btn").each(function(){c.setMonth(i),d.setDate(1),d.setMonth(i+1),d.setDate(0),a(this).AnyTime_current(i==h,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),i++}),c.setFullYear(this.time.getFullYear()),d.setFullYear(this.time.getFullYear()),c.setMonth(this.time.getMonth()),d.setMonth(this.time.getMonth(),1),h=this.time.getDate();var j=this.time.getMonth(),k=-1,l=c.getDay();this.fDOW>l&&(l+=7);var m=0,n=0;a("#"+this.id+" .AnyTime-wk").each(function(){n=g.fDOW,a(this).children().each(function(){if(n-g.fDOW<7){var b=a(this);0==m&&l>n||c.getMonth()!=j?(b.html("&#160;"),b.removeClass("AnyTime-dom-btn-filled AnyTime-cur-btn ui-state-default ui-state-active"),b.addClass("AnyTime-dom-btn-empty"),m?(1==c.getDate()&&0!=n?b.addClass("AnyTime-dom-btn-empty-after-filled"):b.removeClass("AnyTime-dom-btn-empty-after-filled"),c.getDate()<=7?b.addClass("AnyTime-dom-btn-empty-below-filled"):b.removeClass("AnyTime-dom-btn-empty-below-filled"),c.setDate(c.getDate()+1),d.setDate(d.getDate()+1)):(b.addClass("AnyTime-dom-btn-empty-above-filled"),n==l-1?b.addClass("AnyTime-dom-btn-empty-before-filled"):b.removeClass("AnyTime-dom-btn-empty-before-filled")),b.addClass("ui-state-default ui-state-disabled")):((i=c.getDate())==k&&c.setDate(++i),k=i,b.text(i),b.removeClass("AnyTime-dom-btn-empty AnyTime-dom-btn-empty-above-filled AnyTime-dom-btn-empty-before-filled AnyTime-dom-btn-empty-after-filled AnyTime-dom-btn-empty-below-filled ui-state-default ui-state-disabled"),b.addClass("AnyTime-dom-btn-filled ui-state-default"),b.AnyTime_current(i==h,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),c.setDate(i+1),d.setDate(i+1))}n++}),m++}),c.setFullYear(this.time.getFullYear()),d.setFullYear(this.time.getFullYear()),c.setMonth(this.time.getMonth(),this.time.getDate()),d.setMonth(this.time.getMonth(),this.time.getDate());var o=!this.twelveHr,p=this.time.getHours();a("#"+this.id+" .AnyTime-hr-btn").each(function(){var b,h=this.innerHTML;o?b=Number(h):(b=Number(h.substring(0,h.length-2)),"a"==h.charAt(h.length-2)?12==b&&(b=0):12>b&&(b+=12)),c.setHours(b),d.setHours(b),a(this).AnyTime_current(p==b,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),23>b&&c.setHours(c.getHours()+1)}),c.setHours(this.time.getHours()),d.setHours(this.time.getHours(),9);var q=this.time.getMinutes(),r=String(Math.floor(q/10)),s=String(q%10);if(a("#"+this.id+" .AnyTime-min-ten-btn:not(.AnyTime-min-ten-btn-empty)").each(function(){a(this).AnyTime_current(this.innerHTML==r,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),c.getMinutes()<50&&(c.setMinutes(c.getMinutes()+10),d.setMinutes(d.getMinutes()+10))}),c.setMinutes(10*Math.floor(this.time.getMinutes()/10)),d.setMinutes(10*Math.floor(this.time.getMinutes()/10)),a("#"+this.id+" .AnyTime-min-one-btn:not(.AnyTime-min-one-btn-empty)").each(function(){a(this).AnyTime_current(this.innerHTML==s,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),c.setMinutes(c.getMinutes()+1),d.setMinutes(d.getMinutes()+1)}),c.setMinutes(this.time.getMinutes()),d.setMinutes(this.time.getMinutes(),9),q=this.time.getSeconds(),r=String(Math.floor(q/10)),s=String(q%10),a("#"+this.id+" .AnyTime-sec-ten-btn:not(.AnyTime-sec-ten-btn-empty)").each(function(){a(this).AnyTime_current(this.innerHTML==r,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),c.getSeconds()<50&&(c.setSeconds(c.getSeconds()+10),d.setSeconds(d.getSeconds()+10))}),c.setSeconds(10*Math.floor(this.time.getSeconds()/10)),d.setSeconds(10*Math.floor(this.time.getSeconds()/10)),a("#"+this.id+" .AnyTime-sec-one-btn:not(.AnyTime-sec-one-btn-empty)").each(function(){a(this).AnyTime_current(this.innerHTML==s,(!g.earliest||d.getTime()>=e)&&(!g.latest||c.getTime()<=f)),c.setSeconds(c.getSeconds()+1),d.setSeconds(d.getSeconds()+1)}),this.oConv){this.oConv.setUtcFormatOffsetAlleged(this.offMin),this.oConv.setUtcFormatOffsetSubIndex(this.offSI);var t=this.oConv.format(this.time);this.oCur.html(t)}if(b&&this.setFocus(b),this.conv.setUtcFormatOffsetAlleged(this.offMin),this.conv.setUtcFormatOffsetSubIndex(this.offSI),this.updVal(this.conv.format(this.time)),this.div.show(),this.dO){this.oCur.css("width","0");var u=this.dT.width()-this.oMinW;40>u&&(u=40),this.oCur.css("width",String(u)+"px")}this.pop||this.ajax()},updODiv:function(b){var c=!1,d=null;this.oDiv.find(".AnyTime-off-off-btn").each(function(){this.AnyTime_offMin==g.offMin?this.AnyTime_offSI==g.offSI?a(this).AnyTime_current(c=!0,!0):(a(this).AnyTime_current(!1,!0),null==d&&(d=a(this))):a(this).AnyTime_current(!1,!0)}),c||null==d||d.AnyTime_current(!0,!0),this.conv.setUtcFormatOffsetAlleged(this.offMin),this.conv.setUtcFormatOffsetSubIndex(this.offSI),this.updVal(this.conv.format(this.time)),this.upd(b)},updYDiv:function(b){var c,d,e=1,f=this.time.getFullYear();0>f&&(e=-1,f=0-f),f=AnyTime.pad(f,4);var h=g.earliest&&g.earliest.getFullYear(),i=g.latest&&g.latest.getFullYear();c=0,this.yDiv.find(".AnyTime-mil-btn").each(function(){d=(!g.earliest||e*(c+(0>e?0:999))>=h)&&(!g.latest||i>=e*(c+(e>0?0:999))),a(this).AnyTime_current(this.innerHTML==f.substring(0,1),d),c+=1e3}),c=1e3*Math.floor(f/1e3),this.yDiv.find(".AnyTime-cent-btn").each(function(){d=(!g.earliest||e*(c+(0>e?0:99))>=h)&&(!g.latest||i>=e*(c+(e>0?0:99))),a(this).AnyTime_current(this.innerHTML==f.substring(1,2),d),c+=100}),c=100*Math.floor(f/100),this.yDiv.find(".AnyTime-dec-btn").each(function(){d=(!g.earliest||e*(c+(0>e?0:9))>=h)&&(!g.latest||i>=e*(c+(e>0?0:9))),a(this).AnyTime_current(this.innerHTML==f.substring(2,3),d),c+=10}),c=10*Math.floor(f/10),this.yDiv.find(".AnyTime-yr-btn").each(function(){d=(!g.earliest||e*c>=h)&&(!g.latest||i>=e*c),a(this).AnyTime_current(this.innerHTML==f.substring(3),d),c+=1}),this.yDiv.find(".AnyTime-bce-btn").each(function(){a(this).AnyTime_current(0>e,!g.earliest||g.earliest.getFullYear()<0)}),this.yDiv.find(".AnyTime-ce-btn").each(function(){a(this).AnyTime_current(e>0,!g.latest||g.latest.getFullYear()>0)}),this.conv.setUtcFormatOffsetAlleged(this.offMin),this.conv.setUtcFormatOffsetSubIndex(this.offSI),this.updVal(this.conv.format(this.time)),this.upd(b)},updVal:function(a){this.inp.val()!=a&&(this.inp.val(a),this.inp.change())}},d[e].initialize(e)},AnyTime.setCurrent=function(a,b){d[a].setCurrent(b)},AnyTime.setEarliest=function(a,b){d[a].setEarliest(b)},AnyTime.setLatest=function(a,b){d[a].setLatest(b)}}(jQuery);
/* ------------------------------------------------------------------------------
*
*  # Date and time pickers
*
*  Specific JS code additions for picker_date.html page
*
*  Version: 1.1
*  Latest update: Aug 10, 2016
*
* ---------------------------------------------------------------------------- */

$(function() {

    // Single picker
    $('.daterange-single').daterangepicker({ 
        singleDatePicker: true
    });

    // Date range picker
    // ------------------------------

    // Basic initialization
    // $('.daterange-basic').daterangepicker({
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default'
    // });


    // // Display week numbers
    // $('.daterange-weeknumbers').daterangepicker({
    //     showWeekNumbers: true,
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default'
    // });


    // // Button class options
    // $('.daterange-buttons').daterangepicker({
    //     applyClass: 'btn-success',
    //     cancelClass: 'btn-danger'
    // });


    // // Display time picker
    // $('.daterange-time').daterangepicker({
    //     timePicker: true,
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default',
    //     locale: {
    //         format: 'MM/DD/YYYY h:mm a'
    //     }
    // });


    // // Show calendars on left
    // $('.daterange-left').daterangepicker({
    //     opens: 'left',
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default'
    // });


    


    // // Display date dropdowns
    // $('.daterange-datemenu').daterangepicker({
    //     showDropdowns: true,
    //     opens: "left",
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default'
    // });


    // // 10 minute increments
    // $('.daterange-increments').daterangepicker({
    //     timePicker: true,
    //     opens: "left",
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default',
    //     timePickerIncrement: 10,
    //     locale: {
    //         format: 'MM/DD/YYYY h:mm a'
    //     }
    // });


    // // Localization
    // $('.daterange-locale').daterangepicker({
    //     applyClass: 'bg-slate-600',
    //     cancelClass: 'btn-default',
    //     opens: "left",
    //     ranges: {
    //         'Сегодня': [moment(), moment()],
    //         'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //         'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
    //         'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
    //         'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
    //         'Прошедший месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //     },
    //     locale: {
    //         applyLabel: 'Вперед',
    //         cancelLabel: 'Отмена',
    //         startLabel: 'Начальная дата',
    //         endLabel: 'Конечная дата',
    //         customRangeLabel: 'Выбрать дату',
    //         daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
    //         monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    //         firstDay: 1
    //     }
    // });


    // //
    // // Pre-defined ranges and callback
    // //

    // // Initialize with options
    // $('.daterange-predefined').daterangepicker(
    //     {
    //         startDate: moment().subtract(29, 'days'),
    //         endDate: moment(),
    //         minDate: '01/01/2014',
    //         maxDate: '12/31/2016',
    //         dateLimit: { days: 60 },
    //         ranges: {
    //             'Today': [moment(), moment()],
    //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //             'This Month': [moment().startOf('month'), moment().endOf('month')],
    //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //         },
    //         opens: 'left',
    //         applyClass: 'btn-small bg-slate',
    //         cancelClass: 'btn-small btn-default'
    //     },
    //     function(start, end) {
    //         $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
    //         $.jGrowl('Date range has been changed', { header: 'Update', theme: 'bg-primary', position: 'center', life: 1500 });
    //     }
    // );

    // // Display date format
    // $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));


    // //
    // // Inside button
    // //

    // // Initialize with options
    // $('.daterange-ranges').daterangepicker(
    //     {
    //         startDate: moment().subtract(29, 'days'),
    //         endDate: moment(),
    //         minDate: '01/01/2012',
    //         maxDate: '12/31/2016',
    //         dateLimit: { days: 60 },
    //         ranges: {
    //             'Today': [moment(), moment()],
    //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //             'This Month': [moment().startOf('month'), moment().endOf('month')],
    //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //         },
    //         opens: 'left',
    //         applyClass: 'btn-small bg-slate-600',
    //         cancelClass: 'btn-small btn-default'
    //     },
    //     function(start, end) {
    //         $('.daterange-ranges span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
    //     }
    // );

    // // Display date format
    // $('.daterange-ranges span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));


    
    // // Pick-a-date picker
    // // ------------------------------


    // // Basic options
    // $('.pickadate').pickadate();


    // // Change day names
    // $('.pickadate-strings').pickadate({
    //     weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    //     showMonthsShort: true
    // });


    // // Button options
    // $('.pickadate-buttons').pickadate({
    //     today: '',
    //     close: '',
    //     clear: 'Clear selection'
    // });


    // // Accessibility labels
    // $('.pickadate-accessibility').pickadate({
    //     labelMonthNext: 'Go to the next month',
    //     labelMonthPrev: 'Go to the previous month',
    //     labelMonthSelect: 'Pick a month from the dropdown',
    //     labelYearSelect: 'Pick a year from the dropdown',
    //     selectMonths: true,
    //     selectYears: true
    // });


    // // Localization
    // $('.pickadate-translated').pickadate({
    //     monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    //     weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    //     today: 'aujourd\'hui',
    //     clear: 'effacer',
    //     formatSubmit: 'yyyy/mm/dd'
    // });


    // // Format options
    // $('.pickadate-format').pickadate({

    //     // Escape any “rule” characters with an exclamation mark (!).
    //     format: 'You selecte!d: dddd, dd mmm, yyyy',
    //     formatSubmit: 'yyyy/mm/dd',
    //     hiddenPrefix: 'prefix__',
    //     hiddenSuffix: '__suffix'
    // });


    // // Editable input
    // var $input_date = $('.pickadate-editable').pickadate({
    //     editable: true,
    //     onClose: function() {
    //         $('.datepicker').focus();
    //     }
    // });

    // var picker_date = $input_date.pickadate('picker');
    // $input_date.on('click', function(event) { // register events (https://github.com/amsul/pickadate.js/issues/542)
    //     if (picker_date.get('open')) {
    //         picker_date.close();
    //     } else {
    //         picker_date.open();
    //     }                        
    //     event.stopPropagation();
    // });


    // // Dropdown selectors
    // $('.pickadate-selectors').pickadate({
    //     selectYears: true,
    //     selectMonths: true
    // });


    // // Year selector
    // $('.pickadate-year').pickadate({
    //     selectYears: 4
    // });


    // // Set first weekday
    // $('.pickadate-weekday').pickadate({
    //     firstDay: 1
    // });


    // // Date limits
    // $('.pickadate-limits').pickadate({
    //     min: [2014,3,20],
    //     max: [2014,7,14]
    // });


    // // Disable certain dates
    // $('.pickadate-disable').pickadate({
    //     disable: [
    //         [2015,8,3],
    //         [2015,8,12],
    //         [2015,8,20]
    //     ]
    // });


    // // Disable date range
    // $('.pickadate-disable-range').pickadate({
    //     disable: [
    //         5,
    //         [2013, 10, 21, 'inverted'],
    //         { from: [2014, 3, 15], to: [2014, 3, 25] },
    //         [2014, 3, 20, 'inverted'],
    //         { from: [2014, 3, 17], to: [2014, 3, 18], inverted: true }
    //     ]
    // });


    // // Events
    // $('.pickadate-events').pickadate({
    //     onStart: function() {
    //         console.log('Hello there :)')
    //     },
    //     onRender: function() {
    //         console.log('Whoa.. rendered anew')
    //     },
    //     onOpen: function() {
    //         console.log('Opened up')
    //     },
    //     onClose: function() {
    //         console.log('Closed now')
    //     },
    //     onStop: function() {
    //         console.log('See ya.')
    //     },
    //     onSet: function(context) {
    //         console.log('Just set stuff:', context)
    //     }
    // });


    // // Pick-a-time time picker
    // // ------------------------------

    // // Default functionality
    // $('.pickatime').pickatime();


    // // Clear button
    // $('.pickatime-clear').pickatime({
    //     clear: ''
    // });


    // // Time formats
    // $('.pickatime-format').pickatime({

    //     // Escape any “rule” characters with an exclamation mark (!).
    //     format: 'T!ime selected: h:i a',
    //     formatLabel: '<b>h</b>:i <!i>a</!i>',
    //     formatSubmit: 'HH:i',
    //     hiddenPrefix: 'prefix__',
    //     hiddenSuffix: '__suffix'
    // });


    // // Send hidden value
    // $('.pickatime-hidden').pickatime({
    //     formatSubmit: 'HH:i',
    //     hiddenName: true
    // });


    // // Editable input
    // var $input_time = $('.pickatime-editable').pickatime({
    //     editable: true,
    //     onClose: function() {
    //         $('.datepicker').focus();
    //     }
    // });

    // var picker_time = $input_time.pickatime('picker');
    // $input_time.on('click', function(event) { // register events (https://github.com/amsul/pickadate.js/issues/542)
    //     if (picker_time.get('open')) {
    //         picker_time.close();
    //     } else {
    //         picker_time.open();
    //     }                        
    //     event.stopPropagation();
    // });


    // // Time intervals
    // $('.pickatime-intervals').pickatime({
    //     interval: 150
    // });


    // // Time limits
    // $('.pickatime-limits').pickatime({
    //     min: [7,30],
    //     max: [14,0]
    // });


    // // Using integers as hours
    // $('.pickatime-limits-integers').pickatime({
    //     disable: [
    //         3, 5, 7
    //     ]
    // })


    // // Disable times
    // $('.pickatime-disabled').pickatime({
    //     disable: [
    //         [0,30],
    //         [2,0],
    //         [8,30],
    //         [9,0]
    //     ]
    // });


    // // Disabling ranges
    // $('.pickatime-range').pickatime({
    //     disable: [
    //         1,
    //         [1, 30, 'inverted'],
    //         { from: [4, 30], to: [10, 30] },
    //         [6, 30, 'inverted'],
    //         { from: [8, 0], to: [9, 0], inverted: true }
    //     ]
    // });


    // // Disable all with exeption
    // $('.pickatime-disableall').pickatime({
    //     disable: [
    //         true,
    //         3, 5, 7,
    //         [0,30],
    //         [2,0],
    //         [8,30],
    //         [9,0]
    //     ]
    // });


    // // Events
    // $('.pickatime-events').pickatime({
    //     onStart: function() {
    //         console.log('Hello there :)')
    //     },
    //     onRender: function() {
    //         console.log('Whoa.. rendered anew')
    //     },
    //     onOpen: function() {
    //         console.log('Opened up')
    //     },
    //     onClose: function() {
    //         console.log('Closed now')
    //     },
    //     onStop: function() {
    //         console.log('See ya.')
    //     },
    //     onSet: function(context) {
    //         console.log('Just set stuff:', context)
    //     }
    // });



    // // Anytime picker
    // // ------------------------------

    // // Basic usage
    // $("#anytime-date").AnyTime_picker({
    //     format: "%W, %M %D in the Year %z %E",
    //     firstDOW: 1
    // });


    // // Time picker
    // $("#anytime-time").AnyTime_picker({
    //     format: "%H:%i"
    // });


    // // Display hours only
    // $("#anytime-time-hours").AnyTime_picker({
    //     format: "%l %p"
    // });


    // // Date and time
    // $("#anytime-both").AnyTime_picker({
    //     format: "%M %D %H:%i",
    // });


    // // Custom display format
    // $("#anytime-weekday").AnyTime_picker({
    //     format: "%W, %D of %M, %Z"
    // });


    // // Numeric date
    // $("#anytime-month-numeric").AnyTime_picker({
    //     format: "%d/%m/%Z"
    // });


    // // Month and day
    // $("#anytime-month-day").AnyTime_picker({
    //     format: "%D of %M"
    // });


    // // On demand picker
    // $('#ButtonCreationDemoButton').click(function (e) {
    //     $('#ButtonCreationDemoInput').AnyTime_noPicker().AnyTime_picker().focus();
    //     e.preventDefault();
    // });


    // //
    // // Date range
    // //

    // // Options
    // var oneDay = 24*60*60*1000;
    // var rangeDemoFormat = "%e-%b-%Y";
    // var rangeDemoConv = new AnyTime.Converter({format:rangeDemoFormat});

    // // Set today's date
    // $("#rangeDemoToday").click( function (e) {
    //     $("#rangeDemoStart").val(rangeDemoConv.format(new Date())).change();
    // });

    // // Clear dates
    // $("#rangeDemoClear").click( function (e) {
    //     $("#rangeDemoStart").val("").change();
    // });

    // // Start date
    // $("#rangeDemoStart").AnyTime_picker({
    //     format: rangeDemoFormat
    // });

    // // On value change
    // $("#rangeDemoStart").change(function(e) {
    //     try {
    //         var fromDay = rangeDemoConv.parse($("#rangeDemoStart").val()).getTime();

    //         var dayLater = new Date(fromDay+oneDay);
    //             dayLater.setHours(0,0,0,0);

    //         var ninetyDaysLater = new Date(fromDay+(90*oneDay));
    //             ninetyDaysLater.setHours(23,59,59,999);

    //         // End date
    //         $("#rangeDemoFinish")
    //         .AnyTime_noPicker()
    //         .removeAttr("disabled")
    //         .val(rangeDemoConv.format(dayLater))
    //         .AnyTime_picker({
    //             earliest: dayLater,
    //             format: rangeDemoFormat,
    //             latest: ninetyDaysLater
    //         });
    //     }

    //     catch(e) {

    //         // Disable End date field
    //         $("#rangeDemoFinish").val("").attr("disabled","disabled");
    //     }
    // });
    
});
