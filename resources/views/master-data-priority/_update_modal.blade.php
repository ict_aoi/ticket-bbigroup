<div id="updatePriorityModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => '#',
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'update_priority'
					])
				!!}
				<div class="modal-body">
					@include('form.text', [
						'field' => 'priority',
						'label' => 'priority',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'update_name_priority'
						]
					])

					@include('form.text', [
						'field' => 'duration_reso',
						'label' => 'duration_reso',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'update_duration_reso',
							'rows' => 5,
							'style' => 'resize: none;'
						]
					])
                    @include('form.select', [
                        'field' => 'unit_reso',
                        'label' => 'unit_reso',
                        'options' => [
                            ''         => '-- Pilih Unit/Satuan--',
                            'hari'      => 'hari',
                            'jam'       => 'jam',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'update_unit_reso'
                        ]
                    ])
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
