@extends('layouts.app',['active' => 'priority'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Issue Category</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Priority</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertPriorityModal"><i class="icon-plus2 position-left"></i>Create New</a>
        </div>
	</div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="priorityTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Priority</th>
                        <th>Durasi Target</th>
                        <th>Satuan Unit</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('master-data-priority._insert_modal')
	@include('master-data-priority._update_modal')
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#priorityTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/priority/data',
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'priority', name: 'priority',searchable:true,orderable:true},
                {data: 'duration_reso', name: 'duration_reso',searchable:true,orderable:true},
                {data: 'unit_reso', name: 'unit_reso',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:true,orderable:true},
            ]
        });
        var dtable = $('#issueCategoryTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        $('#insert_priority').submit(function (event){
            event.preventDefault();
            var priority = $('#priority').val();
            var duration = $('#duration_reso').val();
            var unit = $('#select_unit_reso').val();
            console.log(duration);

            if(!priority){
                $("#alert_warning").trigger("click", 'Priority wajib diisi');
                return false
            }
            if(!unit){
                $("#alert_warning").trigger("click", 'Unit wajib diisi');
                return false
            }
            if(!duration){
                $("#alert_warning").trigger("click", 'Duration wajib diisi');
                return false
            }
            else if(isNaN(duration)){
                $("#alert_warning").trigger("click", 'Duration wajib numeric');
                return false
            }
            console.log($('#insert_priority').serialize());
            $('#insertPriorityModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#insert_priority').attr('action'),
                        data: $('#insert_priority').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (response) {
                            console.log(response);
                            $('#insertPriorityModal').modal();
                            $('#insert_priority').trigger("reset");
                            $('#priorityTable').DataTable().ajax.reload();
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                        },
                        error: function ( xhr, status, error) {
                            $.unblockUI();
                            // console.log(response);
                            var err = eval("(" + xhr.responseText + ")");
                            // $('#priorityError').text(response.responseJSON.errors.title);
                            $('#insertPriorityModal').modal();
                            console.log(err);
                            // if (response.status == 422){
                                //  $("#alert_warning").trigger("click",response.responseJSON);
                                //  $('#duration').val(response.responseJSON.responseText);
                                // $("#alert_error").trigger("click", 'Data Gagal disimpan');
                                $("#alert_error").trigger("click",err.errors.priority);
                            // }
                        }
                    });
                }
            });
        });

        $('#update_priority').submit(function (event){
		event.preventDefault();
		var name = $('#update_name_priority').val();

		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}

		$('#updatePriorityModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#update_priority').attr('action'),
                        data: $('#update_priority').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (response) {
                            $('#updatePriorityModal').modal('hide');
                            $('#update_priority').trigger("reset");
                            $('#priorityTable').DataTable().ajax.reload();
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                        },
                        error: function (response) {
                            $.unblockUI();
                            console.log(response.responseJSON.responseText);
                            if (response.status == 422) {
                                // $("#alert_warning").trigger("click",response.responseJSON);

                            }
                            $('#updatePriorityModal').modal();
                        }
                    });
                }
            });
        });
    })
    function edit(url)
    {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#ffff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();

            }
        })
        .done(function (response) {
            console.log(response);
            $('#update_priority').attr('action', response.url_update);
            $('#update_name_priority').val(response.priority);
            $('#update_duration_reso').val(response.duration_reso);
            $('#update_unit_reso').val(response.unit_reso).trigger('change');
            $('#update_duration_respon').val(response.duration_respon);
            $('#update_unit_respon').val(response.unit_respon).trigger('change');
            $('#updatePriorityModal').modal();

        });
    }

    function hapus(url)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "delete",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function (response) {
                $.unblockUI();
            }
        }).done(function ($result) {
            $('#priorityTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil hapus');
        });
    }

</script>
@endsection
