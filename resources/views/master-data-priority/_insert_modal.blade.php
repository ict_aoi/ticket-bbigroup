<div id="insertPriorityModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
				{!!
					Form::open([
						'role' => 'form',
						'url' => route('priority.store'),
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'insert_priority'
					])
				!!}
				<div class="modal-body">
					@include('form.text', [
						'field' => 'priority',
						'label' => 'Priority',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'priority'
						]
					])

					@include('form.text', [
						'field' => 'duration_reso',
						'label' => 'duration_reso',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'duration_reso',
							'rows' => 5,
							'style' => 'resize: none;'
						]
					])

					@include('form.select', [
                                            'field' => 'unit_reso',
                                            'label' => 'unit_reso',
                                            'options' => [
                                                ''         => '-- Pilih Satuan--',
                                                'jam'      => 'jam',
                                                'hari'       => 'hari',
                                            ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_unit_reso'
                                            ]
                                        ])
                    <span id="priorityError" class="alert-message"></span>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
