<div id="updateIssueModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => '#',
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'update_issue'
					])
				!!}
				<div class="modal-body">
					@include('form.text', [
						'field' => 'name',
						'label' => 'Nama',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'update_name'
						]
					])

					@include('form.textarea', [
						'field' => 'description',
						'label' => 'Description',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'update_description',
							'rows' => 5,
							'style' => 'resize: none;'
						]
					])
                    @include('form.select', [
                        'field' => 'department',
                        'label' => 'Department',
                        'options' => [
                            ''         => '-- Pilih Department--',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'update_department'
                        ]
                    ])
                    @include('form.select', [
                        'field' => 'sub_department',
                        'label' => 'Sub Department',
                        'options' => [
                            ''         => '-- Pilih Sub Department--',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'update_sub_department'
                        ]
                    ])

					@include('form.select', [
                                            'field' => 'type',
                                            'label' => 'Type',
                                            'options' => [
                                                ''         => '-- Pilih Issue Type--',
                                                'request'  => 'request',
                                                'support'  => 'support',
                                                'project'  => 'project',

                                            ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'update_type'
                                            ]
                                        ])
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
