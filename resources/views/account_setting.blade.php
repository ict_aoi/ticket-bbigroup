@extends('layouts.app',['active' => 'account_setting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Account Setting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Account Setting</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">Account settings<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
    </div>

    <div class="panel-body">
         {!!
            Form::open([
                'role' => 'form',
                'url' => route('accountSetting.updateAccount',auth::user()->id),
                'method' => 'put',
                'enctype' => 'multipart/form-data'
            ])
        !!}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Name</label>
                        <input type="text" value="{{ auth::user()->name }}" readonly="readonly" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>Email</label>
                        <input type="text" value="{{ auth::user()->email }}" readonly="readonly" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Old password</label>
                        <input type="password" name="old_password" placeholder="Enter Old password" class="form-control">
                        @if ($errors->has('old_password'))
                            <span class="help-block" style="color: red;">
                                <strong>{{ $errors->first('old_password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <label>New password</label>
                        <input type="password" name="new_password" placeholder="Repeat new password" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Repeat password</label>
                        <input type="password" name="retype_password" placeholder="Repeat new password" class="form-control">
                        @if ($errors->has('retype_password'))
                            <span class="help-block" style="color: red;">
                                <strong>{{ $errors->first('retype_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <label class="display-block">Upload profile image</label>
                        <input type="file" class="file-styled" name="photo" accept="image/*">
                        <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('page-js')
<script>
    $(function() {
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });
    });
</script>
@endsection
