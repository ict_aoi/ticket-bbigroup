
<div id="updateWorkDayModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue">
                <h5 class="modal-title">Workday Setting<span id="workday_span"></span></h5>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <form id="workdayForm" action="/work-day/update" method="post">
                @csrf
                <input type="hidden" id="id" name="id">
				<div class="modal-body">
                        <div class="tab-pane active" id="senin">
                            <div class="row">
                                <label class="col-md-4" for="day_name">
                                    Hari:
                                </label>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" value="" id="day_name" name="day_name" readonly>
                                </div>
                            </div><br><br><br>
                            <div class="row">
                                <label class="form-label col-md-4" for="active">
                                    Pilih Masuk atau Libur
                                </label>
                                <div class="col-md-8">
                                    <select id="active" name="active" class="form-control">
                                        <option value="0" selected>Libur</option>
                                        <option value="1">Masuk</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <label class="form-label col-md-4" for="masuk">Select Jam Masuk:</label>
                                <div class="col-md-4">
                                    <input type="time" id="start_hour" name="start_hour" class="form-control" > <br>
                                </div>
                            </div>
                            <div class="row">
                                <label class="form-label col-md-4" for="keluar">Select Jam keluar:</label>
                                <div class="col-md-4">
                                    <input type="time" id="end_hour" name="end_hour" class="form-control" > <br>
                                </div>
                            </div>
                        </div>
                    </div>


				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
            </form>
		</div>
	</div>
</div>
