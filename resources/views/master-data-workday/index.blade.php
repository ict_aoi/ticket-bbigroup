@extends('layouts.app',['active' => 'workday'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Issue Category</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Work Day Setting</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#updateWorkDayModal"><i class="icon-plus2 position-left"></i>Create New</a>
        </div>
	</div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="workdayTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Hari</th>
                        <th>Masuk/Tidak</th>
                        <th>Jam Masuk</th>
                        <th>Jam Pulang</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-modal')
	{{-- @include('master-data-workday._insert_modal') --}}
	@include('master-data-workday._update_modal')
@endsection
@section('page-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#active').change(function () {
            var value = $(this).val();
            console.log('value'+value);
            if(value==0){
                $('#start_hour').val(null).attr('readonly',true);
                $('#end_hour').val(null).attr('readonly',true);
            }else{
                $('#start_hour').attr('readonly',false);
                $('#end_hour').attr('readonly',false);
            }
        })
        $('#workdayTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/work-day/data',
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'day_name', name: 'day_name',searchable:true,orderable:true},
                {data: 'active', name: 'active',searchable:true,orderable:true},
                {data: 'start_hour', name: 'start_hour',searchable:true,orderable:true},
                {data: 'end_hour', name: 'end_hour',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:true,orderable:true},
            ]
        });

        var dtable = $('#workdayTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#workdayForm').submit(function (event){
            event.preventDefault();
            // var name = $('#update_name_priority').val();

            // if(!name){
            //     $("#alert_warning").trigger("click", 'Nama wajib diisi');
            //     return false
            // }

            $('#updateWorkDayModal').modal('hide');
            console.log($('#workdayForm').serialize());
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#workdayForm').attr('action'),
                        data: $('#workdayForm').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();

                        },
                        success: function (response) {
                            $('#updateWorkDayModal').modal('hide');
                            $('#workdayForm').trigger("reset");
                            $('#priorityTable').DataTable().ajax.reload();
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                        },
                        error: function (response) {
                            $.unblockUI();
                            console.log(response.responseJSON.responseText);
                            if (response.status == 422) {
                                // $("#alert_warning").trigger("click",response.responseJSON);

                            }
                            $('#updateWorkDayModal').modal();
                        }
                        // dtable.draw();
                    });
                }
            });
        });

    })
    function edit(url)
    {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#ffff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();

            }
        })
        .done(function (response) {
            console.log(response);
            $('#workdayForm').attr('action', response.url_update);
            $('#id').val(response.id);
            $('#day_name').val(response.day_name);
            $('#active').val(response.active).trigger('change');
            $('#start_hour').val(response.start_hour);
            $('#end_hour').val(response.end_hour);
            $('#updateWorkDayModal').modal();

        });
    }
</script>

@endsection
