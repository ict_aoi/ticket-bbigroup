<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Ticket System</title>
        <link rel="icon" href="{{ asset('images/logo-bbi.png') }}" type="image/x-icon">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        @yield('page-css')
    </head>
    <body>
        @include('includes.main_navbar')
        @include('includes.top_notif')
        <div class="page-container">
            <div class="page-content">
                @include('includes.side_navbar')
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                            @yield('page-content')
                            @yield('page-modal')
                            <script src="{{ mix('js/backend.js') }}"></script>
                            <script src="{{ mix('js/notification.js') }}"></script>
                            <script src="{{ mix('js/datepicker.js') }}"></script>
                            @yield('page-js')
                            <style>
                                td {
                                     text-transform:uppercase
                                }
                            </style>
                        <div class="footer text-muted">
                            &copy; <?php echo date("Y"); ?> <a href="{{ route('ticket.index') }}">Ticket System BBI Group</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
