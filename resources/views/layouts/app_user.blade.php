<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Ticket System</title>
        <link rel="icon" href="{{ asset('/images/logo-bbi.png')  }}" type="image/x-icon">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('page-css')
    </head>
    <body>
        @include('includes.top_notif')

        <div class="wa-cover">
            <br>
                <div class="col-md-12 wa-cover">
                    <div class="d-flex flex-column flex-1" >
                        <!-- Page container -->
                        <div class="page-container">

                            <!-- Page content -->
                            <div class="page-content layout-boxed-bg">

                                <!-- Main content -->
                                <div class="content-wrapper">

                                    <!-- Page header -->
                                    <div class="page-header page-header-default">
                                        <div class="page-header-content">
                                            <div class="page-title">
                                                <h4><a href="{{ route('ticket.index') }}"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">{{ $data_user->factory }}</span> - Ticket System</h4>
                                            </div>
                                        </div>

                                        <div class="breadcrumb-line">
                                            <ul class="breadcrumb">
                                                Selamat Datang, {{ $data_user->name }}
                                            </ul>
                                            <ul class="breadcrumb-elements">
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="content">
                                        @yield('page-content')
                                        @yield('page-modal')
                                        <script src="{{ mix('js/backend.js') }}"></script>
                                        <script src="{{ mix('js/notification.js') }}"></script>
                                        {{-- <script src="{{ mix('js/logon.js') }}"></script> --}}
                                        @yield('page-js')

                                    </div>
                                    <div class="footer text-muted">
                                        &copy; <?php echo date("Y"); ?>.<a href="{{ route('ticket.index') }}">Ticket System BBI Group</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{-- </div> --}}
        </div>
    </body>
</html>
