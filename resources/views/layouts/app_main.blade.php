<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Ticket System</title>
        <link rel="icon" href="{{ asset('images/logo-bbi.png') }}" type="image/x-icon">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('page-css')
    </head>
	<body class="login-cover">
        @include('includes.top_notif')
        <div class="navbar navbar-inverse bg-indigo">
            <div class="navbar-header">
                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple"><i class="icon-menu7"></i></a></li>
                </ul>
            </div>
            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('home') }}" class="legitRipple">
                        <i class="icon-lock"></i> Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="content">
                @yield('page-content')
                @yield('page-modal')
                <script src="{{ mix('js/logon.js') }}"></script>

                {{-- <script src="{{ mix('js/backend.js') }}"></script>
                <script src="{{ mix('js/notification.js') }}"></script> --}}
                @yield('page-js')
            <div class="footer text-muted">
                &copy; <?php echo date("Y"); ?>. <a class="nav-link" style="color:#999999 !important" href="{{ route('ticket.index') }}">Ticket System BBI Group</a>
            </div>
        </div>

    </body>
</html>
