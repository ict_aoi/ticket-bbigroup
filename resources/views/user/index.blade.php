@extends('layouts.app',['active' => 'user'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">User</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertUserModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="userTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Sub Department</th>
                        <th>Jenis Kelamin</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('user._insert_table')
	@include('user._insert_modal')
	@include('user._update_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/user.js') }}"></script>
@endsection
