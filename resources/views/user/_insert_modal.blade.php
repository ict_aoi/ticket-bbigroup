<div id="insertUserModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => route('user.store'),
						'method' => 'post',
						'class' => 'form-horizontal',
						'enctype' => 'multipart/form-data',
						'id'=> 'insert_user'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							@include('form.text', [
								'field' => 'nik',
								'label' => 'Nik',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'nik'
								]
							])

							@include('form.text', [
								'field' => 'name',
								'label' => 'Nama',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'name'
								]
							])

							@include('form.text', [
								'field' => 'email',
								'label' => 'Email',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'email'
								]
							])

							@include('form.select', [
                                            'field' => 'position',
                                            'label' => 'Position',
                                            'options' => [
                                                ''  => '-- Pilih Position--',
                                                '1' => 'Staff',
                                                '2' => 'Supervisor',
                                                '3' => 'Sub Dept Head',
                                                '4' => 'Dept Head',
                                            ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_position'
                                            ]
                                        ])

							@include('form.select', [
                                            'field' => 'department',
                                            'label' => 'Department',
                                            'options' => [
                                                ''         => '-- Pilih Department--',
                                            ]+$department,
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_department'
                                            ]
                                        ])
                            <div id="create_sub_department">
                                @include('form.select', [
                                                'field' => 'sub_department',
                                                'label' => 'Sub Department',
                                                'options' => [
                                                    ''         => '-- Pilih Sub Department--',
                                                ],
                                                'class' => 'select-search',
                                                'attributes' => [
                                                    'id' => 'select_sub_department'
                                                ]
                                            ])
                            </div>

							@include('form.select', [
								'field' => 'sex',
								'label' => 'Jenis Kelamin',
								'options' => [
									'' => '-- Pilih Jenis Kelamin --',
									'laki' => 'Laki',
									'perempuan' => 'Perempuan',
								],
								'class' => 'select-search',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'select_kelamin'
								]
							])

							@include('form.select', [
								'field' => 'role',
								'label' => 'Role',
								'options' => [
									'' => '-- Pilih Role --',
								]+$roles,
								'class' => 'select-search',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'select_role'
								]
							])

							@include('form.file', [
								'field' => 'photo',
								'label' => 'Upload Photo',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'help' => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
								'attributes' => [
									'id' => 'photo',
									'accept' => 'image/*'
								]
							])

							@include('form.password', [
								'field' => 'password',
								'label' => 'Password',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'password'
								]
							])
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h6 class="panel-title text-semibold">User roles &nbsp; <span class="label label-info heading-text">Mapping User with Role.</span></h6>
								</div>

								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Role</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="user_role"></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
