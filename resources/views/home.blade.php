@extends('layouts.app',['active' => 'dashboard'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard Test</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i
                class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>
    </div>
    {{-- <a href="/home/pluckTest" class="btn btn-success">Tes Pluck</a> --}}
</div>
@endsection
@section('page-content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-4">
            <div class="panel bg-teal-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        {{-- <span class="heading-text badge bg-teal-800">+53,6%</span> --}}
                    </div>

                    <h3 class="no-margin">{{$compact[0]}}</h3>
                    Request Waiting for Approval
                    <div class="text-muted text-size-small">...</div>
                </div>

                <div class="container-fluid">
                    <div id="members-online"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel bg-green-400">
                <div class="panel-body">
                    <div class="heading-elements">
                    </div>

                    <h3 class="no-margin">{{$compact[1]}}</h3>
                    Waiting for Execution
                    <div class="text-muted text-size-small">...</div>
                </div>

                <div id="server-load"></div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel bg-blue-400">
                <div class="panel-body">
                    <h3 class="no-margin">{{$compact[2]}}</h3>
                    New Support Ticket
                    <div class="text-muted text-size-small">...</div>
                </div>

                <div id="today-revenue"></div>
            </div>
        </div>

        {{-- <div class="col-md-3">
            <div class="panel bg-pink-400">
                <div class="panel-body">
                    <h3 class="no-margin">15</h3>
                    New Request Ticket
                    <div class="text-muted text-size-small">...</div>
                </div>

                <div id="today-revenue"></div>
            </div>
        </div> --}}

    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Ticket Support</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="myChart" style="width:200px !important; height:200px !important;"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">New Daily Support Issue</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="myChart2" style="width:200px !important; height:200px !important;"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">ICT Support Issues</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="myChart3" style="width:200px !important; height:200px !important;"></canvas>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('page-js')
<script>
    $(document).ready( function () {
        var data1 = 1;
        var data2 = 0;
        var data3 = 0;
        var data4 = [];
        var array1 = [];
        var array2 = [];
        var array_line1=[];
        var array_line2=[];
        const data = [50,60,70,80,90,100];
        var data5 = 0;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:"get",
            url: "/cekdata/chart",
            async: false,
            success: function (data){
                // console.log(data);
                data1 = data[0];
                data2 = data[1];
                data3 = data[2];
                data4 = data[3];
                data5 = data[4];
                for(var i = 0; i < data4.length; i++){
                    array1.push(data4[i].count);
                    array2.push(data4[i].issue_category);
                }
                for(var i = 0; i < data5.length; i++){
                    array_line1.push(data5[i][0]);
                    array_line2.push(data5[i][1]);
                }
                console.log(array_line1);
                console.log(array_line2);
                function1();
            },
            error: function (data){

            }

        });

    function function1(){
        console.log('di function'+data1);
    }
        // console.log('data 1:'+data1);
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Ticket Blm Selesai', 'Ticket Selesai'],
            datasets: [{
                label: '# of Votes',
                data: [data2, data3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(54, 162, 235, 0.7)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });


    var chart2 = document.getElementById('myChart2');
    var myChart = new Chart(chart2, {
        type: 'line',
        data: {
            labels: array_line1,
            datasets: [{
                label: 'New Daily Support Issue',
                data: array_line2,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.7)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var chart3 = document.getElementById('myChart3');
    var myChart = new Chart(chart3, {
        type: 'bar',
        data: {
            // labels: ['data', 'data', 'data', 'data', 'data', 'data'],
            labels: array2,
            datasets: [{
                // label: ['Data1', 'Data2'],
                label: 'Support Issues Category',
                data: array1,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 206, 86, 0.7)',
                    'rgba(75, 192, 192, 0.7)',
                    'rgba(153, 102, 255, 0.7)',
                    'rgba(255, 159, 64, 0.7)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
});
</script>
@endsection
