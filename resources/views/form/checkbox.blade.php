<div id="{{ $field }}_error" class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-lg-12 {{ isset($class) ? $class : '' }}">
	@if (isset($label))
	<label
		for="{{ $field }}" class="control-label {{ isset($label_col) ? $label_col : 'col-xs-2' }}"
	>
		{{ $label }}
	</label>
	@endif
	<div class="control-input {{ isset($form_col) ? $form_col : 'col-xs-10' }} {{ $errors->has($field) ? 'has-error' : ''}}">
			<label>
				{!! Form::checkbox (
						$field, 
						isset($default) ? $default : '',
						isset($checked) ? $checked : false,
						(isset($attributes) ? $attributes : [])
					)  
				!!}
				{{ isset($placeholder) ? $placeholder : ''}}
			</label>
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>