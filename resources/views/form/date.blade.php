<div id="{{ $field }}_error" class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-xs-12">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label {{ isset($label_col) ? $label_col : 'col-xs-2' }} text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="{{ isset($form_col) ? $form_col : 'col-xs-10'}}">
		<div class="input-group">
			{!!
				Form::date(
					$field,
					isset($default) ? $default : null,
					[
						'class' => 'form-control input-date '.(isset($class) ? $class : ''),
						'placeholder' => isset($placeholder) ? $placeholder : ''
					] + (isset($attributes) ? $attributes : [])
				)
			!!}
			{{-- <span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span> --}}
		</div>
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
		@if (isset($mandatory))
		<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
	</div>
</div>
