<div id="detailTicketModal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg" style="width:75%">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Ticket<span id="histori_stock"></span></h5>
			</div>

			<div class="modal-body">
				<div class="tabbable tab-content-bordered" id="tab" class="hidden">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#detail-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('progres')">Detail</a></li>
						<li><a href="#due-date-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('due-date')">Due Date</a></li>
                        <li><a href="#comment-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('comment')">Comment</a></li>
                        <li><a href="#approval-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('approval')">Approval</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active" id="detail-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminProject.storeProgres'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_progres_ticket'
                                ])
                            !!}
                            <div class="modal-body">

                                @include('form.select',[
                                    'field' => 'progress',
                                    'label' => 'Update Progress',
                                    'options' => [
                                        ''          =>'--Pilih Progress--',
                                        'desain-db'  =>'Desain Database',
                                        'desain-ui'  =>'Desain User Interface',
                                        'create-db'  =>'Membuat Database',
                                        'create-ui'  =>'Membuat User Interface',
                                        'local-testing'  =>'Local Testing',
                                        'upload-to-server'  =>'Upload ke Server',
                                        'testing'  =>'Final Testing',
                                        'done'  =>'Done',
                                    ],
                                    'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_progress_ticket'
                                    ]
                                ])

                                @include('form.select', [
                                            'field' => 'status',
                                            'label' => 'Update Status',
                                            'options' => [
                                                ''                           => '-- Pilih Status--',
                                                'closed'                     => 'Closed',
                                                'open'                       => 'Open',
                                                'on-process'                 => 'On Process',
                                            ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_status_ticket'
                                            ]
                                        ])


                                @include('form.textarea', [
                                    'field' => 'remark',
                                    'label' => 'Remark',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'remark',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])
                                {!! Form::hidden('ticket_id',null, array('id' => 'ticket_id')) !!}
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        {!! Form::close() !!}
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="progres_table">
									<thead>
										<tr>
											<th>Progress</th>
											<th>Status</th>
											<th>Keterangan</th>
											<th>Agent</th>
                                            <th>Created At</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
                        <div class="tab-pane" id="due-date-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminProject.updateDueDate'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_due_date_ticket'
                                ])
                            !!}
                            <div class="modal-body">

                                @include('form.select', [
                                            'field' => 'pic',
                                            'label' => 'Assign To',
                                            'options' => [
                                                '' => '-- Pilih Agen--',
                                                ]+$users,
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'pic'
                                            ]
                                        ])

                                @include('form.select', [
                                    'field' => 'status',
                                    'label' => 'Priority',
                                    'options' => [
                                        'low' => 'Low',
                                        'normal' => 'Normal',
                                        'high' => 'High',
                                    ],
                                    'class' => 'select-search',
                                    'attributes' => [
                                        'id' => 'priority'
                                    ]
                                ])

                                @include('form.date', [

                                'field' 		=> 'due_date',
                                'label' 		=> 'Due Date',
                                'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
                                'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
                                'default'		=> \Carbon\Carbon::now()->format('Y-m-d'),
                                'placeholder' 	=> 'yyyy-mm-dd',
                                // 'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'due_date',
                                    // 'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])


                                @include('form.textarea', [
                                    'field' => 'remark',
                                    'label' => 'Remark',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'remark',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])
                                {!! Form::hidden('__ticket_id',null, array('id' => '__ticket_id')) !!}
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        {{-- Approval Modal --}}
                        <div class="tab-pane" id="approval-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminProject.updateApproval'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_approval_ticket'
                                ])
                            !!}
                            <div class="modal-body">
                                @include('form.select', [
                                    'field' => 'status',
                                    'label' => 'Approval',
                                    'options' => [
                                        ''=>'-- Pilih Approval --',
                                        'approve' => 'Approve',
                                        'reject' => 'Reject',
                                    ],
                                    'class' => 'select-search',
                                    'attributes' => [
                                        'id' => 'select_approval_ticket'
                                    ]
                                ])
                                @include('form.textarea', [
                                    'field' => 'comment',
                                    'label' => 'Comment Here',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'remark_approval',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])
                                {!! Form::hidden('__ticket_id',null, array('idd' => '__ticket_id')) !!}
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        {{-- START PANEL --}}
						<div class="tab-pane" id="comment-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminProject.storeComment'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_comment_ticket'
                                ])
                            !!}
                            <div class="modal-body">

                                @include('form.textarea', [
                                    'field' => 'comment',
                                    'label' => 'Add Comment',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'comment',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])

                                @include('form.file', [
                                    'field'      => 'r_upload',
                                    'label'      => 'Attachment',
                                    'mandatory'  => '*Max 5MB',
                                    'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id'       => 'r_upload',
                                        'rows'     => 5,
                                        'style'    => 'resize: none;',
                                        // 'required' => ''
                                    ]
                                ])

                                {!! Form::hidden('_ticket_id',null, array('id' => '_ticket_id')) !!}

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>

                        {!! Form::close() !!}
                        <div class="table-responsive">
                            <table class="table datatable-basic table-striped table-hover table-responsive" id="comment_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Created At</th>
                                        <th>Name</th>
                                        <th>Comment</th>
                                        <th>Attactment</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
			    <div class="modal-footer">
				{!! Form::hidden('active_modal_tab',null, array('id' => 'active_modal_tab')) !!}
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
