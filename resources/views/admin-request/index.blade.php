@extends('layouts.app',['active' => 'admin-request'])
@section('page-css')
<style>
	.daterangepicker{
		z-index:9999999 !important;
	}
</style>

@endsection
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ticket Request</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Ticket Request</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">
	{!!
		Form::open(array(
			'class' => 'form-horizontal',
			'role' => 'form',
			'url' => '#',
			'method' => 'get',
			'target' => '_blank'
		))
	!!}

		@include('form.select', [
			'field' 			=> 'factory',
			'label' 			=> 'Factory',
			'default' 			=> auth::user()->warehouse,
			'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				''      => '-- All Factory --',
				'AOI1' => 'AOI 1',
				'AOI2' => 'AOI 2',
				'BBI'   => 'BBI',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_factory'
			]
		])

		@include('form.select', [
					'field' => 'status',
					'label' => 'Status',
					'label_col' => 'col-md-2 col-lg-2 col-sm-12',
					'form_col' => 'col-md-10 col-lg-10 col-sm-12',
					'options' => [
						'' => '-- All --',
						'open' => 'Open',
						'closed' => 'Closed',
					],
					'class' => 'select-search',
					'attributes' => [
						'id' => 'select_status'
					]
				])
        @permission(['menu-permission'])
		@include('form.select', [
					'field' => 'dept',
					'label' => 'Dept',
					'label_col' => 'col-md-2 col-lg-2 col-sm-12',
					'form_col' => 'col-md-10 col-lg-10 col-sm-12',
					'options' => [
						'' => '-- All --',
					],
					'class' => 'select-search',
					'attributes' => [
						'id' => 'select_dept'
					]
				])
        @endpermission
		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
	</div>
        <div class="panel panel-default border-grey">
		<div class="panel-body">
		<div class="table-responsive">
            <table class="table table-basic table-condensed" id="adminRequestTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kode Ticket</th>
                        <th></th>
                        <th>Status</th>
                        <th>Is Confirm Guest</th>
                        <th>Is Confirm Host</th>
                        <th>Nama</th>
                        <th>Dept</th>
                        <th>Category</th>
                        <th>Judul</th>
                        <th>Created_at</th>
                        <th>Priority</th>
                        <th>PIC</th>
                        <th>Due Date</th>
                        <th>Factory</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('admin-request._request_modal')
@endsection

@section('page-js')
<script>

$(document).ready( function () {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    showDept();
    showStatus();
	$('#adminRequestTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		deferRender:true,
        // responsive:true,
		ajax: {
			type: 'GET',
			url: '/admin-request/data',
			data: function(d) {
                return $.extend({}, d, {
                    "factory"     : $('#select_factory').val(),
                    "last_status": $('#select_status').val(),
                    "department" : $('#select_dept').val(),
                });
           }
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'code', name: 'code',searchable:true,orderable:false},
			{data: 'notifikasi', name: 'notifikasi',searchable:true,orderable:true},
			{data: 'last_status', name: 'last_status',searchable:true,orderable:true},
			{data: 'is_confirm_guest', name: 'is_confirm_guest',searchable:true,orderable:true},
			{data: 'is_confirm_host', name: 'is_confirm_host',searchable:true,orderable:true},
			{data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'department', name: 'department',searchable:true,orderable:true},
            {data: 'issue_category', name: 'issue_category',searchable:true,orderable:true},
			{data: 'title', name: 'title',searchable:true,orderable:true},
            {data: 'created_at', name: 'created_at',searchable:true,orderable:true},
            {data: 'priority', name: 'priority',searchable:true,orderable:true},
            {data: 'handled_by', name: 'handled_by',searchable:true,orderable:true},
            {data: 'due_date', name: 'due_date',searchable:true,orderable:true},
			{data: 'factory', name: 'factory',searchable:true,orderable:true},
		]
	});

    function showDept(){
        $.ajax({
            type: "get",
            url: "/admin-request/deptSelect",
            success: function (data) {
                console.log(data[0]);
                var $dept =  $('#select_dept');
                $dept.empty();
                $dept.append('<option value="" selected hidden>--All--</option>');
                for (var i = 0; i < data[0].length; i++) {

                    $dept.append('<option value=' + data[0][i].department + '>' + data[0][i].department + '</option>');
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
    function showStatus(){
        $.ajax({
            type: "get",
            url: "/admin-request/deptSelect",
            success: function (data) {
                console.log(data[1]);
                var $status =  $('#select_status');
                $status.empty();
                $status.append('<option value="" selected hidden>--ALL--</option>');
                for (var i = 0; i < data[1].length; i++) {

                    $status.append('<option value=' + data[1][i].status + '>' + data[1][i].status + '</option>');
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

	var dtable = $('#adminRequestTable').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#select_factory').on('change',function(){
        console.log($(this).val());
        dtable.draw();
    });
	$('#select_dept').on('change',function(){
        console.log($(this).val());
        dtable.draw();
    });

    $('#select_status').on('change', function () {
        console.log($(this).val());
        dtable.draw();
    });


	$('#update_progres_ticket').submit(function (event){
		event.preventDefault();
		var status = $('#select_status_ticket').val();
		var remark = $('#remark').val();
        console.log(status);
		if(!status){
			$("#alert_warning").trigger("click", 'Status wajib dipilih');
			return false
		}
		else if(!remark){
			$("#alert_warning").trigger("click", 'Description wajib diisi');
			return false
		}

		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#update_progres_ticket').attr('action'),
					data: $('#update_progres_ticket').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#update_progres_ticket').trigger("reset");
						$('#adminRequestTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});
	$('#update_approval_ticket').submit(function (event){
		event.preventDefault();
		var approval = $('#select_approval_ticket').val();
		var remark = $('#remark_approval').val();
        var id = $('#__ticket_id').val();
        var dataal= $('#update_due_date_ticket').serialize();
        console.log('remark', remark);
        console.log(approval);
        console.log('tiket', id);
        console.log('data', dataal);
		if(!approval){
			$("#alert_warning").trigger("click", 'approval wajib dipilih');
			return false
		}
		else if(!remark){
			$("#alert_warning").trigger("click", 'Description wajib diisi');
			return false
		}

		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: '/admin-request/update-approval',
					// data: $('#update_approval_ticket').serialize(),
                    data: {
                        remark: remark,
                        __ticket_id: id,
                        status: approval
                    },
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#approval-ticket').trigger("reset");
						$('#adminRequestTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
                        console.log('error', response);
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});

	$('#update_due_date_ticket').submit(function (event){
		event.preventDefault();
		var pic = $('#pic').val();
		var priority = $('#priority').val();
		var due_date = $('#due_date').val();
        console.log(due_date);
        console.log($('#update_due_date_ticket').serialize());
        console.log(pic);

		if(!pic){
			$("#alert_warning").trigger("click", 'PIC wajib dipilih');
			return false
		}
		else if(!priority){
			$("#alert_warning").trigger("click", 'Priority wajib diisi');
			return false
		}

		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#update_due_date_ticket').attr('action'),
					data: $('#update_due_date_ticket').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#update_due_date_ticket').trigger("reset");
						$('#adminRequestTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
                        console.log('error', response);
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});


	$('#update_comment_ticket').submit(function (event){
		event.preventDefault();
		var comment = $('#comment').val();
        console.log(comment);

		if(!comment){
			$("#alert_warning").trigger("click", 'comment wajib diisi');
			return false
		}

		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				var formData = new FormData($('#update_comment_ticket')[0]);
				$.ajax({
					type: "POST",
					url: $('#update_comment_ticket').attr('action'),
					// data: $('#update_comment_ticket').serialize(),
					data: formData,
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#update_comment_ticket').trigger("reset");
						$('#comment_table').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
						$('#detailTicketModal').modal();
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});

});

function showDetail(ticket_id)
{
	$('#ticket_id').val(ticket_id);
	$('#_ticket_id').val(ticket_id);
	$('#__ticket_id').val(ticket_id);
    $.get('/admin-request/edit/' + ticket_id , function (data) {
            console.log(data);
            $('#select_status_ticket').val(data.last_status).trigger('change');
            $('#pic').val(data.handled_by).trigger('change');
            $('#priority').val(data.priority).trigger('change');
            $('#due_date').val(data.due_date);
            if (data.approval != null){
                $('#select_approval_ticket').val(data.approval).trigger('change');
            }

        })
	$('#detailTicketModal').modal();
	progresDataTables();

	var dtableProgres = $('#progres_table').dataTable().api();
	$("#progres_table.dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtableProgres.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtableProgres.search("").draw();
			}
			return;
	});
	dtableProgres.draw();
}

function changeTabModal(status)
{
    $('#active_modal_tab').val(status).trigger('change');
}

function progresDataTables()
{
    var ticket_id = $('#ticket_id').val();
	// console.log(ticket_id);
  	$('#progres_table').DataTable().destroy();
    $('#progres_table tbody').empty();

    var progres_table = $('#progres_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/admin-request/progres',
			data: function(d) {
                return $.extend({}, d, {
					"ticket_id" : ticket_id,
				});
           },
        },
        columns: [
            {data: 'status', name: 'status',searchable:true,orderable:false},
			{data: 'remark', name: 'remark',searchable:true,orderable:false},
			{data: 'user_id', name: 'user_id',searchable:true,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:true,orderable:false},
        ]
    });
}

// function showComment()
// {
// 	var _ticket_id = $('#_ticket_id').val();
// 	$.ajax({
// 		type: "get",
// 		url: '/admin-support/comment',
// 		data:{
//                 "_ticket_id"    : _ticket_id,
//            },
// 	});
// }

function commentDataTables()
{
    $('#comment_table').DataTable().destroy();
		$('#comment_table tbody').empty();
		var _ticket_id = $('#_ticket_id').val();
		var comment_table = $('#comment_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-request/comment',
				data: function(d){
					return $.extend({},d,{
						'_ticket_id' : _ticket_id,
					});
				},
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_at', name: 'created_at',searchable:true,orderable:true},
				{data: 'user_id', name: 'user_id',searchable:true,orderable:true},
				{data: 'comment', name: 'comment',searchable:true,orderable:true},
				{data: 'action', name: 'action',searchable:true,orderable:true},
			]
		});

}

$('#active_modal_tab').on('change',function()
    {
		var active_tab = $('#active_modal_tab').val();
        if(active_tab == 'progres')
        {
			progresDataTables();

			var dtableProgres = $('#progres_table').dataTable().api();
			$("#progres_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableProgres.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableProgres.search("").draw();
					}
					return;
			});
			dtableProgres.draw();


        }else if(active_tab == 'comment')
        {
			commentDataTables();

			var dtableComment = $('#comment_table').dataTable().api();
			$("#comment_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableComment.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableComment.search("").draw();
					}
					return;
            });
            dtableComment.draw();

        }
    });

</script>
@endsection
