<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header">
        <a class="navbar-brand" href="#" style="color:#fff">TICKET BINA BUSANA INTERNUSA</a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            <li class="dropdown">
                <p class="navbar-text">
                    <span class="label bg-success-400">Online</span>
                </p>
            </li>
        </ul>

        <div class="navbar-right">
            <p class="navbar-text">Welcome, {{ Auth::user()->name }}</p>
        </div>
    </div>
</div>
