<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    @permission(['menu-user','menu-role','menu-permission'])
                    <li class="navigation-header"><span>Master Data</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>User Management</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-permission')
                                <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Permission</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Role</a></li>
                            @endpermission
                            @permission('menu-user')
                                <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">User</a></li>
                            @endpermission
                        </ul>
                    </li>
                    <li class="">
                    <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Issue</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-permission')
                                <li class="{{ $active == 'issue-category' ? 'active' : '' }}" class="legitRipple"><a href="{{ route('issue.index') }}" >Category</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'issue-detail' ? 'active' : '' }}" class="legitRipple"><a href="{{ route('detailIssue.index') }}" >Detail Issue</a></li>
                            @endpermission
                        </ul>
                    </li>
                    <li class="">
                    <a href="#" class="has-ul legitRipple"><i class="icon-office"></i> <span>Department</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-permission')
                                <li class="{{ $active == 'department' ? 'active' : '' }}" class="legitRipple"><a href="{{ route('department.index') }}" >Department</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'sub-department' ? 'active' : '' }}" class="legitRipple"><a href="{{ route('subdepartment.index') }}" >Sub Department</a></li>
                            @endpermission
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" ><i class="icon-books"></i> <span>Knowledge Base</span></a>
                    </li>
                    <li class = "{{ $active == 'priority' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('priority.index') }}" ><i class = "icon-pushpin"></i> <span>Priority</span></a>
                    <li class = "{{ $active == 'workday' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('workDay.index') }}" ><i class = " icon-calendar2"></i> <span>WorkDay Setting</span></a>
                    @endpermission
                    {{-- @permission(['menu-permission'])
                    <li class = "navigation-header"><span>Ticket</span> <i class   = "icon-archive" title  = "Reports"></i></li>
                    <li class = "{{ $active == 'admin-support' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminSupport.index') }}" ><i class = "icon-phone-wave"></i> <span>Support</span></a>
                    </li>
                    <li class = "{{ $active == 'admin-request' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminRequest.index') }}" ><i class = "icon-users"></i> <span>Request</span></a>
                    </li>
                    <li class = "{{ $active == 'admin-project' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminProject.index') }}" ><i class = "icon-stackoverflow"></i> <span>Project</span></a>
                    </li>
                    @endpermission --}}


                    @permission([['request-ict'],['request-hr'], ['request-mekanik'],['menu-permission']])
                    {{-- @permission('request-hr') --}}
                    <li class = "navigation-header"><span>Ticket</span> <i class   = "icon-archive" title  = "Reports"></i></li>
                    <li class = "{{ $active == 'admin-support' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminSupport.index') }}" ><i class = "icon-phone-wave"></i> <span>Support</span></a>
                    </li>
                    <li class = "{{ $active == 'admin-request' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminRequest.index') }}" ><i class = "icon-users"></i> <span>Request</span></a>
                    </li>
                    @endpermission
                    @permission([['request-ict'], ['menu-permission']])
                    <li class = "{{ $active == 'admin-project' ? 'active' : '' }}" class = "legitRipple"><a href = "{{ route('adminProject.index') }}" ><i class = "icon-stackoverflow"></i> <span>Project</span></a>
                    </li>
                    @endpermission
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
