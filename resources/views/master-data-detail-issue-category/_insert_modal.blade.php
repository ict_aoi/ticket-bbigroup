<div id="insertDetailIssueModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">   
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => route('detailIssue.store'),
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'insert_detail_issue'
					])
				!!}
				<div class="modal-body">

					@include('form.select', [
								'field' => 'issue',
								'label' => 'Issue Category',
								'options' => [
									'' => '-- Pilih Category Issue--',
								]+$issue,
								'class' => 'select-search',
								'attributes' => [
									'id' => 'select_issue'
								]
							])

					@include('form.text', [
						'field' => 'name',
						'label' => 'Nama',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'name'
						]
					])

					@include('form.textarea', [
						'field' => 'description',
						'label' => 'Description',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'description',
							'rows' => 5,
							'style' => 'resize: none;'
						]
					])
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>