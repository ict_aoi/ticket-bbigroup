@extends('layouts.app',['active' => 'permission'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Permission</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Permission</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertPermissionModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="permissionTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('permission._insert_modal')
	@include('permission._update_modal')
@endsection

@section('page-js')
<script>
	$(document).ready( function () {
		$('#permissionTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/permission/data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'display_name', name: 'display_name',searchable:true,orderable:true},
				{data: 'description', name: 'description',searchable:true,orderable:true},
				{data: 'action', name: 'action',searchable:true,orderable:true},
			]
		});

		var dtable = $('#permissionTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();

		$('#insert_permission').submit(function (event){
			event.preventDefault();
			var name = $('#name').val();
			
			if(!name){
				$("#alert_warning").trigger("click", 'Nama wajib diisi');
				return false
			}
			
			$('#insertPermissionModal').modal('hide');
			bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
				if(result){
					$.ajax({
						type: "POST",
						url: $('#insert_permission').attr('action'),
						data: $('#insert_permission').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function (response) {
							$('#insertPermissionModal').modal('hide');
							$('#insert_permission').trigger("reset");
							$('#permissionTable').DataTable().ajax.reload();
							$("#alert_success").trigger("click", 'Data Berhasil disimpan');
						},
						error: function (response) {
							$.unblockUI();
							if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
							$('#insertPermissionModal').modal();
						}
					});
				}
			});
		});

		$('#update_permission').submit(function (event){
			event.preventDefault();
			var name = $('#update_name').val();
			
			if(!name){
				$("#alert_warning").trigger("click", 'Nama wajib diisi');
				return false
			}
			
			$('#updatePermissionModal').modal('hide');
			bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
				if(result){
					$.ajax({
						type: "PUT",
						url: $('#update_permission').attr('action'),
						data: $('#update_permission').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function (response) {
							$('#updatePermissionModal').modal('hide');
							$('#update_permission').trigger("reset");
							$('#permissionTable').DataTable().ajax.reload();
							$("#alert_success").trigger("click", 'Data Berhasil disimpan');
						},
						error: function (response) {
							$.unblockUI();
							if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
							$('#updatePermissionModal').modal();
						}
					});
				}
			});
		});
		
	});

	function edit(url)
	{
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#update_permission').attr('action', response.url_update);
			$('#update_name').val(response.name);
			$('#update_description').val(response.description);
			$('#updatePermissionModal').modal();

		});
	}
	
	function hapus(url)
	{
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type: "delete",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function (response) {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
			}
		}).done(function ($result) {
			$('#permissionTable').DataTable().ajax.reload();
			$("#alert_success").trigger("click", 'Data Berhasil hapus');
		});
	}
</script>
@endsection
