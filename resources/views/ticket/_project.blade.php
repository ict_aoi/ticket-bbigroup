{!!
	Form::open([
		'role'   => 'form',
		'url'    => route('ticket.store_project'),
		'method' => 'post',
		'class'  => 'form-horizontal',
		'id'     => 'insert_project'
	])
!!}

{{-- <div> --}}
<div class="col-md-6">
	@include('form.text', [
		'field'      => 'p_nik',
		'label'      => 'NIK',
		'default'	 => $data_user->nik,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_nik',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'p_name',
		'label'      => 'Name',
		'default'	 => $data_user->name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_name',
			'readonly' => '',
			// 'required' => ''
		]
	])

</div>

<div class="col-md-6">
	@include('form.text', [
		'field'      => 'p_department',
		'label'      => 'Department',
		'default'	 => $data_user->department_name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_department',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'p_factory',
		'label'      => 'Factory',
		'default'	 => $data_user->factory,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_factory',
			'readonly' => '',
			// 'required' => ''
		]
	])
</div>
<div class="col-md-12">
	@include('form.text', [
		'field'      => 'p_email_anda',
		'label'      => 'Email Anda',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_email_anda',
			// 'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'p_email_atasan',
		'label'      => "Email Atasan Anda",
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_email_atasan',
			// 'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.select', [
		'field'     => 'p_select_issue',
		'label'     => 'Issue Category',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Category Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 'p_select_issue'
		]
	])
	@include('form.select', [
		'field'     => 'p_select_detail_issue',
		'label'     => 'Detail Issue',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Detail Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 'p_select_detail_issue',
			// 'required' => ''

		]
	])
</div>

<div class="col-md-12">
	@include('form.text', [
		'field'      => 'p_title',
		'label'      => 'Title',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_title',
			// 'readonly' => '',
			// 'required' => ''
		]
	])
	@include('form.textarea', [
		'field'      => 'p_description',
		'label'      => 'Description',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'p_description',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
</div>

<div class="col-md-12">

	@include('form.file', [
		'field'      => 'p_upload',
		'label'      => 'Attachment',
		'mandatory'  => '*Max 5MB',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_upload',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
</div>

<div class="label-block text-center">
	{{-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> --}}
	<button type="submit" class="btn btn-primary">Submit form</button>
</div>
{!! Form::close() !!}
