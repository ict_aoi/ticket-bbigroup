<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($close))
                <li><a href="#" onclick="close_ticket('{!! $close !!}')" ><i class="icon-checkmark-circle2"></i> Close</a></li>
            @endif
            @if (isset($cancel))
                <li><a href="#" onclick="cancel_ticket('{!! $cancel !!}')"><i class="icon-close2"></i> Cancel</a></li>
            @endif
        </ul>
    </li>
</ul>
