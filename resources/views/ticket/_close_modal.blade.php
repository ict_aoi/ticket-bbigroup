<div id="closeTicketModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Close Ticket</h5>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!! Form::hidden('close_ticket_id',null, array('id' => 'close_ticket_id')) !!}
				{!!
					Form::open([
						'role'    => 'form' ,
						'url'     => '#',
						'method'  => 'POST' ,
						'class'   => 'form-horizontal' ,
						'enctype' => 'multipart/form-data' ,
						'id'      => 'user_close_ticket'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@include('form.text', [
								'field' => 'keterangan',
								'label' => 'Keterangan',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'close_keterangan'
								]
							])
						</div>
						{!! Form::hidden('close_user_id',null, array('id' => 'close_user_id')) !!}
						{{-- {!! Form::hidden('mappings', '[]', array('id' => 'update_mappings')) !!} --}}
										
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
				{!! Form::close() !!}	
			
		</div>
	</div>
</div>