@extends('layouts.app_main',['active' => 'ticket'])

@section('page-css')
    <style>
        #wrapper {
            top: 300px;
            right: 50px;
            position: absolute;
        }
    </style>
@endsection

@section('page-content')
<!-- Search field -->
    {{-- <div id="wrapper"> --}}

        <br><br><br><br><br><br><br><br>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="text-center">
                    <img src="{{ asset('/images/logo-bbi-full.png')  }}" class="img-fluid" style="width:50%;height:100%;" alt="Responsive image">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default text-center" >
                    <div class="heading-elements">
                    </div>

                    <div class="panel-body" >
                        {{-- <form action="#" class="main-search"> --}}
                            {{-- <div class="input-group content-group">
                                <div class="has-feedback has-feedback-left">
                                    <input id="nik" type="text" class="form-control input-xlg" value="" placeholder="Silahkan Masukkan NIK Anda" >
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>

                                <div class="input-group-btn">
                                    <button id="logon" class="btn btn-primary btn-xlg">Search</button>
                                </div>
                            </div> --}}
                        {{-- </form> --}}

                        <form action="/ticket/home" method="post">
                            @csrf
                            <div class="input-group content-group">
                                <div class="has-feedback has-feedback-left">
                                    <input id="nik_2" type="text" name="nik" class="form-control input-xlg" value="" placeholder="Silahkan Masukkan NIK Anda (ini form POST)" >
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                    @if(Session::has('pesan'))
                                        <div><p class="text-danger">{{Session::get('pesan')}}</p></div>
                                    @endif
                                </div>
                                <div class="input-group-btn">
                                    {{-- <button id="logon_2" class="btn btn-primary btn-xlg">Search</button> --}}
                                    <input type="submit" value="Simpan Data" class="btn btn-primary btn-xlg">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    {{-- </div> --}}
@endsection

@section('page-js')
    {{-- <script src="{{ mix('js/backend.js') }}"></script>
    <script src="{{ mix('js/notification.js') }}"></script> --}}
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#nik").keyup(function(event) {
            if (event.keyCode === 13) {
                $("#logon").click();
            }
        });

        $('#logon').on('click', function(event, arg1) {

            var nik_user = $('#nik').val();
            console.log(nik_user);

            // console.log(!nik_user);
            if(!nik_user){
                // alert('hai');
                $("#alert_warning").trigger("click", 'Masukkan NIK anda terlebih dahulu!');
                // return false
            }
            else{
                $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/ticket/logon',
                        data: {
                                id : nik_user
                            },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner3 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            // window.open(response, '_self');
                            console.log('sukses');
                            console.log(response);
                            // e.preventDefault();
                            var nik = response.nik;
                            var url = '{{ route("ticket.home", [":nik"] ) }}';
                            url = url.replace(':nik', nik);

                            window.location.assign(url);
                        },
                        error: function(response) {
                            console.log('error', response);
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                            if (response.status == 500) $("#alert_warning").trigger("click", "Login gagal, info ICT");
                            // $('#confirmationModal').modal();
                        }
                    })

                    // .done(function(response) {
                    //     console.log(response);
                    //     var message = response.message;

                    //     $("#alert_warning").trigger("click", ''+message+'');
                    // })
            }
        });


        // $.ajax({
        //     type: "POST",
        //     url: $('#form').attr('action'),
        //     data: $('#form').serialize(),
        //     beforeSend: function() {
        //         $.blockUI({
        //             message: '<i class="icon-spinner4 spinner"></i>',
        //             overlayCSS: {
        //                 backgroundColor: '#fff',
        //                 opacity: 0.8,
        //                 cursor: 'wait'
        //             },
        //             css: {
        //                 border: 0,
        //                 padding: 0,
        //                 backgroundColor: 'transparent'
        //             }
        //         });
        //     },
        //     complete: function() {
        //         $.unblockUI();
        //     },
        //     success: function(response) {

        //         window.open(response, '_self');

        //     },
        //     error: function(response) {
        //         $.unblockUI();
        //         if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
        //         if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
        //         $('#confirmationModal').modal();
        //     }
        // });
    </script>

@endsection
