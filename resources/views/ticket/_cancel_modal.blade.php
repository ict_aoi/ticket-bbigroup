<div id="cancelTicketModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-red">
				<h5 class="modal-title">Cancel Ticket</h5>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!! Form::hidden('cancel_ticket_id',null, array('id' => 'cancel_ticket_id')) !!}
				{!!
					Form::open([
						'role'    => 'form' ,
						'url'     => '#',
						'method'  => 'POST' ,
						'class'   => 'form-horizontal' ,
						'enctype' => 'multipart/form-data' ,
						'id'      => 'user_cancel_ticket'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@include('form.text', [
								'field' => 'keterangan',
								'label' => 'Keterangan',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'cancel_keterangan'
								]
							])
						</div>
						{!! Form::hidden('cancel_user_id',null, array('id' => 'cancel_user_id')) !!}
						{{-- {!! Form::hidden('mappings', '[]', array('id' => 'update_mappings')) !!} --}}
										
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Submit form</button>
				</div>
				{!! Form::close() !!}	
			
		</div>
	</div>
</div>