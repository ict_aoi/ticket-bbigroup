{!!
	Form::open([
		'role'   => 'form',
		'url'    => route('ticket.store_support'),
		'method' => 'post',
		'class'  => 'form-horizontal',
		'id'     => 'insert_support'
	])
!!}

{{-- <div> --}}
<div class="col-md-6">
	@include('form.text', [
		'field'      => 's_nik',
		'label'      => 'NIK',
		'default'	 => $data_user->nik,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_nik',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 's_name',
		'label'      => 'Name',
		'default'	 => $data_user->name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_name',
			'readonly' => '',
			// 'required' => ''
		]
	])

</div>

<div class="col-md-6">
	@include('form.text', [
		'field'      => 's_department',
		'label'      => 'Department',
		'default'	 => $data_user->department_name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_department',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 's_factory',
		'label'      => 'Factory',
		'default'	 => $data_user->factory,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_factory',
			'readonly' => '',
			// 'required' => ''
		]
	])
</div>
<div class="col-md-12">
	@include('form.text', [
		'field'      => 's_email_anda',
		'label'      => 'Your Email',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_email_anda',
			// 'readonly' => '',
			// 'required' => ''
		]
	])
    @include('form.select', [
		'field'     => 's_select_department',
		'label'     => 'Department',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Department Tujuan--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 's_select_department',
			// 'required' => ''

		]
	])
	@include('form.select', [
		'field'     => 's_select_issue',
		'label'     => 'Issue Category',
		'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Category Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 's_select_issue'
		]
	])
	@include('form.select', [
		'field'     => 's_select_detail_issue',
		'label'     => 'Detail Issue',
		'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Detail Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 's_select_detail_issue',
			// 'required' => ''

		]
	])
</div>

<div class="col-md-12">
	@include('form.text', [
		'field'      => 's_title',
		'label'      => 'Title',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_title',
			// 'readonly' => '',
			// 'required' => ''
		]
	])
	@include('form.textarea', [
		'field'      => 's_description',
		'label'      => 'Description',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_description',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
</div>



<div class="col-md-12">

	@include('form.file', [
		'field'      => 's_upload',
		'label'      => 'Attachment',
		'mandatory'  => '*Max 5MB',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 's_upload',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
	{{-- <div class="form-group">
		<div class="row">
			<label class="display-block">Upload profile image</label>
			<input type="file" class="file-styled" name="doc" id="doc">
			<span class="help-block">Max file size 5MB</span>
		</div>
	</div> --}}
</div>

<div>
	<a id="tambahIssue" class="btn btn-success">
		Tambah Issue
	</a>
</div>
<div id="data_2">
	@include('form.select', [
		'field'     => 's_select_issue_2',
		'label'     => 'Issue Category 2',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Category Issue 2--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 's_select_issue_2'
		]
	])
	@include('form.select', [
		'field'     => 's_select_detail_issue_2',
		'label'     => 'Detail Issue 2',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Detail Issue 2--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 's_select_detail_issue_2',
			// 'required' => ''

		]
	])
    <div class="col-md-12">
        @include('form.text', [
            'field'      => 's_title_2',
            'label'      => 'Title',
            // 'mandatory'  => '*Wajib diisi',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id'       => 's_title_2',
                // 'readonly' => '',
                // 'required' => ''
            ]
        ])
        @include('form.textarea', [
            'field'      => 's_description_2',
            'label'      => 'Description',
            // 'mandatory'  => '*Wajib diisi',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id'       => 's_description',
                'rows'     => 5,
                'style'    => 'resize: none;',
                // 'required' => ''
            ]
        ])
    </div>
	<div>
		<a id="tambahIssue2" class="btn btn-success">
			Tambah Issue
		</a>
	</div>
</div>
<div id="data_3">
	@include('form.select', [
		'field'     => 's_select_issue_3',
		'label'     => 'Issue Category 3',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Category Issue 3--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 's_select_issue_3'
		]
	])
	@include('form.select', [
		'field'     => 's_select_detail_issue_3',
		'label'     => 'Detail Issue 3',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Detail Issue 3--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 's_select_detail_issue_3',
			// 'required' => ''

		]
	])
    <div class="col-md-12">
        @include('form.text', [
            'field'      => 's_title_3',
            'label'      => 'Title',
            // 'mandatory'  => '*Wajib diisi',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id'       => 's_title_3',
                // 'readonly' => '',
                // 'required' => ''
            ]
        ])
        @include('form.textarea', [
            'field'      => 's_description_3',
            'label'      => 'Description',
            // 'mandatory'  => '*Wajib diisi',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id'       => 's_description',
                'rows'     => 5,
                'style'    => 'resize: none;',
                // 'required' => ''
            ]
        ])
    </div>
</div>


<div class="label-block text-center">
	{{-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> --}}
	<button type="submit" class="btn btn-primary">Submit form</button>
</div>
{!! Form::close() !!}
