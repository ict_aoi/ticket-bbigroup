@extends('layouts.app_user',['active' => 'ticket-user'])

@section('page-css')
    <style>
        .navbar-inverse .navbar-nav>.active>a,
        .navbar-inverse .navbar-nav>.active>a:hover,
        .navbar-inverse .navbar-nav>.active>a:focus {
            color: black;
            background-color: transparent;
        }

        .navbar-inverse .navbar-nav>.open>a,
        .navbar-inverse .navbar-nav>.open>a:hover,
        .navbar-inverse .navbar-nav>.open>a:focus {
            background-color: transparent;
            color: black;
        }

        .navbar-default .navbar-collapse,
        .navbar-default .navbar-form {
            border-color: black;
        }

        /* .navbar-inverse .navbar-nav > li > a:hover,
      .navbar-inverse .navbar-nav > li > a:focus {
      color: black;
      background-color: transparent;
     } */
        .legitRipple-ripple {
            background-color: black;
        }


        .navigation>li.active>a>[class*=text-] {
            color: black;
        }

    </style>
@endsection
@section('page-content')


    <div class="panel panel-flat">
        <div class="panel-heading">
            <span class="label label-default">Menu</span>
        </div>

        <div class="container-fluid">
            <div class="content-group tab-content-bordered navbar-component">
                <div class="navbar navbar-inverse bg-teal-400" style="position: relative;">
                    <div class="navbar-header">

                        <ul class="nav navbar-nav visible-xs-block">
                            <li><a data-toggle="collapse" data-target="#demo1"><i class="icon-tree5"></i></a></li>
                        </ul>
                    </div>

                    <div class="navbar-collapse collapse" id="demo1">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#tab-demo1" data-toggle="tab" aria-expanded="false"
                                    onclick="changeTabUser('home')">
                                    <i class="icon-home5 position-left"></i>
                                    Home
                                </a>
                            </li>

                            <li>
                                <a href="#tab-demo2" data-toggle="tab" aria-expanded="false"
                                    onclick="changeTabUser('support')">
                                    <i class="icon-phone-wave position-left"></i>
                                    Support
                                </a>
                            </li>

                            <li>
                                <a href="#tab-demo3" data-toggle="tab" aria-expanded="false"
                                    onclick="changeTabUser('request')">
                                    <i class="icon-cart5 position-left"></i>
                                    Request
                                </a>
                            </li>

                            <li>
                                <a href="#tab-demo4" data-toggle="tab" aria-expanded="false"
                                    onclick="changeTabUser('history')">
                                    <i class="icon-stack4 position-left"></i>
                                    History
                                </a>
                            </li>

                            <li>
                                <a href="#tab-demo5" data-toggle="tab" aria-expanded="false"
                                    onclick="changeTabUser('project')">
                                    <i class="icon-stackoverflow position-left"></i>
                                    Project
                                </a>
                            </li>

                            {{-- <li>
								<a href="#tab-demo4" data-toggle="tab" aria-expanded="false" onclick="changeTabUser('faq')">
									<i class="icon-question3 position-left"></i>
									FAQ
								</a>
							</li> --}}


                        </ul>
                    </div>
                </div>
                {!! Form::hidden('active_tab_user', null, ['id' => 'active_tab_user']) !!}
                <div class="tab-content">
                    <div class="tab-pane fade active in has-padding" id="tab-demo1">
                        @include('ticket._home')
                    </div>
                    <div class="tab-pane fade has-padding" id="tab-demo2">
                        @include('ticket._support')
                    </div>
                    <div class="tab-pane fade has-padding" id="tab-demo3">
                        @include('ticket._request')
                    </div>
                    <div class="tab-pane fade has-padding" id="tab-demo4">
                        @include('ticket._history')
                    </div>
                    <div class="tab-pane fade has-padding" id="tab-demo5">
                        @include('ticket._project')
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-modal')
    @include('ticket._close_modal')
    @include('ticket._cancel_modal')
    @include('ticket._detail_modal')
@endsection

@section('page-js')
    <script>
        $('#data_2').hide();
        $('#data_3').hide();
        $('#tambahIssue').on('click', function() {
            $('#data_2').show();
            $('#tambahIssue').hide();

        });
        $('#tambahIssue2').on('click', function() {
            $('#data_3').show();
            $('#tambahIssue2').hide();

        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#s_select_department').on('change', function() {
                var data = $('#s_select_department').val();
                console.log('data' + data);
                // load_list_issue();
                if (data){
                    $.ajax({
                        type: 'GET',
                        url: '/ticket/support-issue/'+data
                    })
                    .done(function(response){
                        console.log(response);
                        var issue = response.support_issue;
                        console.log(issue);
                        console.log('panjang data'+issue.length);

                        $("#s_select_issue").empty();
                        $("#s_select_issue").append('<option value="">-- Pilih Category Issue --</option>');
                        $("#s_select_issue_2").empty();
                        $("#s_select_issue_2").append('<option value="">-- Pilih Category Issue 2 --</option>');
                        $("#s_select_issue_3").empty();
                        $("#s_select_issue_3").append('<option value="">-- Pilih Category Issue 3 --</option>');

                        for (var i = 0; i < issue.length; i++) {

                            $("#s_select_issue").append('<option value="' + issue[i].id + '">' + issue[i].name +
                                '</option>');
                            $("#s_select_issue_2").append('<option value="' + issue[i].id + '">' + issue[i].name +
                                '</option>');
                            $("#s_select_issue_3").append('<option value="' + issue[i].id + '">' + issue[i].name +
                                '</option>');
                        };
                    })
                }
                // APPEND ISSU NE DISINI
            })

            $('#s_select_issue').on('change', function(event, arg1) {

                var value = $(this).val();


                if (value) {
                    $.ajax({
                            type: 'GET',
                            url: '/ticket/list-detail-issue?id=' + value
                        })
                        .done(function(response) {
                            var issueDetails = response.issueDetails;


                            // console.log(issueDetails);
                            $("#s_select_detail_issue").empty();
                            $("#s_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');

                            $.each(issueDetails, function(id, name) {

                                $("#s_select_detail_issue").append('<option value="' + id +
                                    '">' + name + '</option>');
                            });
                        })
                } else {

                    $("#s_select_detail_issue").empty();
                    $("#s_select_detail_issue").append(
                    '<option value="">-- Pilih Detail Issue --</option>');
                }
            });
            $('#s_select_issue_2').on('change', function(event, arg1) {

                var value = $(this).val();


                if (value) {
                    $.ajax({
                            type: 'GET',
                            url: '/ticket/list-detail-issue?id=' + value
                        })
                        .done(function(response) {
                            var issueDetails = response.issueDetails;


                            // console.log(issueDetails);
                            $("#s_select_detail_issue_2").empty();
                            $("#s_select_detail_issue_2").append(
                                '<option value="">-- Pilih Detail Issue --</option>');

                            $.each(issueDetails, function(id, name) {

                                $("#s_select_detail_issue_2").append('<option value="' + id +
                                    '">' + name + '</option>');
                            });
                        })
                } else {

                    $("#s_select_detail_issue_2").empty();
                    $("#s_select_detail_issue_2").append(
                        '<option value="">-- Pilih Detail Issue --</option>');
                }
            });
            $('#s_select_issue_3').on('change', function(event, arg1) {

                var value = $(this).val();


                if (value) {
                    $.ajax({
                            type: 'GET',
                            url: '/ticket/list-detail-issue?id=' + value
                        })
                        .done(function(response) {
                            var issueDetails = response.issueDetails;


                            // console.log(issueDetails);
                            $("#s_select_detail_issue_3").empty();
                            $("#s_select_detail_issue_3").append(
                                '<option value="">-- Pilih Detail Issue --</option>');

                            $.each(issueDetails, function(id, name) {

                                $("#s_select_detail_issue_3").append('<option value="' + id +
                                    '">' + name + '</option>');
                            });
                        })
                } else {

                    $("#s_select_detail_issue_3").empty();
                    $("#s_select_detail_issue_3").append(
                        '<option value="">-- Pilih Detail Issue --</option>');
                }
            });

            // req_load_list_issue();

            $('#r_select_issue').on('change', function(event, arg1) {

                var value = $(this).val();


                if (value) {
                    $.ajax({
                            type: 'GET',
                            url: '/ticket/list-detail-issue?id=' + value
                        })
                        .done(function(response) {
                            var issueDetails = response.issueDetails;


                            // console.log(issueDetails);
                            $("#r_select_detail_issue").empty();
                            $("#r_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');

                            $.each(issueDetails, function(id, name) {

                                $("#r_select_detail_issue").append('<option value="' + id +
                                    '">' + name + '</option>');
                            });
                        })
                } else {

                    $("#r_select_detail_issue").empty();
                    $("#r_select_detail_issue").append(
                    '<option value="">-- Pilih Detail Issue --</option>');
                }
            });

            // pro_load_list_issue();

            $('#p_select_issue').on('change', function(event, arg1) {

                var value = $(this).val();


                if (value) {
                    $.ajax({
                            type: 'GET',
                            url: '/ticket/list-detail-issue?id=' + value
                        })
                        .done(function(response) {
                            var issueDetails = response.issueDetails;


                            // console.log(issueDetails);
                            $("#p_select_detail_issue").empty();
                            $("#p_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');

                            $.each(issueDetails, function(id, name) {

                                $("#p_select_detail_issue").append('<option value="' + id +
                                    '">' + name + '</option>');
                            });
                        })
                } else {

                    $("#r_select_detail_issue").empty();
                    $("#r_select_detail_issue").append(
                    '<option value="">-- Pilih Detail Issue --</option>');
                }
            });

        });

        function load_department() {
            $.ajax({
                    type: "GET",
                    url: '/ticket/department'
                })
                .done(function(response) {
                    console.log(response);
                    var department = response.department;
                    $("#s_select_department").empty();
                    $("#s_select_department").append('<option value="">-- Pilih Department (ajax) --</option>');
                    for (var i = 0; i < department.length; i++) {
                        $("#s_select_department").append('<option value="' + department[i].id + '">' + department[i]
                            .dept_name + '</option>');
                    }
                })
        }


        function load_list_issue(status) {
            console.log(status);
            $.ajax({
                    type: 'GET',
                    url: '/ticket/list-issue/' + status,
                })
                .done(function(response) {
                    console.log(response);
                    var issue = response;
                    console.log('ini issue', issue);

                    if (status == "support") {
                        // $("#s_select_issue").empty();
                        // $("#s_select_issue").append('<option value="">-- Pilih Category Issue --</option>');
                        // $("#s_select_issue_2").empty();
                        // $("#s_select_issue_2").append('<option value="">-- Pilih Category Issue 2 --</option>');
                        // $("#s_select_issue_3").empty();
                        // $("#s_select_issue_3").append('<option value="">-- Pilih Category Issue 3 --</option>');

                        // for (var i = 0; i < issue.length; i++) {

                        //     $("#s_select_issue").append('<option value="' + issue[i].id + '">' + issue[i].name +
                        //         '</option>');
                        //     $("#s_select_issue_2").append('<option value="' + issue[i].id + '">' + issue[i].name +
                        //         '</option>');
                        //     $("#s_select_issue_3").append('<option value="' + issue[i].id + '">' + issue[i].name +
                        //         '</option>');
                        // };
                    } else if (status == "request") {
                        $("#r_select_issue").empty();
                        $("#r_select_issue").append('<option value="">-- Pilih Category Issue --</option>');

                        for (var i = 0; i < issue.length; i++) {

                            $("#r_select_issue").append('<option value="' + issue[i].id + '">' + issue[i].name +
                                '</option>');
                        };
                    } else if (status == "project") {
                        $("#p_select_issue").empty();
                        $("#p_select_issue").append('<option value="">-- Pilih Category Issue --</option>');

                        for (var i = 0; i < issue.length; i++) {

                            $("#p_select_issue").append('<option value="' + issue[i].id + '">' + issue[i].name +
                                '</option>');
                        };
                    }
                })
        }

        // function req_load_list_issue(status) {

        // 	$.ajax({
        // 			type: 'GET',
        // 			url: '/ticket/list-issue/'+status
        // 		})
        // 		.done(function(response) {
        // 			var issue = response.issue;

        // 			$("#r_select_issue").empty();
        // 			$("#r_select_issue").append('<option value="">-- Pilih Category Issue --</option>');

        // 			$.each(issue, function(id, name) {

        // 				$("#r_select_issue").append('<option value="' + id + '">' + name + '</option>');
        // 			});
        // 		})

        // }

        // function pro_load_list_issue(status) {
        //     $.ajax({
        // 			type: 'GET',
        // 			url: '/ticket/list-issue/'+status
        // 		})
        // 		.done(function(response) {
        // 			console.log(response);
        //             var issue = response.issue;


        // 			$("#p_select_issue").empty();
        // 			$("#p_select_issue").append('<option value="">-- Pilih Category Issue --</option>');

        // 			$.each(issue, function(id, name) {

        // 				$("#p_select_issue").append('<option value="' + id + '">' + name + '</option>');
        // 			});
        // 		})
        //         .error(function (response) {
        //             console.log('Error:', response);
        //         })

        // }

        $('#s_upload').bind('change', function() {

            //this.files[0].size gets the size of your file.
            console.log(this.files[0].size);

            if (this.files[0].size > 5120000) {
                $("#alert_warning").trigger("click", 'File yang diupload melebih 5MB');
                $('#s_upload').val('');
                return false
            }

        });

        $('#insert_support').submit(function(event) {
            event.preventDefault();
            var email_anda = $('#s_email_anda').val();
            var select_issue = $('#s_select_issue').val();
            var select_detail_issue = $('#s_select_detail_issue').val();
            var title = $('#s_title').val();
            var description = $('#s_description').val();

            if (!email_anda) {
                $("#alert_warning").trigger("click", 'Email field is required\nKolom Email wajib diisi');
                return false
            }
            if (!select_issue) {
                $("#alert_warning").trigger("click",
                'Please choose Issue Category\nKategori masalah wajib dipilih');
                return false
            }
            if (!select_detail_issue) {
                $("#alert_warning").trigger("click", 'Please choose Detail Issue\nDetail kategori wajib dipilih');
                return false
            }
            if (!title) {
                $("#alert_warning").trigger("click", 'Title field is required\nJudul wajib diisi');
                return false
            }
            if (!description) {
                $("#alert_warning").trigger("click", 'Description field is required\nDeskripsi wajib diisi');
                return false
            }

            console.log($('#insert_support').serialize());
            // $('#insertIssueModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan mengajukan ticket support ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#insert_support')[0]);

                    $.ajax({
                        type: "POST",
                        url: $('#insert_support').attr('action'),
                        // data: $('#insert_support').serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            console.log(response);
                            var code = response.code;
                            // $('#insertPermissionModal').modal('hide');
                            $('#insert_support').trigger("reset");
                            // $('#select_issue').val('');
                            load_list_issue();
                            $("#s_select_detail_issue").empty();
                            $("#s_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');
                            // $('#issueCategoryTable').DataTable().ajax.reload();
                            $("#alert_success_2").trigger("click",
                                'Ticket berhasil di simpan dengan\n No. ' + code);
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            // $('#insertPermissionModal').modal();
                        }
                    });
                }
            });
        });
        $('#insert_project').submit(function(event) {
            event.preventDefault();
            var email_anda = $('#p_email_anda').val();
            var select_issue = $('#p_select_issue').val();
            var select_detail_issue = $('#p_select_detail_issue').val();
            var title = $('#p_title').val();
            var description = $('#p_description').val();

            if (!email_anda) {
                $("#alert_warning").trigger("click", 'Email field is required\nKolom Email wajib diisi');
                return false
            }
            if (!select_issue) {
                $("#alert_warning").trigger("click",
                'Please choose Issue Category\nKategori masalah wajib dipilih');
                return false
            }
            if (!select_detail_issue) {
                $("#alert_warning").trigger("click", 'Please choose Detail Issue\nDetail kategori wajib dipilih');
                return false
            }
            if (!title) {
                $("#alert_warning").trigger("click", 'Title field is required\nJudul wajib diisi');
                return false
            }
            if (!description) {
                $("#alert_warning").trigger("click", 'Description field is required\nDeskripsi wajib diisi');
                return false
            }


            // $('#insertIssueModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan mengajukan ticket project ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#insert_project')[0]);
                    $.ajax({
                        type: "POST",
                        url: $('#insert_project').attr('action'),
                        // data: $('#insert_support').serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            console.log(response);
                            var code = response.code;
                            // $('#insertPermissionModal').modal('hide');
                            $('#insert_support').trigger("reset");
                            // $('#select_issue').val('');
                            load_list_issue();
                            $("#s_select_detail_issue").empty();
                            $("#s_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');
                            // $('#issueCategoryTable').DataTable().ajax.reload();
                            $("#alert_success_2").trigger("click",
                                'Ticket berhasil di simpan dengan\n No. ' + code);
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            // $('#insertPermissionModal').modal();
                        }
                    });
                }
            });
        });

        $('#r_upload').bind('change', function() {

            //this.files[0].size gets the size of your file.
            console.log(this.files[0].size);

            if (this.files[0].size > 5120000) {
                $("#alert_warning").trigger("click", 'File yang diupload melebih 5MB');
                $('#r_upload').val('');
                return false
            }

        });

        $('#insert_request').submit(function(event) {
            event.preventDefault();
            var email_anda = $('#r_email_anda').val();
            var email_atasan = $('#r_email_atasan').val();
            var select_issue = $('#r_select_issue').val();
            var select_detail_issue = $('#r_select_detail_issue').val();
            var title = $('#r_title').val();
            var description = $('#r_description').val();

            if (!email_anda) {
                $("#alert_warning").trigger("click", 'Email field is required\nKolom Email wajib diisi');
                return false
            }
            if (!email_atasan) {
                $("#alert_warning").trigger("click",
                    "Dept Head Email field is required\nKolom Email Atasan wajib diisi");
                return false
            }
            if (!select_issue) {
                $("#alert_warning").trigger("click",
                'Please choose Issue Category\nKategori masalah wajib dipilih');
                return false
            }
            if (!select_detail_issue) {
                $("#alert_warning").trigger("click", 'Please choose Detail Issue\nDetail kategori wajib dipilih');
                return false
            }
            if (!title) {
                $("#alert_warning").trigger("click", 'Title field is required\nJudul wajib diisi');
                return false
            }
            if (!description) {
                $("#alert_warning").trigger("click", 'Description field is required\nDeskripsi wajib diisi');
                return false
            }


            $('#insertIssueModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan mengajukan ticket support ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#insert_request')[0]);
                    $.ajax({
                        type: "POST",
                        url: $('#insert_request').attr('action'),
                        data: formData,
                        processData: false,
                        contentType: false,
                        // data: $('#insert_request').serialize(),
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            console.log(response);
                            var code = response.code;
                            // $('#insertPermissionModal').modal('hide');
                            $('#insert_request').trigger("reset");
                            // $('#select_issue').val('');
                            load_list_issue();
                            $("#r_select_detail_issue").empty();
                            $("#r_select_detail_issue").append(
                                '<option value="">-- Pilih Detail Issue --</option>');
                            // $('#issueCategoryTable').DataTable().ajax.reload();
                            $("#alert_success_2").trigger("click",
                                'Ticket berhasil di simpan dengan\n No. ' + code);
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            // $('#insertPermissionModal').modal();
                        }
                    });
                }
            });
        });


        function changeTabUser(status) {
            $('#active_tab_user').val(status).trigger('change');
            load_list_issue(status);
            load_department();
        }


        $('#active_tab_user').on('change', function() {
            var active_tab = $('#active_tab_user').val();
            if (active_tab == 'history') {
                historyTable()
                // var nik_pegawai = $('#s_nik').val();
                // var historyTable = $('#historyTable').DataTable({
                // 	dom: 'Bfrtip',
                // 	processing: true,
                // 	serverSide: true,
                // 	pageLength:100,
                // 	deferRender:true,
                // 	ajax: {
                // 		type: 'GET',
                // 		url: '/ticket/history',
                // 		data: function(d){
                // 			return $.extend({},d,{
                // 				'nik_pegawai' : nik_pegawai,
                // 			});
                // 		},
                // 	},
                // 	fnCreatedRow: function(row, data, index) {
                // 		var info = historyTable.page.info();
                // 		var value = index + 1 + info.start;
                // 		$('td', row).eq(0).html(value);
                // 	},
                // 	columns: [
                // 		{data: null, sortable: false, orderable: false,searchable: false},
                // 		{data: 'uuid', name: 'id',searchable:true,visible:false,orderable:false},
                // 		{data: 'ticket_category', name: 'ticket_category',searchable:true,orderable:true},
                // 		{data: 'code', name: 'code',searchable:true,orderable:true},
                // 		{data: 'name', name: 'name',searchable:true,orderable:true},
                // 		{data: 'department', name: 'department',searchable:true,orderable:true},
                // 		{data: 'title', name: 'title',searchable:true,orderable:true},
                // 		{data: 'created_at', name: 'created_at',searchable:true,orderable:true},
                // 		{data: 'last_status', name: 'last_status',searchable:true,orderable:true},
                // 		{data: 'action', name: 'action',searchable:true,orderable:true},
                // 	]
                // });

                // var dtable = $('#historyTable').dataTable().api();
                // $(".historyTable_filter input")
                // 	.unbind() // Unbind previous default bindings
                // 	.bind("keyup", function (e) { // Bind our desired behavior
                // 		// If the user pressed ENTER, search
                // 		if (e.keyCode == 13) {
                // 			// Call the API search function
                // 			dtable.search(this.value).draw();
                // 		}
                // 		// Ensure we clear the search if they backspace far enough
                // 		if (this.value == "") {
                // 			dtable.search("").draw();
                // 		}
                // 		return;
                // });
                // dtable.draw();


            } else if (active_tab == 'comment') {
                showComment();

                // var dtableComment = $('#comment_table').dataTable().api();
                // $("#comment_table.dataTables_filter input")
                // 	.unbind() // Unbind previous default bindings
                // 	.bind("keyup", function (e) { // Bind our desired behavior
                // 		// If the user pressed ENTER, search
                // 		if (e.keyCode == 13) {
                // 			// Call the API search function
                // 			dtableComment.search(this.value).draw();
                // 		}
                // 		// Ensure we clear the search if they backspace far enough
                // 		if (this.value == "") {
                // 			dtableComment.search("").draw();
                // 		}
                // 		return;
                // });
                // dtableComment.draw();

            }
        });

        function detail(id) {
            // var uuid = "'"+id+"'";
            $('#ticket_id').val(id);
            $('#_ticket_id').val(id);
            $('#detailModal').modal();
            progressTable();
        }

        function changeDetailUser(status) {
            $('#active_detail_user').val(status).trigger('change');
        }


        $('#active_detail_user').on('change', function() {
            var active_tab = $('#active_detail_user').val();
            if (active_tab == 'progress') {
                progressTable()

            } else if (active_tab == 'comment') {
                commentTable()
            }
        });

        function historyTable() {

            $('#historyTable').DataTable().destroy();
            $('#historyTable tbody').empty();
            var nik_pegawai = $('#s_nik').val();
            var historyTable = $('#historyTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength: 100,
                deferRender: true,
                ajax: {
                    type: 'GET',
                    url: '/ticket/history',
                    data: function(d) {
                        return $.extend({}, d, {
                            'nik_pegawai': nik_pegawai,
                        });
                    },
                },
                fnCreatedRow: function(row, data, index) {
                    var info = historyTable.page.info();
                    var value = index + 1 + info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [{
                        data: null,
                        sortable: false,
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'uuid',
                        name: 'id',
                        searchable: true,
                        visible: false,
                        orderable: false
                    },
                    {
                        data: 'ticket_category',
                        name: 'ticket_category',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'code',
                        name: 'code',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'name',
                        name: 'name',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'department',
                        name: 'department',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'title',
                        name: 'title',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'last_status',
                        name: 'last_status',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: true,
                        orderable: true
                    },
                ]
            });

            var dtable = $('#historyTable').dataTable().api();
            $(".historyTable_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function(e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });
            dtable.draw();
        }

        function progressTable() {
            $('#progressTable').DataTable().destroy();
            $('#progressTable tbody').empty();
            var ticket_id = $('#ticket_id').val();
            var historyTable = $('#progressTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength: 100,
                deferRender: true,
                ajax: {
                    type: 'GET',
                    url: '/ticket/progress',
                    data: function(d) {
                        return $.extend({}, d, {
                            'ticket_id': ticket_id,
                        });
                    },
                },
                fnCreatedRow: function(row, data, index) {
                    var info = historyTable.page.info();
                    var value = index + 1 + info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [{
                        data: null,
                        sortable: false,
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'id',
                        name: 'id',
                        searchable: true,
                        visible: false,
                        orderable: false
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'user_id',
                        name: 'user_id',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'remark',
                        name: 'remark',
                        searchable: true,
                        orderable: true
                    },
                ]
            });

            var dtable = $('#progressTable').dataTable().api();
            $(".progressTable input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function(e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });
            dtable.draw();
        }

        function commentTable() {
            $('#commentTable').DataTable().destroy();
            $('#commentTable tbody').empty();
            var ticket_id = $('#ticket_id').val();
            var historyTable = $('#commentTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength: 100,
                deferRender: true,
                ajax: {
                    type: 'GET',
                    url: '/ticket/comment',
                    data: function(d) {
                        return $.extend({}, d, {
                            'ticket_id': ticket_id,
                        });
                    },
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        searchable: true,
                        visible: false,
                        orderable: false
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'user_id',
                        name: 'user_id',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'comment',
                        name: 'comment',
                        searchable: true,
                        orderable: true
                    },
                    {
                        data: 'action',
                        name: 'comment',
                        searchable: true,
                        orderable: true
                    },
                ]
            });

            var dtable = $('#commentTable').dataTable().api();
            $(".commentTable input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function(e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });
            dtable.draw();
        }

        $('#user_insert_comment').submit(function(event) {
            event.preventDefault();
            var comment = $('#comment').val();
            var user1 = $('#_ticket_id').val();
            var user2 = $('#_user_id').val();

            if (!comment) {
                $("#alert_warning").trigger("click", 'Masukkan komentar terlebih dahulu');
                return false
            }

            $('#detailModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#user_insert_comment')[0]);
                    $.ajax({
                        type: "POST",
                        url: $('#user_insert_comment').attr('action'),
                        // data: $('#user_insert_comment').serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            $('#detailModal').modal('hide');
                            $('#user_insert_comment').trigger("reset");
                            $('#commentTable').DataTable().ajax.reload();
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                            $('#detailModal').modal();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            $('#detailModal').modal();
                        }
                    });
                }
            });
        });

        // function close_ticket(id)
        // {
        // 	// var uuid = "'"+id+"'";
        // 	// $('#ticket_id').val(id);
        // 	$('#close_ticket_id').val(id);
        // 	$('#closeTicketModal').modal();
        // 	// progressTable();
        // }
        function close_ticket(url) {
            var x = url;
            console.log(x);
            $.ajax({
                type: "get",
                url: url,
                beforeSend: function() {
                    // update_mappings = [];
                    // $('#historyTable').DataTable().destroy();
                    // $('#historyTable tbody').empty();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    console.log("b");
                },
                success: function(response) {
                    console.log("a");

                    $.unblockUI();
                    $('#user_close_ticket').attr('action', response.url_update);
                    $('#close_ticket_id').val(response.id);
                    $('#close_user_id').val(response.user_id);
                    console.log($('#user_close_ticket').val());
                    console.log($('#close_ticket_id').val());
                    console.log($('#close_user_id').val());
                    // $('#update_name').val(response.name);
                    // $('#update_email').val(response.email);
                    // $('#historyTable').DataTable().ajax.reload();
                    $('#closeTicketModal').modal();
                    // historyTable()
                    // $('#detailModal').modal('hide');
                    // $('#user_insert_comment').trigger("reset");
                    // $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                    // $('#closeTicketModal').modal('show');
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);
                    // $('#detailModal').modal();
                }
            })

            $('#closeTicketModal').modal();
        }

        $('#user_close_ticket').submit(function(event) {
            event.preventDefault();
            var c_keterangan = $('#close_keterangan').val();

            console.log(c_keterangan);
            console.log($('#user_close_ticket').attr('action'));
            if (!c_keterangan) {
                $("#alert_warning").trigger("click", 'Keterangan harus diisi terlebih dahulu');
                return false
            }

            $('#closeTicketModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#user_close_ticket')[0]);
                    console.log(formData);
                    $.ajax({
                        type: "POST",
                        url: $('#user_close_ticket').attr('action'),
                        // data: $('#user_insert_comment').serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            $('#closeTicketModal').modal('hide');
                            $('#user_close_ticket').trigger("reset");
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                            $('#historyTable').DataTable().ajax.reload();
                            // $('#closeTicketModal').modal();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            if (response.status == 522) $("#alert_warning").trigger("click",
                                "Ticket sudah closed");
                            $('#historyTable').DataTable().ajax.reload();
                            // $('#closeTicketModal').modal();
                        }
                    });
                }
            });
        });

        function cancel_ticket(url) {
            var x = url;
            console.log(x);
            $.ajax({
                type: "get",
                url: url,
                beforeSend: function() {
                    // update_mappings = [];
                    // $('#historyTable').DataTable().destroy();
                    // $('#historyTable tbody').empty();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    // console.log("b");
                },
                success: function(response) {
                    // console.log("a");

                    $.unblockUI();
                    $('#user_cancel_ticket').attr('action', response.url_update);
                    $('#cancel_ticket_id').val(response.id);
                    $('#cancel_user_id').val(response.user_id);
                    // $('#update_name').val(response.name);
                    // $('#update_email').val(response.email);
                    // $('#historyTable').DataTable().ajax.reload();
                    $('#cancelTicketModal').modal();
                    // historyTable()
                    // $('#detailModal').modal('hide');
                    // $('#user_insert_comment').trigger("reset");
                    // $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                    // $('#closeTicketModal').modal('show');
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);
                    // $('#detailModal').modal();
                }
            })

            $('#cancelTicketModal').modal();
        }

        $('#user_cancel_ticket').submit(function(event) {
            event.preventDefault();
            var c_keterangan = $('#cancel_keterangan').val();

            console.log(c_keterangan);
            console.log($('#user_cancel_ticket').attr('action'));
            if (!c_keterangan) {
                $("#alert_warning").trigger("click", 'Keterangan harus diisi terlebih dahulu');
                return false
            }

            $('#cancelTicketModal').modal('hide');
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
                if (result) {
                    var formData = new FormData($('#user_cancel_ticket')[0]);
                    // console.log(formData);
                    $.ajax({
                        type: "POST",
                        url: $('#user_cancel_ticket').attr('action'),
                        // data: $('#user_insert_comment').serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            $('#cancelTicketModal').modal('hide');
                            $('#user_cancel_ticket').trigger("reset");
                            $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                            $('#historyTable').DataTable().ajax.reload();
                            // $('#closeTicketModal').modal();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",
                                response.responseJSON);
                            if (response.status == 522) $("#alert_warning").trigger("click",
                                "Ticket sudah cancel");
                            $('#historyTable').DataTable().ajax.reload();
                            // $('#closeTicketModal').modal();
                        }
                    });
                }
            });
        });

        function hapus(url) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                    type: "delete",
                    url: url,
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function() {
                        $.unblockUI();
                    },
                    error: function() {
                        $.unblockUI();
                    }
                })
                .done(function() {
                    $('#userTable').DataTable().ajax.reload();
                    $("#alert_success").trigger("click", 'Data Berhasil hapus');
                });
        }
    </script>

@endsection
