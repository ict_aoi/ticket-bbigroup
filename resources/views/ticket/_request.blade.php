{!!
	Form::open([
		'role'   => 'form',
		'url'    => route('ticket.store_request'),
		'method' => 'post',
		'class'  => 'form-horizontal',
		'id'     => 'insert_request'
	])
!!}

{{-- <div> --}}
<div class="col-md-6">
	@include('form.text', [
		'field'      => 'r_nik',
		'label'      => 'NIK',
		'default'	 => $data_user->nik,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_nik',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'r_name',
		'label'      => 'Name',
		'default'	 => $data_user->name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_name',
			'readonly' => '',
			// 'required' => ''
		]
	])

</div>

<div class="col-md-6">
	@include('form.text', [
		'field'      => 'r_department',
		'label'      => 'Department',
		'default'	 => $data_user->department_name,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_department',
			'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'r_factory',
		'label'      => 'Factory',
		'default'	 => $data_user->factory,
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_factory',
			'readonly' => '',
			// 'required' => ''
		]
	])
</div>
<div class="col-md-12">
	@include('form.text', [
		'field'      => 'r_email_anda',
		'label'      => 'Email Anda',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_email_anda',
			// 'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.text', [
		'field'      => 'r_email_atasan',
		'label'      => "Email Atasan Anda",
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_email_atasan',
			// 'readonly' => '',
			// 'required' => ''
		]
	])

	@include('form.select', [
		'field'     => 'r_select_issue',
		'label'     => 'Issue Category',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Category Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 'r_select_issue'
		]
	])
	@include('form.select', [
		'field'     => 'r_select_detail_issue',
		'label'     => 'Detail Issue',
		// 'mandatory' => '*Wajib diisi',
		'options'   => [
			'' => '-- Pilih Detail Issue--',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id'       => 'r_select_detail_issue',
			// 'required' => ''

		]
	])
</div>

<div class="col-md-12">
	@include('form.text', [
		'field'      => 'r_title',
		'label'      => 'Title',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_title',
			// 'readonly' => '',
			// 'required' => ''
		]
	])
	@include('form.textarea', [
		'field'      => 'r_description',
		'label'      => 'Description',
		// 'mandatory'  => '*Wajib diisi',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_description',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
</div>

<div class="col-md-12">

	@include('form.file', [
		'field'      => 'r_upload',
		'label'      => 'Attachment',
		'mandatory'  => '*Max 5MB',
		'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
		'attributes' => [
			'id'       => 'r_upload',
			'rows'     => 5,
			'style'    => 'resize: none;',
			// 'required' => ''
		]
	])
</div>

<div class="label-block text-center">
	{{-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> --}}
	<button type="submit" class="btn btn-primary">Submit form</button>
</div>
{!! Form::close() !!}
