<div id="insertSubDepartmentModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
				{!!
					Form::open([
						'role' => 'form',
						'url' => route('subdepartment.store'),
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'insert_sub_department'
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
                                            'field' => 'dept',
                                            'label' => 'Pilih Department',
                                            'options' => [
                                                '' => '-- Pilih Department--',
                                                ]+$department,
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'department'
                                            ]
                                        ])
                    @include('form.text', [
						'field' => 'sub_department',
						'label' => 'Sub Department',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'sub_department'
						]
					])

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
