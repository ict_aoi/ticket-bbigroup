@extends('layouts.app',['active' => 'sub-department'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Sub Department</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Sub Department</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertSubDepartmentModal"><i class="icon-plus2 position-left"></i>Create New</a>
        </div>
	</div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="subDepartmentTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Sub Department Name</th>
                        <th>Department Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-modal')
	@include('master-data-sub-department._insert_modal')
	@include('master-data-sub-department._update_modal')
@endsection
@section('page-js')
    <script>
        $(document).ready( function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#subDepartmentTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength:100,
                deferRender:true,
                ajax: {
                    type: 'GET',
                    url: '/sub-department/data',
                },
                columns: [
                    {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                    {data: 'sub_dept_name', name: 'dept_name',searchable:true,orderable:true},
                    {data: 'dept_name', name: 'dept_name',searchable:true,orderable:true},
                    {data: 'action', name: 'action',searchable:true,orderable:true},
                ]
            });
            $('#insert_sub_department').submit(function (event){
                event.preventDefault();
                console.log($('#insert_sub_department').serialize());
                $('#insertDepartmentModal').modal('hide');
                bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                    if(result){
                        $.ajax({
                            type: "POST",
                            url: $('#insert_sub_department').attr('action'),
                            data: $('#insert_sub_department').serialize(),
                            beforeSend: function () {
                                $.blockUI({
                                    message: '<i class="icon-spinner4 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (response) {
                                console.log(response);
                                $('#insertSubDepartmentModal').modal('hide');
                                $('#insert_sub_department').trigger("reset");
                                $('#subDepartmentTable').DataTable().ajax.reload();
                                $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                            },
                            error: function ( xhr, status, error) {
                                $.unblockUI();
                                // console.log(response);
                                var err = eval("(" + xhr.responseText + ")");
                                // $('#priorityError').text(response.responseJSON.errors.title);
                                $('#insertSubDepartmentModal').modal();
                                console.log(err);
                                // if (response.status == 422){
                                    //  $("#alert_warning").trigger("click",response.responseJSON);
                                    //  $('#duration').val(response.responseJSON.responseText);
                                    // $("#alert_error").trigger("click", 'Data Gagal disimpan');
                                    // $("#alert_error").trigger("click",err.errors.priority);
                                // }
                            }
                        });
                    }
                });
            });
            $('#update_sub_department').submit(function (event){
            event.preventDefault();
            // var name = $('#update_name_priority').val();

            // if(!name){
            //     $("#alert_warning").trigger("click", 'Nama wajib diisi');
            //     return false
            // }

            $('#updateSubDepartmentModal').modal('hide');
                console.log($('#update_sub_department').serialize());
                bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                    if(result){
                        $.ajax({
                            type: "POST",
                            url: $('#update_sub_department').attr('action'),
                            data: $('#update_sub_department').serialize(),
                            beforeSend: function () {
                                $.blockUI({
                                    message: '<i class="icon-spinner4 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (response) {
                                $('#updateSubDepartmentModal').modal('hide');
                                $('#update_sub_deparment').trigger("reset");
                                $('#subDepartmentTable').DataTable().ajax.reload();
                                $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                            },
                            error: function (response) {
                                $.unblockUI();
                                console.log(response.responseJSON.responseText);
                                if (response.status == 422) {
                                    // $("#alert_warning").trigger("click",response.responseJSON);

                                }
                                $('#updateSubDepartmentModal').modal();
                            }
                        });
                    }
                });
            });
        });
        function edit(url)
        {
            $.ajax({
                type: "get",
                url: url,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#ffff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();

                }
            })
            .done(function (response) {
                console.log(response);
                $('#update_sub_department').attr('action', response.url_update);
                $('#update_sub_department_name').val(response.sub_dept_name);
                $('#update_department').val(response.id_dept_name).trigger('change');
                $('#updateSubDepartmentModal').modal();

            });
        }
        function hapus(url)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "delete",
                url: url,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function (response) {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                }
            }).done(function ($result) {
                $('#subDepartmentTable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data Berhasil hapus');
            });
        }
    </script>
@endsection
