<div id="detailTicketModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg" style="width:75%">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Ticket<span id="histori_stock"></span></h5>
			</div>

			<div class="modal-body">
				<div class="tabbable tab-content-bordered" id="tab" class="hidden">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#detail-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('progres')">Detail</a></li>
						<li><a href="#comment-ticket" data-toggle="tab" aria-expanded="false" onclick="changeTabModal('comment')">Comment</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active" id="detail-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminSupport.storeProgres'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_progres_ticket'
                                ])
                            !!}
                            <div class="modal-body">

                                @include('form.select', [
                                            'field' => 'priority',
                                            'label' => 'Setup Priority',
                                            'options' => [
                                                '' => '-- Pilih Priority--',
                                            ]+$priority,
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_priority'
                                            ]
                                        ])
                                {{-- @include('form.select', [
                                            'field' => 'priority',
                                            'label' => 'Setup Priority',
                                            'options' => [
                                                '' => '-- Pilih Priority--',
                                                'low' =>'low',
                                                'medium' =>'medium',
                                                'high' =>'high',
                                             ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_priority'
                                            ]
                                        ]) --}}

                                @include('form.select', [
                                            'field' => 'status',
                                            'label' => 'Update Status',
                                            'options' => [
                                                '' => '-- Pilih Status--',
                                                'planning' => 'planning',
                                                'proses' => 'Proses',
                                                'closed' => 'solved',
                                                'reject' => 'Reject',
                                            ],
                                            'class' => 'select-search',
                                            'attributes' => [
                                                'id' => 'select_status'
                                            ]
                                        ])

                                @include('form.textarea', [
                                    'field' => 'remark',
                                    'label' => 'Remark',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'remark',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])
                                {!! Form::hidden('ticket_id',null, array('id' => 'ticket_id')) !!}
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        {!! Form::close() !!}
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="progres_table">
									<thead>
										<tr>
											<th>Status</th>
											<th>Keterangan</th>
											<th>Agent</th>
											<th>Created At</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="comment-ticket">
							<br/>
                            {!!
                                Form::open([
                                    'role' => 'form',
                                    'url' => route('adminSupport.storeComment'),
                                    'method' => 'post',
                                    'class' => 'form-horizontal',
                                    'id'=> 'update_comment_ticket'
                                ])
                            !!}

                            <div class="modal-body">
                                @include('form.textarea', [
                                    'field' => 'comment',
                                    'label' => 'Add Comment',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id' => 'comment',
                                        'rows' => 5,
                                        'style' => 'resize: none;'
                                    ]
                                ])

                                @include('form.file', [
                                    'field'      => 's_upload',
                                    'label'      => 'Attachment',
                                    'mandatory'  => '*Max 5MB',
                                    'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id'       => 's_upload',
                                        'rows'     => 5,
                                        'style'    => 'resize: none;',
                                        // 'required' => ''
                                    ]
                                ])
                                {!! Form::hidden('_ticket_id',null, array('id' => '_ticket_id')) !!}
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>

                        {!! Form::close() !!}

                        <div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="commentTable" style="width:100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Created At</th>
											<th>Name</th>
                                            <th>Comment</th>
                                            <th>Attachment</th>
										</tr>
									</thead>
								</table>
							</div>



                    </div>
			    <div class="modal-footer">
				{!! Form::hidden('active_modal_tab',null, array('id' => 'active_modal_tab')) !!}
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
