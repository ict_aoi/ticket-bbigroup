@extends('layouts.app_main',['active' => 'ticket'])

@section('page-css')
    <style>
        #wrapper {
            top: 300px;
            right: 50px;
            position: absolute;
        }
    </style>
@endsection

@section('page-content')
<!-- Search field -->
    {{-- <div id="wrapper"> --}}

        <br><br><br><br><br><br><br><br>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="text-center">
                    <img src="{{ asset('/images/logo-bbi-full.png')  }}" class="img-fluid" style="width:50%;height:100%;" alt="Responsive image">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default text-center" >
                    <div class="heading-elements">
                    </div>

                    <div class="panel-body" >
                            <div class="input-group container-fluid">

                                    <h5>Anda sudah melakukan {{$status}} terhadap request</h5>
                                    <a href="/login" class="btn btn-primary"> Klik untuk Login</a>

                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    {{-- </div> --}}
@endsection
