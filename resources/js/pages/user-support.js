$(document).ready( function () {
	$('#select_issue').on('change', function(event, arg1) {

		var value = $(this).val();


		if (value) {
			$.ajax({
					type: 'get',
					url: '/ticket/list-detail-issue?id=' + value
				})
				.done(function(response) {
					var issueDetails = response.issueDetails;


					$("#select_detail_issue").empty();
					$("#select_detail_issue").append('<option value="">-- Pilih Subcont --</option>');

					$.each(issueDetails, function(detail_issue_id, name) {

						$("#select_detail_issue").append('<option value="' + detail_issue_id + '">' + name + '</option>');
					});
				})
		} else {

			$("#select_detail_issue").empty();
			$("#select_detail_issue").append('<option value="">-- Pilih Subcont --</option>');
		}
	}); 

	$('#insert_support').submit(function (event){
		event.preventDefault();
		var issue = $('#select_issue').val();
		var detail_issue = $('#select_detail_issue').val();
		
		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}
		
		$('#insertIssueModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_issue').attr('action'),
					data: $('#insert_issue').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#insertPermissionModal').modal('hide');
						$('#insert_issue').trigger("reset");
						$('#issueCategoryTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#insertPermissionModal').modal();
					}
				});
			}
		});
	});
});