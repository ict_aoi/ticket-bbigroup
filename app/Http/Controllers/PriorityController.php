<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Priority;
use DB;
use StdClass;
use Validator;

use App\Models\IssueCategory;

use Yajra\Datatables\Datatables;

class PriorityController extends Controller
{
    public function index(){
        return view('master-data-priority.index');
    }

    public function data(){
        if(request()->ajax()){
            $data = DB::table('priority')->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('master-data-issue-category._action', [
                    'model'      => $data,
                    'edit_modal' => route('priority.edit',$data->id),
                    'delete'     => route('priority.destroy',$data->id),
                ]);

            })
            ->make(true);
        }
    }

    public function store(Request $request){
        // $data = request()->validate([
        //     'priority' => 'required|unique:priority,priority'
        // ]);
        $this->validate($request, [
            'priority' => 'required|unique:priority,priority'
        ]);

        // if(IssueCategory::where('name',str_slug($request->name))->exists())
        //     return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);
        // dd($request);
        $permission = Priority::firstorCreate([
            'priority'        => $request->priority,
            'duration_reso' => $request->duration_reso,
            'unit_reso'  => $request->unit_reso,
        ]);
        return response()->json('success', 200);
    }

    public function edit($id)

    {
        $priority               = Priority::find($id);
        $obj                    = new StdClass();
        $obj->id                = $id;
        $obj->priority          = $priority->priority;
        $obj->duration_reso     = $priority->duration_reso;
        $obj->unit_reso         = $priority->unit_reso;
        $obj->url_update        = route('priority.update',$priority->id);

		return response()->json($obj,200);
    }

    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required|min:3'
        // ]);

        // if(IssueCategory::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
        //     return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $update = Priority::find($id);
        // $update->priority = str_slug($request->priority);
        $update->priority = $request->priority;
        $update->duration_reso = $request->duration_reso;
        $update->unit_reso = $request->unit_reso;
        $update->save();

        return response()->json('success', 200);
    }
    public function destroy($id)
    {
        $priority = Priority::findorFail($id)->delete();
        return response()->json(200);
    }


}
