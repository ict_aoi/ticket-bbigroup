<?php

namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Yajra\Datatables\Datatables;

use App\Models\IssueCategory;
use App\Models\DetailIssueCategory;
use Illuminate\Http\Request;

class DetailIssueCategoryController extends Controller
{
    public function index()
    {
        $issue = IssueCategory::pluck('name', 'id')->all();
        return view('master-data-detail-issue-category.index',compact('issue'));
    }
    public function data()
    {
        if(request()->ajax())
        {
            $data = DetailIssueCategory::orderby('created_at','desc');
            return datatables()->of($data)
            ->editColumn('issue_category_id',function($data){
                return ($data->issue_category_id)? $data->IssueCategory->name : null;
            })
            ->addColumn('action', function($data) {
                return view('master-data-detail-issue-category._action', [
                    'model' => $data,
                    'edit_modal' => route('detailIssue.edit',$data->id),
                    'delete' => route('detailIssue.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'issue' => 'required|min:3',
        ]);

        if(DetailIssueCategory::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);

        $permission = DetailIssueCategory::firstorCreate([
            'issue_category_id' => $request->issue,
            'name' => $request->name,
            'description' => $request->description
        ]);
        return response()->json('success', 200);
    }

    public function edit($id)

    {
        $issue = DetailIssueCategory::find($id);
        $obj = new StdClass();
        $obj->id = $id;
        $obj->name = $issue->name;
        $obj->description = $issue->description;
		$obj->url_update = route('detailIssue.update',$issue->id);

		return response()->json($obj,200);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(DetailIssueCategory::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $detail_issue = DetailIssueCategory::find($id);
        $detail_issue->issue_category_id = $request->issue;
        $detail_issue->name = $request->name;
        $detail_issue->description = $request->description;
        $detail_issue->save();

        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $detail_issue = DetailIssueCategory::findorFail($id)->delete();
        return response()->json(200);
    }
}
