<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\IssueCategory;
use Yajra\Datatables\Datatables;
use Illuminate\Routing\UrlGenerator;

use App\Models\User;
use App\Models\Ticket;
use App\Models\DetailTicket;
use App\Models\DetailIssueCategory;
use Illuminate\Support\Facades\Storage;

class TicketController extends Controller
{
    public function index()
    {
        // $issue = IssueCategory::pluck('name', 'id')->all();
        return view('ticket.index');

    }


    // public function logon(Request $request)
    // {
    //     $nik_id = $request->id;

    //     $data_user = DB::table('adt_bbigroup_emp_new')
    //         ->select('nik','name','department_name','position','factory')
    //         ->where('nik',$nik_id)
    //         ->first();

    //     if($data_user != null){
    //         $url = route('ticket.home',compact('nik_id'));
    //         // return view('ticket.home',['user'=>$data_user]);
    //         // return response()->json($url,200);
    //         return response()->json($data_user,200);
    //     }
    //     else{
    //         return response()->json(['message'=>'NIK tidak diketemukan!'],422);

    //     }
    //     //if gak ada nik maka balik ke index
    //     // $issue = IssueCategory::pluck('name', 'id')->all();
    //     // return view('ticket.home',compact('issue'));
    // }

    public function home(Request $request)
    {
        // dd($request);
        $data_user = DB::connection('absen')->table('adt_bbigroup_emp_new')
            ->select('nik','name','department_name','position','factory')
            ->where('nik',$request->nik)
            ->first();
        // dd($data_user);
        if($data_user != null){
            $issue = IssueCategory::pluck('name', 'id')->all();

            $carbon = Carbon::now();
            $ticket_support_today = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','support',]
            ,[DB::raw('date(created_at)'),$carbon]])
            ->count();

            $ticket_support_closed = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','support',]])
            ->whereIn('last_status',['closed','rejected'])
            ->count();

            $ticket_support_open = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','support',]])
            ->whereNotIn('last_status',['closed','rejected'])
            ->count();

            $total_support                 = $ticket_support_closed + $ticket_support_open;
            if($total_support==0){
                $ticket_support_open_percent   = 0;
                $ticket_support_closed_percent = 0;
            }
            else{
                $ticket_support_open_percent   = ($ticket_support_open / $total_support) * 100;
                $ticket_support_closed_percent = ($ticket_support_closed / $total_support) * 100;
            }

            $ticket_request_today = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','request',]
            ,[DB::raw('date(created_at)'),$carbon]])
            ->count();

            $ticket_request_closed = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','request',]])
            ->whereIn('last_status',['closed','rejected'])
            ->count();

            $ticket_request_open = Ticket::where([['nik_pegawai',$request->nik],['ticket_category','request',]])
            ->whereNotIn('last_status',['closed','rejected'])
            ->count();

            $total_request                 = $ticket_request_closed + $ticket_request_open;
            if($total_request==0){
                $ticket_request_open_percent   = 0;
                $ticket_request_closed_percent = 0;
            }
            else{
                $ticket_request_open_percent   = ($ticket_request_open / $total_request) * 100;
                $ticket_request_closed_percent = ($ticket_request_closed / $total_request) * 100;
            }
            return view('ticket.home',compact(
                'issue',
                'data_user',
                'ticket_support_today',
                'ticket_support_closed',
                'ticket_support_open',
                'ticket_request_today',
                'ticket_request_closed',
                'ticket_request_open',
                'ticket_support_open_percent',
                'ticket_support_closed_percent',
                'ticket_request_open_percent',
                'ticket_request_closed_percent',
                'total_support',
                'total_request'
            ));
        }
        else{
            return redirect('/ticket')->with('pesan', 'NIK tidak ditemukan');
        }
    }
    // public function home($id)
    // {
    //     $data_user = DB::table('adt_bbigroup_emp_new')
    //         ->select('nik','name','department','position','factory')
    //         ->where('nik',$id)
    //         ->first();

    //     $issue = IssueCategory::pluck('name', 'id')->all();

    //     $carbon = Carbon::now();
    //     $ticket_support_today = Ticket::where([['nik_pegawai',$id],['ticket_category','support',]
    //     ,[DB::raw('date(created_at)'),$carbon]])
    //     ->count();

    //     $ticket_support_closed = Ticket::where([['nik_pegawai',$id],['ticket_category','support',]])
    //     ->whereIn('last_status',['closed','rejected'])
    //     ->count();

    //     $ticket_support_open = Ticket::where([['nik_pegawai',$id],['ticket_category','support',]])
    //     ->whereNotIn('last_status',['closed','rejected'])
    //     ->count();

    //     $total_support                 = $ticket_support_closed + $ticket_support_open;
    //     if($total_support==0){
    //         $ticket_support_open_percent   = 0;
    //         $ticket_support_closed_percent = 0;
    //     }
    //     else{
    //         $ticket_support_open_percent   = ($ticket_support_open / $total_support) * 100;
    //         $ticket_support_closed_percent = ($ticket_support_closed / $total_support) * 100;
    //     }

    //     $ticket_request_today = Ticket::where([['nik_pegawai',$id],['ticket_category','request',]
    //     ,[DB::raw('date(created_at)'),$carbon]])
    //     ->count();

    //     $ticket_request_closed = Ticket::where([['nik_pegawai',$id],['ticket_category','request',]])
    //     ->whereIn('last_status',['closed','rejected'])
    //     ->count();

    //     $ticket_request_open = Ticket::where([['nik_pegawai',$id],['ticket_category','request',]])
    //     ->whereNotIn('last_status',['closed','rejected'])
    //     ->count();

    //     $total_request                 = $ticket_request_closed + $ticket_request_open;
    //     if($total_request==0){
    //         $ticket_request_open_percent   = 0;
    //         $ticket_request_closed_percent = 0;
    //     }
    //     else{
    //         $ticket_request_open_percent   = ($ticket_request_open / $total_request) * 100;
    //         $ticket_request_closed_percent = ($ticket_request_closed / $total_request) * 100;
    //     }
    //     return view('ticket.home',compact(
    //         'issue',
    //         'data_user',
    //         'ticket_support_today',
    //         'ticket_support_closed',
    //         'ticket_support_open',
    //         'ticket_request_today',
    //         'ticket_request_closed',
    //         'ticket_request_open',
    //         'ticket_support_open_percent',
    //         'ticket_support_closed_percent',
    //         'ticket_request_open_percent',
    //         'ticket_request_closed_percent',
    //         'total_support',
    //         'total_request'
    //     ));
    // }


    public function listDetailIssue(Request $request)
    {
        $issue_id = $request->id;

        $issueDetails = DB::table('detail_issue_categories')->where('issue_category_id',$issue_id)->pluck('name','id')->all();
        return response()->json(['issueDetails' => $issueDetails]);
    }

    public function department(){
        $department = DB::table('department')->get();
        return response()->json(['department'=>$department]);
    }

    public function select_support_issue($id){
        $issue = DB::table('issue_categories')
                ->where('department',$id)
                ->get();
        return response()->json(['issue'=>$issue]);
    }

    public function support_issue($data){
        $support_issue = DB::table('issue_categories')
                        ->where('department', $data)
                        ->where('type', 'support')
                        ->get();
        return response()->json(['support_issue'=>$support_issue]);
    }

    public function listIssue($type)
    {
        // $issue = IssueCategory::pluck('name', 'id')
        //         ->where('type',$type)
        //         ->all();
        $issue = DB::table('issue_categories')
                ->select('name', 'id')
                ->where('type',$type)
                ->get();
        // $issueDetails = DB::table('issue_categories')->where('issue_category_id',$issue_id)->pluck('name','id')->all();
        return response()->json($issue);
    }

    //===SUPPORT===
    public function storeSupport(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            's_nik'                 => 'required',
            's_name'                => 'required',
            's_department'          => 'required',
            's_factory'             => 'required',
            's_email_anda'          => 'required',
            's_select_issue'        => 'required',
            's_select_detail_issue' => 'required',
            's_title'               => 'required',
            's_description'         => 'required',
        ]);

        // // if(Ticket::where('name',str_slug($request->name))->exists())
        // //     return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);

        $carbon = Carbon::now();
        $code   = "SUP-".Carbon::parse($carbon)->format('ymd').Carbon::now()->format('u');

        if ($request->hasFile('s_upload'))
        {
            $file_name =  $request->s_upload->getClientOriginalName();
            $request->s_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }
        // dd($time);

        $permission = Ticket::firstorCreate([
            'nik_pegawai'              => $request->s_nik,
            'detail_issue_category_id' => $request->s_select_detail_issue,
            'ticket_category'          => 'support',
            'title'                    => $request->s_title,
            'description'              => $request->s_description,
            'attachment'               => $file_name,
            'last_status'              => 'open',
            // 'user_confirm_host'        => 'Not Set Yet',
            'code'                     => $code,
            'name'                     => $request->s_name,
            'department'               => $request->s_department,
            'factory'                  => $request->s_factory,
            'email_user'               => $request->s_email_anda,
            'ip_address'               => \Request::ip(),
        ]);

        $permission2 = DetailTicket::firstorCreate([
            'ticket_id'  => $permission->id,
            'status'     => 'open',
            'comment'    => '-',
            'user_id'    => $permission->nik_pegawai,
            'attachment' => $file_name,
            'remark'     => 'Sistem Menerima Ticket User',
        ]);

        $header = Ticket::find($permission->id);
        $header->last_status = $permission2->status;
        $header->save();

        if($request->s_select_detail_issue_2 !=null){
            $permission = Ticket::firstorCreate([
                'nik_pegawai'              => $request->s_nik,
                'detail_issue_category_id' => $request->s_select_detail_issue_2,
                'ticket_category'          => 'support',
                'title'                    => $request->s_title_2,
                'description'              => $request->s_description_2,
                'attachment'               => $file_name,
                'last_status'              => 'open',
                // 'user_confirm_host'        => 'Not Set Yet',
                'code'                     => $code,
                'name'                     => $request->s_name,
                'department'               => $request->s_department,
                'factory'                  => $request->s_factory,
                'email_user'               => $request->s_email_anda,
                'ip_address'               => \Request::ip(),
            ]);

            $permission2 = DetailTicket::firstorCreate([
                'ticket_id'  => $permission->id,
                'status'     => 'open',
                'comment'    => '-',
                'user_id'    => $permission->nik_pegawai,
                'remark'     => 'Sistem Menerima Ticket User',
            ]);

            $header = Ticket::find($permission->id);
            $header->last_status = $permission2->status;
            $header->save();

            if($request->s_select_detail_issue_3 !=null){
                $permission = Ticket::firstorCreate([
                    'nik_pegawai'              => $request->s_nik,
                    'detail_issue_category_id' => $request->s_select_detail_issue_3,
                    'ticket_category'          => 'support',
                    'title'                    => $request->s_title_3,
                    'description'              => $request->s_description_3,
                    'attachment'               => $file_name,
                    'last_status'              => 'open',
                    // 'user_confirm_host'        => 'Not Set Yet',
                    'code'                     => $code,
                    'name'                     => $request->s_name,
                    'department'               => $request->s_department,
                    'factory'                  => $request->s_factory,
                    'email_user'               => $request->s_email_anda,
                    'ip_address'               => \Request::ip(),
                ]);

                $permission2 = DetailTicket::firstorCreate([
                    'ticket_id'  => $permission->id,
                    'status'     => 'open',
                    'comment'    => '-',
                    'user_id'    => $permission->nik_pegawai,
                    'remark'     => 'Sistem Menerima Ticket User',
                ]);

                $header = Ticket::find($permission->id);
                $header->last_status = $permission2->status;
                $header->save();
            }

        }
        // $sum = Ticket::select()
        // ->where()
        // ->whereNull('delete_at')
        // ->first();

        return response()->json(['code' => $permission->code, 'success', 200]);
    }

     //===REQUEST===
     public function storeRequest(Request $request)
     {

         $this->validate($request, [
             'r_nik'                 => 'required',
             'r_name'                => 'required',
             'r_department'          => 'required',
             'r_factory'             => 'required',
             'r_email_anda'          => 'required',
             'r_select_issue'        => 'required',
             'r_select_detail_issue' => 'required',
             'r_title'               => 'required',
             'r_description'         => 'required',
             'ip_address'            => \Request::ip(),
         ]);


         if ($request->hasFile('r_upload'))
         {
            $file_name =  $request->r_upload->getClientOriginalName();
            $request->r_upload->storeAs('public', $file_name);
         }
         else{
            $file_name = null;
         }


         $carbon = Carbon::now();
         $code   = "REQ-".Carbon::parse($carbon)->format('ymd').Carbon::now()->format('u');

         $permission = Ticket::firstorCreate([
             'nik_pegawai'              => $request->r_nik,
             'detail_issue_category_id' => $request->r_select_detail_issue,
             'ticket_category'          => 'request',
             'title'                    => $request->r_title,
             'description'              => $request->r_description,
             'attachment'               => $file_name,
             'last_status'              => 'open',
             'code'                     => $code,
             'name'                     => $request->r_name,
             'department'               => $request->r_department,
             'factory'                  => $request->r_factory,
             'email_user'               => $request->r_email_anda,
             'email_atasan'             => $request->r_email_atasan,
         ]);

        $permission2 = DetailTicket::firstorCreate([
            'ticket_id'  => $permission->id,
            'status'     => 'open',
            // 'comment'    => '-',
            'user_id'    => '2',
            'remark'     => 'Sistem Menerima Ticket User',
        ]);

        $header = Ticket::find($permission->id);
        $header->last_status = $permission2->status;
        $header->save();

        $this->sendEmailNewTicket($permission->id);

         return response()->json(['code' => $code, 'success', 200]);
     }

     //===PROJECT===
     public function storeProject(Request $request)
     {

        $this->validate($request, [
            'p_nik'                 => 'required',
            'p_name'                => 'required',
            'p_department'          => 'required',
            'p_factory'             => 'required',
            'p_email_anda'          => 'required',
            'p_select_issue'        => 'required',
            'p_select_detail_issue' => 'required',
            'p_title'               => 'required',
            'p_description'         => 'required',
            'ip_address'            => \Request::ip(),
        ]);

        $file_name = null;
        $carbon = Carbon::now();
        $code   = "PRO-".Carbon::parse($carbon)->format('ymd').Carbon::now()->format('u');

        $permission = Ticket::firstorCreate([
            'nik_pegawai'              => $request->p_nik,
            'detail_issue_category_id' => $request->p_select_detail_issue,
            'ticket_category'          => 'project',
            'title'                    => $request->p_title,
            'description'              => $request->p_description,
            'attachment'               => $file_name,
            'last_status'              => 'open',
            'code'                     => $code,
            'name'                     => $request->p_name,
            'department'               => $request->p_department,
            'factory'                  => $request->p_factory,
            'email_user'               => $request->p_email_anda,
            'email_atasan'             => $request->p_email_atasan
        ]);

       $permission2 = DetailTicket::firstorCreate([
           'ticket_id'  => $permission->id,
           'status'     => 'open',
           // 'comment'    => '-',
           'user_id'    => '2',
           'remark'     => 'Sistem Menerima Ticket User',
       ]);

       $header = Ticket::find($permission->id);
       $header->last_status = $permission2->status;
       $header->save();

       $this->sendEmailNewTicket($permission->id);

        return response()->json(['code' => $code, 'success', 200]);
    }
    public function dataHistory(Request $request)
    {
        $pegawai = $request->nik_pegawai;
        if(request()->ajax())
        {
            $data = Ticket::where('nik_pegawai',$pegawai)->orderby('created_at','desc');
            return datatables()->of($data)
            ->editColumn('uuid',function($data){
                return $data->id;
            })
            ->editColumn('code', function($data){

                return '<button type="button" class="label label-flat border-grey text-indigo-800" class="btn btn-link legitRipple"  onclick="detail(\''.$data->id.'\')">'.$data->code.'</button>';
            })
            ->editColumn('ticket_category', function($data){
                if($data->ticket_category == 'support')
                {
                    return '<span class="label label bg-pink">Support</span>';
                    // return '<button type="button" class="label label bg-pink" class="btn btn-link legitRipple"  onclick="detail(\''.$data->id.'\')">Support</button>';
                }
                if($data->ticket_category == 'request')
                {
                    return '<span class="label label bg-purple">Request</span>';
                    // return '<button type="button" class="label label bg-purple" class="btn btn-link legitRipple"  onclick="detail(\''.$data->id.'\')">Request</button>';
                }
            })
            ->editColumn('last_status', function($data){
                if($data->last_status == "waiting for user dept-head approval") return '<span class="label label bg-grey">Waiting for user Dept-Head approval</span>';
                if($data->last_status == "waiting for ict dept-head approval") return '<span class="label label bg-grey">Waiting for ICT Dept-Head approval</span>';
                if($data->last_status == "open") return '<span class="label label-info">Open</span>';
                if($data->last_status == "in progress") return '<span class="label label-warning">In Progress</span>';
                if($data->last_status == "imc process") return '<span class="label label bg-brown">IMC Process</span>';
                if($data->last_status == "purchasing process") return '<span class="label label bg-brown">Purchasing Process</span>';
                if($data->last_status == "supplier process") return '<span class="label label bg-brown">Supplier Process</span>';
                if($data->last_status == "cancel") return '<span class="label label-danger">Canceled</span>';
                if($data->last_status == "closed") return '<span class="label label-success">Closed</span>';
            })
            ->addColumn('action', function($data) {
                if($data->last_status=='closed' || $data->last_status=='cancel' ){
                    return '';
                }else{

                    $detail = DetailTicket::where([
                        ['ticket_id', $data->id],
                        ['status', 'onprogress'],
                        ])
                    ->orderby('created_at','asc')
                    ->get();

                    if($detail != null){
                        return view('ticket._action', [
                            'model' => $data,
                            'close' => route('ticket.close',$data->id),
                            // 'cancel' => route('ticket.cancel',$data->id),
                        ]);
                    }
                    else{
                        return view('ticket._action', [
                            'model' => $data,
                            'close' => route('ticket.close',$data->id),
                            'cancel' => route('ticket.cancel',$data->id),
                        ]);
                    }

                }
            })
            ->rawColumns(['ticket_category','code','last_status','action'])
            ->make(true);
        }
    }

    public function close($id)
    {
        $user            = Ticket::find($id);
        $obj             = new StdClass();
        $obj->id         = $id;
        $obj->user_id    = $user->nik_pegawai;
        $obj->url_update = route('ticket.close_update',$user->id);

		return response()->json($obj,200);
    }

    public function close_update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'close_keterangan' => 'required',
        // ]);

        $ticket = Ticket::find($id);
        if($ticket->last_status == 'closed' || $ticket->last_status == 'cancel'){

            return response()->json(522);
        }
        else{
            $detail_ticket = DetailTicket::firstorCreate([
                'ticket_id' => $id,
                'status'    => 'closed',
                'remark'    => $request->close_keterangan,
                'user_id'   => $ticket->nik_pegawai,
            ]);

            $detail_ticket_id = $detail_ticket->id;

            $ticket->last_status = 'closed';
            $ticket->save();

            // SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);
            return response()->json(200);
        }
    }

    public function cancel($id)
    {
        $user            = Ticket::find($id);
        $obj             = new StdClass();
        $obj->id         = $id;
        $obj->user_id    = $user->nik_pegawai;
        $obj->url_update = route('ticket.cancel_update',$user->id);

		return response()->json($obj,200);
    }

    public function cancel_update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'close_keterangan' => 'required',
        // ]);

        $ticket = Ticket::find($id);
        if($ticket->last_status == 'closed' || $ticket->last_status == 'cancel'){

            return response()->json(522);
        }
        else{
            $detail_ticket = DetailTicket::firstorCreate([
                'ticket_id' => $id,
                'status'    => 'cancel',
                'remark'    => $request->cancel_keterangan,
                'user_id'   => $ticket->nik_pegawai,
            ]);

            $detail_ticket_id = $detail_ticket->id;

            $ticket->last_status = 'cancel';
            $ticket->save();

            // SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);
            return response()->json(200);
        }
    }


    public function dataProgress(Request $request)
    {
        $id = $request->ticket_id;
        if(request()->ajax())
        {
            $data = DetailTicket::where([
                ['ticket_id', $id],
                ['comment', null],
                ])
            ->orderby('created_at','asc')
            ->get();
            return datatables()->of($data)
            ->editColumn('user_id',function ($data)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            })
            ->editColumn('status', function($data){
                if($data->status == "waiting for user dept-head approval") return '<span class="label label bg-grey">Waiting for user Dept-Head approval</span>';
                if($data->status == "waiting for ict dept-head approval") return '<span class="label label bg-grey">Waiting for ICT Dept-Head approval</span>';
                if($data->status == "open") return '<span class="label label-info">Open</span>';
                if($data->status == "in progress") return '<span class="label label-warning">In Progress</span>';
                if($data->status == "imc process") return '<span class="label label bg-brown">IMC Process</span>';
                if($data->status == "purchasing process") return '<span class="label label bg-brown">Purchasing Process</span>';
                if($data->status == "supplier process") return '<span class="label label bg-brown">Supplier Process</span>';
                if($data->status == "rejected") return '<span class="label label-danger">Rejected by ICT</span>';
                if($data->status == "closed") return '<span class="label label-success">Closed</span>';
            })
            ->rawColumns(['status','action'])
            ->make(true);
        }
    }

    public function dataComment(Request $request)
    {
        $id = $request->ticket_id;
        if(request()->ajax())
        {
            $data = DetailTicket::where([
                ['ticket_id', $id],
                ['comment', '!=', null],
                ])
            ->orderby('created_at','asc')
            ->get();
            return datatables()->of($data)
            ->editColumn('user_id',function ($data)
            {
                if($data->is_admin == true)
                {
                    return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
                }
                else
                {
                    return $data->Ticket->name;
                }
            })
            ->addColumn('action',function($data){
                $file_name = $data->attachment;
                $link = route('ticket.download',$data->id);

                if($data->attachment != null){
                    return '<a class="label label-flat border-grey text-grey-600"
                    href="'.$link.'">'.$file_name.'</a>';
                }
                else{
                    return '';
                }

            })
            ->make(true);
        }
    }

    public function download($id)
    {
        $file = DetailTicket::where('id', $id)->firstOrFail();

        $pathToFile = storage_path('app/public/' . $file->attachment);
        return response()->download($pathToFile);
    }

    public function addComment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        $user_id   = $request->_user_id;
        $comment   = $request->comment;

        if ($request->hasFile('c_upload'))
        {
            $file_name =  $request->c_upload->getClientOriginalName();
            $request->c_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }


        $detail_tikcet = DetailTicket::firstorCreate([
            'ticket_id'  => $ticket_id,
            'comment'    => $comment,
            'user_id'    => $user_id,
            'attachment' => $file_name,
        ]);

        $ticket          = Ticket::find($ticket_id);
        $count_comment   = DetailTicket::where('ticket_id',$ticket_id)->where('user_id', $ticket->nik_pegawai)->where('status',null)->count();
        $ticket->comment = $count_comment;
        $ticket->save();

        return response()->json('success', 200);
    }

    // public function email()
    // {
    //     $id_ticket = 'a57d9610-46f0-11ea-8e0f-7ddba70ffc89';
    //     $ticket = Ticket::where('id',$id_ticket)->first();
    //     $cc     = ['niam.alfiyan@aoi.co.id'];
    //     $emails = ['niam.alfiyan@aoi.co.id'];
    //     return vieW('ticket.email', compact('ticket'));
    // }
    static function sendEmailNewTicket($id_ticket)
    {
        try
        {
            $ticket = Ticket::where('id',$id_ticket)->first();
            $detail_issue_category_id = $ticket->detail_issue_category_id;
            $department = IssueCategory::where('id', function($query) use ($detail_issue_category_id)
            {
                $query->select('id')
                ->from('detail_issue_categories')
                ->where('id',$detail_issue_category_id);
            })->first();
// Error disini
            // $email_hosts = User::where('department', $list_send_email_host)
            //                     ->where('position', '>', 2)
            //                     ->pluck('email')->toArray();

            // $email_hosts  = $email_hosts->implode(',');

            //$cc     = ['niam.alfiyan@aoi.co.id'];
            $emails = $ticket->email_atasan;
            $url    = 'http://127.0.0.1:8000/ticket/approval/guest/'.$id_ticket;

            Mail::send('ticket.email', compact('ticket','url'), function ($message) use ($emails)
            {
                $message->subject('NEW TICKET BUAT ATASAN REQUEST');
                $message->from('no-reply@aoi.co.id', 'Ticket');
                //$message->cc($cc);
                $message->to($emails);
                //$message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

            // $email_hosts="emaildepthead@gmail.com";
            // $url_host    = 'http://127.0.0.1:8000/ticket/approval/host/'.$ticket->id;
            // Mail::send('ticket.email', compact('ticket','url'), function ($message) use ($email_hosts)
            // {
            //     $message->subject('NEW TICKET REQUEST');
            //     $message->from('no-reply@aoi.co.id', 'Ticket');
            //     //$message->cc($cc);
            //     $message->to($email_hosts);
            //     //$message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            // });

        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }

    static function sendEmailUpdateTicket($id_ticket, $id_detail_ticket)
    {
        try
        {
            $ticket  = Ticket::where('id',$id_ticket)->first();
            $detail_ticket = DetailTicket::where(
                       'id',$id_detail_ticket)->first();
            //$cc     = ['niam.alfiyan@aoi.co.id'];
            $emails = $ticket->email_user;
            $url    = url()->current();

            Mail::send('ticket.email_update', compact('ticket','detail_ticket','url'), function ($message) use ($emails)
            {
                $message->subject('UPDATE INFORMATION TICKET');
                $message->from('no-reply@aoi.co.id', 'Ticket');
                //$message->cc($cc);
                $message->to($emails);
                //$message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }

    static function sendEmailCommentTicket($id_ticket, $id_detail_ticket)
    {
        try
        {
            $ticket  = Ticket::where('id',$id_ticket)->first();
            $detail_ticket = DetailTicket::where(
                       'id',$id_detail_ticket)->first();
            //$cc     = ['niam.alfiyan@aoi.co.id'];
            $emails = $ticket->email_user;
            $url    = url()->current();

            Mail::send('ticket.email_comment', compact('ticket','detail_ticket','url'), function ($message) use ($emails)
            {
                $message->subject('UPDATE INFORMATION TICKET');
                $message->from('no-reply@aoi.co.id', 'Ticket');
                //$message->cc($cc);
                $message->to($emails);
                //$message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }

    public function testEmail()
    {
        return view('ticket.email_update');

    }
    public function approvalGuest($id)
    {
        $ticket_id = $id;
        $ticket = Ticket::find($ticket_id);
        $email_atasan = $ticket->email_atasan;
        $ticket->is_confirm_guest = true;
        $ticket->date_confirm_guest = Carbon::now();
        $ticket->save();

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => 'Waiting ICT Dept-Head Approval',
            'remark'    => 'Approved by email '.$email_atasan,
            'user_id'   => '2',
        ]);

        $detail_ticket_id = $detail_ticket->id;
        $this->sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        try
        {
            $ticket = Ticket::where('id',$ticket_id)->first();
            //$cc     = ['niam.alfiyan@aoi.co.id'];
            $emails = ['niam.alfiyan@aoi.co.id'];
            $url    = 'http://127.0.0.1:8000/ticket/approval/host/'.$ticket->id;

            Mail::send('ticket.email', compact('ticket','url'), function ($message) use ($emails)
            {
                $message->subject('NEW TICKET REQUEST');
                $message->from('no-reply@aoi.co.id', 'Ticket');
                //$message->cc($cc);
                $message->to($emails);
                //$message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }


    }

    public function approvalHost($id)
    {
        $ticket_id = $id;
        $ticket = Ticket::find($ticket_id);
        $ticket->is_confirm_host = true;
        $ticket->date_confirm_host = Carbon::now();
        $ticket->save();

        $detail_tikcet = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => 'Approved By Host',
            'remark'    => 'Waiting For Execution',
            'user_id'   => '2',
        ]);
    }

    public function approval($id, $status){
        $ticket_id = $id;

        $ticket =Ticket::find($ticket_id);
        // $ticket->approval = $status;
        $ticket->user_confirm_guest = $ticket->email_atasan;
        $ticket->date_confirm_guest = Carbon::now();
        if($status == 'approve'){
            $ticket->is_confirm_guest = true;
        }
        else if($status == 'reject'){
            $ticket->is_confirm_guest = false;
        }
        $ticket->update();
        $ticket = Ticket::where('id',$ticket_id)->first();


        if($status=='approve'){
            $data=DB::table('ticket_v')
                        ->where('id', $ticket_id)
                        ->pluck('issue_dept');

            // $data=DB::table('tickets')
            //             ->where('id', $ticket_id)
            //             ->first();
            // $data = $data->department;

            $emails = DB::table('users')
                                // ->where('department','=', $data)
                                ->where('dept_id','LIKE', '%'.$data[0].'%')
                                ->where('position', '>', 2)
                                ->first();
            $emails = $emails->email;

            $url    = 'http://127.0.0.1:8000/ticket/approval/host/'.$id;
            Mail::send('ticket.email', compact('ticket','url'), function ($message) use ($emails)
            {
                $message->subject('NEW TICKET BUAT HEAD REQUEST');
                $message->from('no-reply@aoi.co.id', 'Ticket');
                $message->to($emails);
            });
            return view('landing.index', compact('status'));

        }
        else{
            return view('landing.index', compact('status'));
        }
    }

    public function approval_host($id, $status){
        $ticket_id = $id;
        $ticket = Ticket::where('id',$ticket_id)->first();
        // $ticket->approval_head = $status;
        if($status == 'approve'){
            $ticket->is_confirm_host = true;
        }
        else{
            $ticket->is_confirm_host = false;
        }
        $ticket->user_confirm_host = 'Head '.$ticket->department;
        $ticket->date_confirm_host = Carbon::now();
        $ticket->update();
        return view('landing.index', compact('status'));
    }

}
