<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Priority;
use DB;
use StdClass;
use Validator;
use App\Models\Department;
use Yajra\Datatables\Datatables;

class DepartmentController extends Controller
{
    public function index(){
        return view('master-data-department.index');
    }
    public function data(){
        $data = DB::table('department')->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('master-data-department._action', [
                    'model'      => $data,
                    'edit_modal' => route('department.edit',$data->id),
                    'delete'     => route('department.destroy',$data->id),
                ]);

            })
            ->make(true);
    }
    public function store(Request $request){
        // $data = request()->validate([
        //     'priority' => 'required|unique:priority,priority'
        // ]);
        // $this->validate($request, [
        //     'priority' => 'required|unique:priority,priority'
        // ]);

        // if(IssueCategory::where('name',str_slug($request->name))->exists())
        //     return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);

        $permission = Department::firstorCreate([
            'dept_name'        => $request->department,
        ]);
        return response()->json('success', 200);
    }

    public function edit($id)

    {
        $department             = Department::find($id);
        $obj                    = new StdClass();
        $obj->id                = $id;
        $obj->dept_name          = $department->dept_name;
        $obj->url_update        = route('department.update',$department->id);

		return response()->json($obj,200);
    }
    public function update(Request $request, $id)
    {

        $update = Department::find($id);
        // $update->priority = str_slug($request->priority);
        $update->dept_name = $request->department;

        $update->save();
    }
    public function destroy($id)
    {
        $department = Department::findorFail($id)->delete();
        return response()->json(200);
    }

}
