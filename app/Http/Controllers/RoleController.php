<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\Role;
use App\Models\Permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $permissions = Permission::pluck('display_name', 'id')->all();
        return view('role.index',compact('permissions'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Role::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('role._action', [
                    'model' => $data,
                    'edit_modal' => route('role.edit',$data->id),
                    'delete' => route('role.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Role::where('name',str_slug($request->name))->exists()) return response()->json(['message' => 'Role sudah ada, silahkan cari nama Role lain.'], 422);

        $role = Role::firstorCreate([
            'name' => str_slug($request->name),
            'display_name' => $request->name,
            'description' => $request->description
        ]);

        $mappings =  json_decode($request->mappings);
        $array = array();

        foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
        
        if($role->save()) $role->attachPermissions($array);
        return response()->json(200);
        
    }

    public function storePermission(Request $request)
    {
        $role = Role::find($request->role_id);
        $role->attachPermissions([$request->permission_id]);
        return response()->json(200);
    }

    public function edit($id)
    {
        $role = Role::find($id);
        $obj = new StdClass();
        $obj->id = $id;
        $obj->name = $role->display_name;
		$obj->description = $role->description;
		$obj->permissions = $role->permissions()->get();
		$obj->url_update = route('role.update',$role->id);
		$obj->url_permission_role = route('role.dataPermission',$role->id);
		
		return response()->json($obj,200);
    }

    public function dataPermission(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select permissions.id as id
            ,permissions.display_name
            from roles 
            join permission_role on permission_role.role_id = roles.id 
            join permissions on permission_role.permission_id = permissions.id
            where roles.id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('role.destroyPermissionRole',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Role::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $role = Role::find($id);
        $role->name = str_slug($request->name);
        $role->display_name = $request->name;
        $role->description = $request->description;
        $role->save();

        return response()->json('success', 200);
    }

   
    public function destroy($id)
    {
        $role = Role::findorFail($id)->delete();
        return response()->json(200);
    }

    public function destroyPermissionRole($role_id,$permission_id)
    {
        $role = Role::find($role_id);
        $permissions = $role->permissions()->where('permission_id','!=',$permission_id)->get();
        $array = array();
        foreach ($permissions as $key => $permission) {
            $array [] = $permission->id;
        }
        
        $role->perms()->sync([]);
        $role->attachPermissions($array);

        return response()->json($permissions);
    }
}
