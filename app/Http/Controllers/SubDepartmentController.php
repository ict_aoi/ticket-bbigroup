<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use StdClass;
use Validator;
use App\Models\Department;
use App\Models\SubDepartment;
use Yajra\Datatables\Datatables;

class SubDepartmentController extends Controller
{
    public function index(){
        $department = Department::where('id', '!=', '123')->pluck('dept_name', 'id')->all();
        return view('master-data-sub-department.index',compact('department'));
    }
    public function data(){
        $data = DB::table('sub_department')
                ->leftjoin('department', 'department.id', 'sub_department.id_dept_name')
                ->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('master-data-sub-department._action', [
                    'model'      => $data,
                    'edit_modal' => route('subdepartment.edit',$data->id),
                    'delete'     => route('subdepartment.destroy',$data->id),
                ]);

            })
            ->make(true);
    }

    public function store(Request $request){
        // $data = request()->validate([
        //     'priority' => 'required|unique:priority,priority'
        // ]);
        // $this->validate($request, [
        //     'priority' => 'required|unique:priority,priority'
        // ]);

        // if(IssueCategory::where('name',str_slug($request->name))->exists())
        //     return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);

        $sub_dept = SubDepartment::firstorCreate([
            'sub_dept_name'        => $request->sub_department,
            'id_dept_name'        => $request->dept,
        ]);
        return response()->json('success', 200);
    }
    public function edit($id)

    {
        $sub_department             = SubDepartment::find($id);
        $obj                    = new StdClass();
        $obj->id                = $id;
        $obj->sub_dept_name          = $sub_department->sub_dept_name;
        $obj->id_dept_name          = $sub_department->id_dept_name;
        $obj->url_update        = route('subdepartment.update',$sub_department->id);

		return response()->json($obj,200);
    }
    public function update(Request $request, $id)
    {

        $update = SubDepartment::find($id);
        // $update->priority = str_slug($request->priority);
        $update->sub_dept_name = $request->sub_department;
        $update->id_dept_name = $request->update_dept;

        $update->save();
    }
    public function destroy($id)
    {
        $sub_department = SubDepartment::findorFail($id)->delete();
        return response()->json(200);
    }

}
