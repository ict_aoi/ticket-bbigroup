<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Yajra\Datatables\Datatables;

use App\Models\IssueCategory;
use App\Models\DetailIssueCategory;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Priority;
use App\Models\DetailTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DateTime;

use App\Http\Controllers\TicketController as SettingTicket;

class AdminSupportController extends Controller
{
    public function index(Request $request)
    {
        // $position = Auth::user()->position;
        // $sub_dept = Auth::user()->subdept_id;
        // dd($sub_dept);
        $priority = Priority::where('id', '!=', null)->pluck('priority', 'id')->all();
        $comments = DetailTicket::where('comment', '!=', null)
                    ->orderby('created_at','asc')
                    ->get();
        //$comments = array();
        return view('admin-support.index',compact('comments','priority'));
    }
    public function data()
    {
        if(request()->ajax())
        {
            $position = Auth::user()->position;
            $sub_dept = Auth::user()->subdept_id;
            // dd($position);
            if($position <3){
                $data = DB::table('ticket_v')
                        ->where([['ticket_category', 'support'],['issue_sub_dept', $sub_dept]])
                        ->orderby('created_at','desc');
                // dd($data);
            }
            else{
                $department_role='';
                if(Auth::user()->hasRole('ict-manager')){
                    $department_role='ICT';
                }
                elseif(Auth::user()->hasRole('hr-manager')){
                    $department_role='HR';
                }
                elseif(Auth::user()->hasRole('mekanik-manager')){
                    $department_role='MEKANIK';
                }
                if(Auth::user()->hasRole([['ict-manager'],['hr-manager'],['mekanik-manager']])){
                    $data = DB::table('ticket_v')
                        ->where([['ticket_category', 'support'], ['issue_dept', 'like', '%'.$department_role.'%']])
                        ->orderby('created_at','desc');
                }
                else{
                    $data = DB::table('ticket_v')
                            ->where('ticket_category', 'support')
                            ->orderby('created_at','desc');
                }
            }

            return datatables()->of($data)
            ->editColumn('last_status',function ($data)
                {
                    if($data->last_status == 'open') return '<span class="label label-default">Open</span>';
                    if($data->last_status == 'solved') return '<span class="label label-success">Solved</span>';
                    if($data->last_status == 'reject') return '<span class="label label-danger">Reject</span>';
                    else return '<span class="label label-info">'.$data->last_status.'</span>';
                })
            ->editColumn('code',function ($data)
                {
                    return '<button type="button" class="label label-info" class="btn btn-link legitRipple"  onclick="showDetail(\''.$data->id.'\')">'.$data->code.'</button>';
                })
            ->addColumn('notifikasi', function($data)
                {
                    if($data->comment == null){
                        return '';
                    }
                    else{
                        return '<span class="badge badge-danger">'.$data->comment.'</span>';
                    }
                })
            ->editColumn('handled_by',function ($data)
                {
                    if($data->handled_by == null)
                    {
                        return '<span class="label label-danger">Not Set Yet</span>';
                    }
                    else
                    {
                        $change = DB::table('users')
                                ->where('id', $data->handled_by)
                                ->first();
                        // return $data->handled_by;
                        return $change->name;
                    }
                })
            ->editColumn('priority', function($data)
                {
                    if($data->priority == null)
                    {
                        return 'Not Set Yet';
                    }
                    else
                    {
                        $change = DB::table('priority')
                                ->where('id', $data->priority)
                                ->first();
                        // return $data->handled_by;
                        return $change->priority;
                    }
                })

            ->rawColumns(['code', 'last_status','notifikasi', 'handled_by', 'priority'])
            ->make(true);
        }
    }

    public function progres(Request $request)
    {
        if(request()->ajax())
        {
        $ticket_id = $request->ticket_id;
        $data = DetailTicket::where([
                ['ticket_id', $ticket_id],
                ['comment', null],
                ])
                ->orderby('created_at','desc')
                ->get();
        //dd($ticket_id);
        return datatables()->of($data)
        ->editColumn('user_id',function ($data)
        {
            return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
        })
        ->editColumn('status',function ($data)
        {
            if($data->status == 'open') return '<span class="label label-default">Open</span>';
            if($data->status == 'closed') return '<span class="label label-success">Closed</span>';
            if($data->status == 'reject') return '<span class="label label-danger">Reject</span>';
            else return '<span class="label label-info">'.$data->status.'</span>';
        })
        ->rawColumns(['user_id', 'status'])
        ->make(true);
        }

    }

    public function comment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        //dd($ticket_id);
        if(request()->ajax())
        {
        $data = DetailTicket::where([
            ['ticket_id', $ticket_id],
            ['comment', '!=', null],
            ])
        ->orderby('created_at','asc')
        ->get();
        return datatables()->of($data)
        ->editColumn('user_id',function ($data)
        {
            if($data->is_admin == true)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            }
            else
            {
                return $data->Ticket->name;
            }

        })

        ->addColumn('action',function($data){
            $file_name = $data->attachment;
            $link = route('adminSupport.download',$data->id);

            if($data->attachment != null){
                return '<a class="label label-flat border-grey text-grey-600"
                href="'.$link.'">'.$file_name.'</a>';
            }
            else{
                return '';
            }

        })
        ->make(true);
        }
    }

    public function download($id)
    {
        $file = DetailTicket::where('id', $id)->firstOrFail();

        $pathToFile = storage_path('app/public/' . $file->attachment);
        return response()->download($pathToFile);
    }

    public function storeProgres(Request $request)
    {
        $ticket_id = $request->ticket_id;
        $status    = $request->status;
        $priority = $request->priority;

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => $status,
            'remark'    => $request->remark,
            'user_id'   => auth::user()->id,
            'is_admin'  => true,
        ]);

        $ticket              = Ticket::find($ticket_id);
        $ticket->last_status = $status;
        $ticket->priority = $request->priority;

        if($ticket->firts_reponse_date == null)
        {
            $dateDur1 = new DateTime(Carbon::now());
            $dateDur2 = new DateTime($ticket->created_at);
            $ticket->firts_reponse_date = $dateDur1; //ini insert tanggal first response date
            $time = Carbon::now();
            $total = 0;
            $created = $dateDur2;
            $tanggal_respon = $dateDur1;
            $tanggal_masuk = $dateDur2;
            $temp = $tanggal_masuk->format('Y-m-d');
            $jam_tutup = new Carbon($temp.'16:30:00');
            $total_menit = 0;
            $interval = $dateDur2->diff($dateDur1);
            $hari = new DateTime($ticket->created_at);
            $total_arr = [];
            // dd($interval);
            $data = DB::table('workday')->get();
            $holiday = DB::table('holiday')->get();
            array_push($total_arr,['data: ', $data]);
            if($hari->format('Y-m-d')!=$tanggal_respon->format('Y-m-d')){
                // jika hari tiket direspon berbeda dengan hari tiket masuk
                while($hari->format('Y-m-d')!=$tanggal_respon->format('Y-m-d')){
                    // if($hari->format('l')=='Sunday' || $hari->format('l')=='Saturday'){
                    foreach($data as $dataa){
                        // disini foreach hari libur
                        // foreach($holiday as $holidayy){
                            array_push($total_arr,['hari', $dataa, ]);
                            if($hari->format('l')==$dataa->day_name_english && $dataa->active==false){
                                array_push($total_arr,['sekip', $hari->format('Y-m-d')]);
                            }
                            else if($hari->format('l')==$dataa->day_name_english){
                                $hari_libur = DB::table('holiday')->where('date',$hari->format('Y-m-d'))->first();
                                if($hari_libur!=null){
                                    $jam_tutup = new Carbon($hari->format('Y-m-d').$dataa->end_hour);
                                    $jam_buka = new Carbon($hari->format('Y-m-d').$dataa->start_hour);
                                    if($hari == $dateDur2){
                                        $total = $created->diff($jam_tutup);
                                        $temp = $total->h*60 + $total->i;
                                        $total_menit = $total_menit + $temp ;
                                        array_push($total_arr, [$hari->format('Y-m-d'), $dateDur2->format('Y-m-d'),'  batas if  ',$temp]);

                                    }
                                    else{
                                        $diff = $jam_buka->diff($jam_tutup);
                                        $temp = $diff->h*60 + $diff->i;
                                        // $temp = 570;
                                        $total_menit = $total_menit+$temp;
                                        array_push($total_arr, [$hari->format('Y-m-d'),'  batas else  ',$temp]);
                                    }
                                }
                            }
                        // }
                    }
                    // $tanggal_masuk->modify('+1 day');
                    $hari->modify('+1 day');
                }
                $last_day = Carbon::now();
                $hour = DB::table('workday')
                    ->where('day_name_english','=', $last_day->format('l') )
                    ->first();
                $last_day = $last_day->format('Y-m-d');
                $last_day = new Carbon ($last_day.$hour->start_hour);
                $total = $dateDur1->diff($last_day);
                $temp = $total->h*60 + $total->i;
                $total_menit = $total_menit+$temp;
                array_push($total_arr, 'luar while  '. $temp. 'total menit    '. $total_menit);
                // dd($total_arr);
            }else{
                $response_duration = $dateDur1->diff($dateDur2);
                //     // dd($response_duration);
                $total_menit= $response_duration->h*60 + $response_duration->i;
            }

            $hours = floor($total_menit / 60);
            $minutes = $total_menit % 60;
            array_push($total_arr,'jam: '.$hours. 'Menit '.$minutes);
            $int_to_time = gmdate("H:i:s", $total_menit);
            // dd($total_arr);
            $ticket->duration_response_time = $hours. ' Jam '.$minutes.' Menit';
            // sudah masukin holiday

            //  Response Target
            // array_push($total_arr,'MASUK Target Response');
            // $workday = DB::table('workday')->get();
            // $data = DB::table('priority')
            //                 ->where('id',$ticket->priority)
            //                 ->first();
            // array_push($total_arr,$data);
            // if($data->unit_respon=='hari'){

            //     $duration = $data->duration_respon;
            //     $data_tambah = 0;
            //     $due = Carbon::now();
            //     while($duration >0){
            //         $due->modify('+1 day');
            //         foreach ($workday as $workdayy){
            //             array_push($total_arr,['duration: '.$duration, $due->format('Y-m-d'), $due->format('l')]);
            //             // if($due->format('l')!='Saturday' && $due->format('l')!='Sunday'){
            //             if($due->format('l')==$workdayy->day_name_english && $workdayy->active==true){
            //                 $duration = $duration-1;
            //             }
            //         }

            //     }
            //     array_push($total_arr,['akhir: '. $due->format('Y-m-d')]);
            //     // dd($total_arr);
            //     // $ticket->target_response = $due;
            // }
            // else{
            //     $duration = $data->duration_respon * 60;
            //     array_push($total_arr, 'total durasi: '. $duration);
            //     $respon = Carbon::now();
            //     $respon_2 = Carbon::now();
            //     $open_2 = new Carbon($respon->format('Y-m-d').'07:00:00');
            //     $data_workday = DB::table('workday')
            //                     ->where('day_name_english', $respon->format('l'))
            //                     ->first();
            //     $closed = new Carbon($respon->format('Y-m-d').$data_workday->end_hour);
            //     // dd($data_workday->end_hour);
            //     if($respon_2->modify('+'.$duration.' minutes') < $closed ){
            //         array_push($total_arr,['jam open', $respon_2->format('Y-m-d H:i:s'), 'jam close', $closed->format('Y-m-d H:i:s')]);
            //         array_push($total_arr,'Masuk IF');
            //         $open_2 = Carbon::now()->modify('+'.$duration.' minutes');
            //         array_push($total_arr,['Jam akhir', $open_2->format('Y-m-d H:i:s')]);
            //     }
            //     else{
            //         // $open_2 = Carbon::now();
            //         array_push($total_arr,'masuk else');
            //         array_push($total_arr,['var respon', $respon, 'var close', $closed]);
            //         $diff = $respon->diff($closed);
            //         array_push($total_arr,$diff);
            //         $temp = $diff->h*60 + $diff->i;
            //         $duration = $duration-$temp;
            //         // $open_2->modify('+1 day');
            //         array_push($total_arr,['setelah minus', 'durasi: '.$duration]);
            //         // dibawah ini mau diganti
            //         // while (duration > timediff hari itu)
            //         // foreeach disini
            //         while($duration > 0 ){
            //             foreach ($workday as $workdayy){
            //                 if($open_2->format('l') == $workdayy->day_name_english){
            //                     // while($duration >= $workdayy->start_hour->diff($workdayy->end_hour)){
            //                     if($workdayy->active == true){
            //                         $perbedaan = Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
            //                         $perbedaan = $perbedaan->h*60 + $perbedaan->i;
            //                         if($duration >= $perbedaan ){
            //                             $temp_duration = Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
            //                             $temp_duration = $temp_duration->h*60 + $temp_duration->i;
            //                             $duration = $duration - $temp_duration;
            //                             $open_2 = new Carbon($open_2->format('Y-m-d').$workdayy->start_hour);
            //                             // dikurangi diff
            //                         }
            //                         else{
            //                             $open_2->modify('+'.$duration.' minutes');
            //                             $temp_duration =Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
            //                             $temp_duration = $temp_duration->h*60 + $temp_duration->i;
            //                             $duration = $duration - $temp_duration;
            //                         }
            //                     }
            //                     $open_2->modify('+1 day');
            //                     array_push($total_arr,['didalam while', $open_2->format('Y-m-d H:i:s')]);
            //                     // }
            //                 }
            //             }
            //         }
            //         // sampe sini foreach nya
            //         // $open_2->modify('+'.$duration .' minutes');

            //         // if($open_2->format('l')=='Saturday' || $open_2->format('l')=='Sunday'){
            //         //     $open_2->modify('+2 day');
            //         // }
            //         array_push($total_arr,['diluar while', $open_2->format('Y-m-d H:i:s')]);
            //     }
            //     // dd($total_arr);
            //     // $ticket->target_response = $open_2;
            // }

            // DUE DATE TEST
            array_push($total_arr,'MASUK DUE DATE');
            $workday = DB::table('workday')->get();
            $holiday = DB::table('holiday')->get();
            $data = DB::table('priority')
                            ->where('id',$ticket->priority)
                            ->first();
            array_push($total_arr,$data);
            if($data->unit_reso=='hari'){

                $duration = $data->duration_reso;
                $data_tambah = 0;
                $due = Carbon::now();
                while($duration >0){
                    $due->modify('+1 day');
                    foreach ($workday as $workdayy){
                        // foreach ($holiday as $holidayy){
                            array_push($total_arr,['duration: '.$duration, $due->format('Y-m-d'), $due->format('l')]);
                            // if($due->format('l')!='Saturday' && $due->format('l')!='Sunday'){
                            $hari_libur = DB::table('holiday')->where('date',$due->format('Y-m-d'))->first();
                            if($hari_libur==null){

                            }
                            else if($due->format('l')==$workdayy->day_name_english && $workdayy->active==true ){
                                $duration = $duration-1;
                            }
                        // }
                    }

                }
                array_push($total_arr,['akhir: '. $due->format('Y-m-d')]);
                // dd($total_arr);
                $ticket->due_date = $due;
            }
            else{
                $duration = $data->duration_reso * 60;
                array_push($total_arr, 'total durasi: '. $duration);
                $respon = Carbon::now();
                $respon_2 = Carbon::now();
                $open_2 = new Carbon($respon->format('Y-m-d').'07:00:00');
                $data_workday = DB::table('workday')
                                ->where('day_name_english', $respon->format('l'))
                                ->first();
                $closed = new Carbon($respon->format('Y-m-d').$data_workday->end_hour);
                // dd($data_workday->end_hour);
                if($respon_2->modify('+'.$duration.' minutes') < $closed ){
                    array_push($total_arr,['jam open', $respon_2->format('Y-m-d H:i:s'), 'jam close', $closed->format('Y-m-d H:i:s')]);
                    array_push($total_arr,'Masuk IF');
                    $open_2 = Carbon::now()->modify('+'.$duration.' minutes');
                    array_push($total_arr,['Jam akhir', $open_2->format('Y-m-d H:i:s')]);
                }
                else{
                    // $open_2 = Carbon::now();
                    array_push($total_arr,'masuk else');
                    array_push($total_arr,['var respon', $respon, 'var close', $closed]);
                    $diff = $respon->diff($closed);
                    array_push($total_arr,$diff);
                    $temp = $diff->h*60 + $diff->i;
                    $duration = $duration-$temp;
                    // $open_2->modify('+1 day');
                    array_push($total_arr,['setelah minus', 'durasi: '.$duration]);
                    // dibawah ini mau diganti
                    // while (duration > timediff hari itu)
                    // foreeach disini
                    while($duration > 0 ){
                        foreach ($workday as $workdayy){
                            // foreach ($holiday as $holidayy){
                                if($open_2->format('l') == $workdayy->day_name_english){
                                    // while($duration >= $workdayy->start_hour->diff($workdayy->end_hour)){
                                    $hari_libur = DB::table('holiday')->where('date',$open_2->format('Y-m-d'))->first();
                                    if($workdayy->active == true &&$hari_libur==null){
                                        $perbedaan = Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
                                        $perbedaan = $perbedaan->h*60 + $perbedaan->i;
                                        if($duration >= $perbedaan ){
                                            $temp_duration = Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
                                            $temp_duration = $temp_duration->h*60 + $temp_duration->i;
                                            $duration = $duration - $temp_duration;
                                            $open_2 = new Carbon($open_2->format('Y-m-d').$workdayy->start_hour);
                                            // dikurangi diff
                                        }
                                        else{
                                            $open_2->modify('+'.$duration.' minutes');
                                            $temp_duration =Carbon::parse($workdayy->start_hour)->diff(Carbon::parse($workdayy->end_hour));
                                            $temp_duration = $temp_duration->h*60 + $temp_duration->i;
                                            $duration = $duration - $temp_duration;
                                        }
                                    }
                                    $open_2->modify('+1 day');
                                    array_push($total_arr,['didalam while', $open_2->format('Y-m-d H:i:s')]);
                                    // }
                                }
                            // }
                        }
                    }
                    // sampe sini foreach nya
                    // $open_2->modify('+'.$duration .' minutes');

                    // if($open_2->format('l')=='Saturday' || $open_2->format('l')=='Sunday'){
                    //     $open_2->modify('+2 day');
                    // }
                    array_push($total_arr,['diluar while', $open_2->format('Y-m-d H:i:s')]);
                }
                // dd($total_arr);
                $ticket->due_date = $open_2;
            }

            // $data = DB::table('priority')
            //         ->where('id',$ticket->priority)
            //         ->first();
            // array_push($total_arr,$data);
            // if($data->unit=='hari'){

            //     $duration = $data->duration;
            //     $data_tambah = 0;
            //     $due = Carbon::now();
            //     while($duration >=0){
            //         $due->modify('+1 day');
            //         array_push($total_arr,['duration: '.$duration, $due->format('Y-m-d'), $due->format('l')]);
            //         if($due->format('l')!='Saturday' && $due->format('l')!='Sunday'){
            //             $duration = $duration-1;
            //         }

            //     }
            //     array_push($total_arr,['akhir: '. $due->format('Y-m-d')]);
            //     // dd($total_arr);
            //     $ticket->due_date = $due;
            //     // $ticket->due_date = $dateDur1->modify('+'.$data->duration.' day');
            //     // if($ticket->due_date->format('l')=='Sunday' || $ticket->due_date->format('l')=='Saturday'){
            //     //     $ticket->due_date = $ticket->due_date->modify('+2 day');
            //     // }
            //     // if($ticket->due_date->format('l')=='Saturday'){
            //     //     $ticket->due_date = $ticket->due_date->modify('+2 day');
            //     // }
            // }
            // else{
            //     $duration = $data->duration * 60;
            //     array_push($total_arr, 'total durasi: '. $duration);
            //     $respon = Carbon::now();
            //     $respon_2 = Carbon::now();
            //     $closed = new Carbon($respon->format('Y-m-d').'16:30:00');
            //     $open_2 = new Carbon($respon->format('Y-m-d').'07:00:00');
            //     // if open+durasi < closed { open = open+duration }
            //     if($respon_2->modify('+'.$duration.' minutes') < $closed ){
            //         array_push($total_arr,['jam open', $respon_2->format('Y-m-d H:i:s'), 'jam close', $closed->format('Y-m-d H:i:s')]);
            //         array_push($total_arr,'Masuk IF');
            //         $open_2 = Carbon::now()->modify('+'.$duration.' minutes');
            //         array_push($total_arr,['Jam akhir', $open_2->format('Y-m-d H:i:s')]);
            //     }
            //     else{
            //         // $open_2 = Carbon::now();
            //         array_push($total_arr,'masuk else');
            //         array_push($total_arr,['var respon', $respon, 'var close', $closed]);
            //         $diff = $respon->diff($closed);
            //         array_push($total_arr,$diff);
            //         $temp = $diff->h*60 + $diff->i;
            //         $duration = $duration-$temp;
            //         array_push($total_arr,['setelah minus', 'durasi: '.$duration]);
            //         while($duration >=570){
            //             if($open_2->format('l')!='Saturday' && $open_2->format('l')!='Sunday'){
            //                 $duration = $duration - 570;
            //             }
            //             $open_2->modify('+1 day');
            //             array_push($total_arr,['didalam while', $open_2->format('Y-m-d H:i:s')]);
            //         }
            //         $open_2->modify('+'.$duration .' minutes');
            //         $open_2->modify('+1 day');
            //         if($open_2->format('l')=='Saturday' || $open_2->format('l')=='Sunday'){
            //             $open_2->modify('+2 day');
            //         }

            //         array_push($total_arr,['diluar while', $open_2->format('Y-m-d H:i:s')]);
            //     }
            //     // dd($total_arr);
            //     $ticket->due_date = $open_2;
            // }
        }

        if($request->status == 'closed')
        {
            $data = DB::table('tickets')
                    ->where ( 'id',$ticket_id)
                    ->first();

            $ticket->resolution_time = Carbon::now();

            // $date1 = new DateTime($ticket->resolution_time);
            // $date2 = new DateTime($data->firts_reponse_date);


            // $interval = $date1->diff($date2);

            // $ticket->total_time = "Selesai dalam  " .$interval->d." hari ,".$interval->h." jam, ".$interval->i." menit.";
            $dateDur1 = new DateTime(Carbon::now()); /*Respom time*/
            $dateDur2 = new DateTime($data->firts_reponse_date);
            $time = Carbon::now();
            $total = 0;
            $created = $dateDur2;
            $tanggal_respon = $dateDur1;
            $tanggal_masuk = $dateDur2;
            $temp = $tanggal_masuk->format('Y-m-d');
            $jam_tutup = new Carbon($temp.'16:30:00');
            $total_menit = 0;
            $interval = $dateDur2->diff($dateDur1);
            $hari = new DateTime($ticket->firts_reponse_date);
            $total_arr = [];
            // dd($interval);
            $data = DB::table('workday')->get();
            if($hari->format('Y-m-d')!=$tanggal_respon->format('Y-m-d')){
                while($hari->format('Y-m-d')!=$tanggal_respon->format('Y-m-d')){
                    foreach($data as $dataa){
                        // if($hari->format('l')=='Sunday' || $hari->format('l')=='Saturday'){
                        $hari_libur = DB::table('holiday')->where('date',$hari->format('Y-m-d'))->first();
                        if($hari->format('l')==$dataa->day_name_english && $dataa->active==false){
                            array_push($total_arr,['sekip', $hari->format('Y-m-d')]);
                        }
                        else if($hari->format('l')==$dataa->day_name_english && $hari_libur!=null){
                            array_push($total_arr,['sekip', $hari->format('Y-m-d')]);
                        }
                        else if($hari->format('l')==$dataa->day_name_english){
                            $jam_tutup = new Carbon($hari->format('Y-m-d').$dataa->end_hour);
                            $jam_buka = new Carbon($hari->format('Y-m-d').$dataa->start_hour);
                            if($hari == $dateDur2){
                                $total = $created->diff($jam_tutup);
                                $temp = $total->h*60 + $total->i;
                                $total_menit = $total_menit + $temp ;
                                array_push($total_arr, [$hari->format('Y-m-d'), $dateDur2->format('Y-m-d'),'  batas if  ',$temp]);

                            }
                            else{
                                $temp = $jam_buka->diff($jam_tutup);
                                $temp = $temp->h*60 + $temp->i;
                                // $temp = 570;
                                $total_menit = $total_menit+$temp;
                                array_push($total_arr, [$hari->format('Y-m-d'),'  batas else  ',$temp]);
                            }
                        }
                    }
                    // $tanggal_masuk->modify('+1 day');
                    $hari->modify('+1 day');
                }
                $last_day = Carbon::now();
                $hour = DB::table('workday')
                        ->where('day_name_english','=', $last_day->format('l') )
                        ->first();
                $last_day = $last_day->format('Y-m-d');
                $last_day = new Carbon ($last_day.$hour->start_hour);
                $total = $dateDur1->diff($last_day);
                $temp = $total->h*60 + $total->i;
                $total_menit = $total_menit+$temp;
                array_push($total_arr, 'luar while  '. $temp. 'total menit    '. $total_menit);
                    // dd($total_arr);
            }else{
                $response_duration = $dateDur1->diff($dateDur2);
                //     // dd($response_duration);
                $total_menit= $response_duration->h*60 + $response_duration->i;
            }

            $hours = floor($total_menit / 60);
            $minutes = $total_menit % 60;
            $int_to_time = gmdate("H:i:s", $total_menit);
            $ticket->total_time = $hours. ' Jam '.$minutes.' Menit';

            if($ticket->resolution_time > $ticket->due_date){
                $ticket->late = 1;
            }
            else{
                $ticket->late = 0;
            }
        }

        $ticket->handled_by = auth::user()->id;
        $ticket->save();

        $detail_ticket_id = $detail_ticket->id;

        // SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function storeComment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        $comment   = $request->comment;

        if ($request->hasFile('s_upload'))
        {
            $file_name =  $request->s_upload->getClientOriginalName();
            $request->s_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }


        $detail_tikcet = DetailTicket::firstorCreate([
            'ticket_id'  => $ticket_id,
            'comment'    => $comment,
            'is_admin'   => true,
            'user_id'    => auth::user()->id,
            'attachment' => $file_name,
            'is_admin'  => true,
        ]);

        $ticket = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->save();

        //SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_tikcet->id);

        return response()->json('success', 200);
    }

    public function edit($id){
        $ticket = DB::table('tickets')
                    ->where('id','like','%'.$id.'%')
                    ->first();
        return response()->json($ticket);
    }
}
