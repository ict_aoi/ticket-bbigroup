<?php namespace App\Http\Controllers;

use Hash;
use File;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Excel;

use App\Models\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data_request=DB::table('ticket_v')
                ->where('ticket_category','request')
                ->where('is_confirm_host', null)
                ->count();
        $data_waiting_execution = DB::table('ticket_v')
                                    ->where('ticket_category','request')
                                    ->where('last_status','open')
                                    ->count();
        $data_support = DB::table('ticket_v')
                        ->where('ticket_category','support')
                        ->where('last_status','open')
                        ->count();
        $compact = [$data_request, $data_waiting_execution, $data_support];
        return view('home', compact('compact'));
    }

    public function tes(){
        $data = DB::table('users')
                ->where('id',6)
                ->orderby('id','asc');
        DD($data);
        // $name = 'GeneralSite';

        // $excel = DB::table('users')
        //     ->get();

        // return Excel::create($name,function($excel){
        //     $excel->sheet('active',function($sheet){
        //         $sheet->setCellValue('A1','id');
        //         $sheet->setCellValue('B1','name');

        //         $sheet->setWidth(array(
        //             'A'=>15,
        //             'B'=>15
        //         ));

        //         $sheet->cell('A1', function($cell){
        //             $cell->setBackground('#00ffff');
        //             $cell->setFontColor('#000000');
        //         });
        //         $sheet->cell('B1', function($cell){
        //             $cell->setBackground('#00ffff');
        //             $cell->setFontColor('#000000');
        //         });
        //     });
        //     $excel->setActiveSheetIndex(0);
        // })->export('xlsx');

    }

    public function accountSetting()
    {
        return view('account_setting');
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function updateAccount(request $request,$id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $old_password = $request->old_password;
        $password = $request->new_password;
        $retype_password = $request->retype_password;
        $user = User::find($id);
        $image = null;
        if($password)
        {
            if (!Hash::check($old_password, $user->password)) return redirect()->back()
            ->withErrors([
                'old_password' => 'Password tidak sama dengan yang saat ini',
            ]);

            if ($password != $retype_password) return redirect()->back()
            ->withErrors([
                'retype_password' => 'Password yang anda masukan tidak sama dengan password baru',
            ]);
        }


        if ($request->hasFile('photo'))
        {
            if ($request->file('photo')->isValid())
            {
                $old_file = $avatar . '/' . $user->photo;

                if(File::exists($avatar)) File::delete($old_file);

                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        if ($password) $user->password = bcrypt($password);
        if ($image) $user->photo = $image;
        $user->save();

        return redirect()->route('accountSetting'); //->withSuccess();


    }
}
