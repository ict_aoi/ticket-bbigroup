<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Yajra\Datatables\Datatables;

use App\Models\IssueCategory;
use App\Models\DetailIssueCategory;
use App\Models\Ticket;
use App\Models\User;
use App\Models\DetailTicket;
use Illuminate\Http\Request;

use App\Http\Controllers\TicketController as SettingTicket;

class AdminProjectController extends Controller
{
    public function index(Request $request)
    {
        // dd(Auth::user()->hasRole('ict-manager'));
        $users = User::where('id', '!=', '123')->pluck('name', 'id')->all();
        return view('admin-project.index',compact('users'));
    }

    public function data(Request $request)
    {
        $user = Auth::user();
        $factory = $request->factory;
        $department_role='';


        // $status  = $request->last_status;
        // $department = $request->department;
        if(request()->ajax())
        {
            $data = DB::table('ticket_v')
                    ->where('ticket_category', 'project')
                    ->orderby('created_at','desc');
        return datatables()->of($data)
        ->editColumn('last_status',function ($data)
            {
                if($data->last_status == 'open') return '<span class="label label-default">Open</span>';
                if($data->last_status == 'closed') return '<span class="label label-success">Closed</span>';
                if($data->last_status == 'reject') return '<span class="label label-danger">Reject</span>';
                else return '<span class="label label-info">'.$data->last_status.'</span>';
            })
        ->editColumn('code',function ($data)
            {
                return '<button type="button" class="label label-info" class="btn btn-link legitRipple"  onclick="showDetail(\''.$data->id.'\')">'.$data->code.'</button>';
            })
        ->editColumn('handled_by',function ($data)
        {
            if($data->handled_by == null)
            {
                return '<span class="label label-danger">Not Set Yet</span>';
            }
            else
            {
                $change = DB::table('users')
                        ->where('id', $data->handled_by)
                        ->first();
                // return $data->handled_by;
                return $change->name;
            }
        })
        ->editColumn('priority', function ($data){
            if($data->priority == null){
                return '<span class="label label-danger">Not Set Yet</span>';
            }
            else{
                return $data->priority;
            }
        })
        ->editColumn('due_date', function ($data){
            if($data->due_date == null)
            {
                return '<span class="label label-danger">Not Set Yet</span>';
            }
            else
            {
                return $data->due_date;
            }
        })
        ->addColumn('notifikasi', function($data)
        {
            if($data->comment == null){
                return '';
            }
            else{
                return '<span class="badge badge-danger">'.$data->comment.'</span>';
            }
        })
        ->editColumn('is_confirm_guest', function ($data){
            if($data->is_confirm_guest ==true){
                return 'Approved';
            }
            else if($data->is_confirm_guest == null){
                return 'Waiting';
            }
            else{
                return 'Rejected';
            }
        })
        ->editColumn('is_confirm_host', function ($data){
            if($data->is_confirm_host ==true){
                return 'Approved';
            }
            else if($data->is_confirm_host == null){
                return 'Waiting';
            }
            else{
                return 'Rejected';
            }
        })
        ->rawColumns(['code', 'last_status','notifikasi', 'priority', 'handled_by', 'due_date', 'is_confirm_guest', 'is_confirm_host'])
        ->make(true);
        }
    }

    public function storeProgres(Request $request){
        $ticket_id = $request->ticket_id;
        $status    = $request->status;

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => $status,
            'progress'  => $request->progress,
            'remark'    => $request->remark,
            'user_id'   => Auth::user()->id,
            'is_admin'  => true,
        ]);
        $detail_ticket_id = $detail_ticket->id;
        $ticket              = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }

        $ticket->last_status = $status;
        $ticket->progress = $request->progress;
        $ticket->save();

        SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function progres(Request $request){
        $ticket_id = $request->ticket_id;
            $data = DetailTicket::where([
                    ['ticket_id', $ticket_id],
                    ['comment', null],
                    ])
                    ->orderby('created_at','asc')
                    ->get();
            // $data = DB::table('detail_tickets')
            //         ->where('ticket_id', $ticket_id)
            //         ->leftjoin('tickets', 'tickets.id', '=', 'detail_tickets.ticket_id')
            //         ->get();
            //dd($ticket_id);
            return datatables()->of($data)
            ->editColumn('user_id',function ($data)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            })
            ->editColumn('status',function ($data)
            {
                if($data->status == 'open') return '<span class="label label-default">Open</span>';
                if($data->status == 'closed') return '<span class="label label-success">Closed</span>';
                if($data->status == 'reject') return '<span class="label label-danger">Reject</span>';
                else return '<span class="label label-info">'.$data->status.'</span>';
            })
            ->rawColumns(['user_id', 'status'])
            ->make(true);

    }

    public function edit($id){
        $ticket = DB::table('tickets')
                    ->where('id','like','%'.$id.'%')
                    ->first();
        return response()->json($ticket);
    }

    public function UpdateDueDate(Request $request){
        $ticket_id = $request->__ticket_id;
        $priority   = $request->status;
        $due_date   = $request->due_date;
        $date = date_create($due_date);
        $due_date = date_format($date,"Y-m-d");
        $pic   = $request->pic;
        $remark   = $request->remark;
        //dd($remark);
        //dd($pic);
        $ticket = Ticket::find($ticket_id);
        $ticket->priority = $priority;
        $ticket->due_date = $due_date;
        $ticket->last_status = 'Set Due Date';
        $ticket->handled_by = $pic;
        $ticket->save();

        $detail_ticket = DetailTicket::firstorCreate([
            'progress'  => '-',
            'ticket_id' => $ticket_id,
            'status'    => 'Set Due Date',
            'remark'    => $remark,
            'user_id'   => auth::user()->id,
            'is_admin'  => true,
        ]);


        return response()->json('success', 200);
    }

    public function storeComment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        $comment   = $request->comment;

        if ($request->hasFile('r_upload'))
        {
            $file_name =  $request->r_upload->getClientOriginalName();
            $request->r_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id'  => $ticket_id,
            'comment'    => $comment,
            'user_id'    => auth::user()->id,
            'is_admin'   => true,
            'attachment' => $file_name,
        ]);
        $detail_ticket_id = $detail_ticket->id;

        $ticket              = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->save();

        // SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function updateApproval(Request $request)
    {
        $ticket_id = $request->__ticket_id;
        $approval   = $request->status;
        // $remark   = $request->remark;

        //dd($remark);
        $ticket = Ticket::findOrFail($ticket_id);
        if($ticket !=null){
            //dd($pic);
            $ticket->approval = $approval;
            //$ticket->due_date = Carbon::createFromFormat('d/m/Y', $due_date)->format('d/M/Y');
            $ticket->save();
        }

        return response()->json('success');
    }


}
