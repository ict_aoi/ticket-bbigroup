<?php

namespace App\Http\Controllers;
use DB;
use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CekDataController extends Controller
{
    public function index(){
        $ticket = DB::table('tickets')->get();
        foreach ($ticket as $tickett){
            if ($tickett->status == "solved"){
                $waktu = $tickett->resolution_time;
                $carbon_waktu = Carbon::parse($waktu);
                $carbon_waktu->addHours(3);
                $waktu_sekarang = Carbon::now();
                if($carbon_waktu >= $waktu_sekarang){
                    DB::table('tickets')->where('id', $tickett->id)->update([
                        'status' => 'closed'
                    ]);
                }

            }
        }
    }
    public function api(){
        $client = new \GuzzleHttp\Client();
        // $res = $client->get('https://kalenderindonesia.com/api/API8hzKsYbhSu/libur/masehi/2021/7');
        $tahun = Carbon::now();
        $tahun = $tahun->format('Y');
        $tahun_link = 'https://kalenderindonesia.com/api/API8hzKsYbhSu/libur/masehi/'.$tahun;
        // dd($tahun_link);
        // $res = $client->get('https://kalenderindonesia.com/api/API8hzKsYbhSu/libur/masehi/2021');
        $res = $client->get($tahun_link);
        $data = $res->getBody()->getContents();
        // $data2 = explode("{", $data);
        // $result1 = simplexml_load_string($data);
        $data2=json_decode($data);
        $data3 = $data2->data->holiday;
        // dd($data3);
        // dd(count($data3));
        // dd(count($data2->data->holiday->{9}->data));
        // dd($data2->data->holiday->{5});
        // $tanggal =  Carbon::now();
        $data_libur = [];
        DB::table('holiday')->truncate();
        $i = 1;
        for($i=1; $i<13;$i++){
            // dd(count($data2->data->holiday->{$i}->data));
            if($data2->data->holiday->{$i}->count>0){
                for($j=0; $j<$data2->data->holiday->{$i}->count; $j++){
                    $tanggal = $data2->data->holiday->{$i}->data[$j]->date;
                    // dd($tanggal);
                    $tanggal = new DateTime($tanggal);
                    $bulan = $tanggal->format('m');
                    $tahun = $tanggal->format('Y');
                    $date = $tanggal->format('Y-m-d');
                    DB::table('holiday')->insert([
                        'year' => $tahun,
                        'month'=>$bulan,
                        'date' => $date
                    ]);
                    array_push($data_libur,'data ke '.$i);
                    array_push($data_libur,$date);
                    array_push($data_libur,$bulan);
                    array_push($data_libur,$tahun);
                }
            }
        }
        dd($data_libur);
        // dd($data2->data->holiday->{1}->data[0]->date);
        echo $res->getStatusCode(); // 200
        echo $res->getBody(); // { "type": "User", ....
    }

    public function chart(){
        $last6day = Carbon::now()->subDays(6)->format('Y-m-d');
        // dd($last6day);
        $data1 = DB::table('tickets')
                ->where('ticket_category','support')
                ->count();
        $data2 = DB::table('ticket_v')
                ->where('ticket_category','support')
                ->where('last_status', '!=', 'closed')
                ->count();
        $data3 = DB::table('ticket_v')
                ->where ('ticket_category','support')
                ->where('last_status', 'closed')
                ->count();
        $data4 = DB::table('ticket_v')
                ->select(DB::raw('count(ID)'), 'issue_category')
                ->where ('ticket_category','support')
                ->groupBy('issue_category')
                ->get();
        $data5=[];
        $from = Carbon::now()->subday(6)->format('Y-m-d');
        $to = Carbon::now()->format('Y-m-d');
        $data_day = DB::table('ticket_v')
                    ->select(DB::raw('count(created_at)'),\DB::raw('date(created_at)'))
                    ->where('ticket_category','support')
                    ->whereBetween(\DB::raw('date(created_at)'),[$from,$to])
                    ->groupBy(\DB::raw('date(created_at)'))
                    ->get();
        $a = new DateTime($from);
        $b = new DateTime($to);
        $interval = $b->diff($a)->format("%a");
        // disini dikasih for buat input tanggal
        for($i=$interval; $i>=0; $i--) {
            $temp = Carbon::now()->subday($i)->format('Y-m-d');
            $count = 0;
            foreach($data_day as $dataa){
                if($dataa->date==$temp){
                    $count = $dataa->count;
                }
            }
            array_push($data5, [$temp,$count]);
        }
        // dd($data5);
        // dd($data_day);
        // dd($data4);
        $data = [$data1,$data2,$data3,$data4,$data5];
        return response()->json($data);
    }
}
