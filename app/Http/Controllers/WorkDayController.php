<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Priority;
use DB;
use StdClass;
use Validator;

use App\Models\Workday;

use Yajra\Datatables\Datatables;

class WorkDayController extends Controller
{
    public function index(){
        return view('master-data-workday.index');
    }

    public function data(){
        if(request()->ajax()){
            $data = DB::table('workday')
                ->orderBy('id','asc')
                ->get();
            return datatables()->of($data)
            ->editColumn('active', function($data){
                if($data->active == true){
                    return 'Masuk';
                }
                else{
                    return 'Libur';
                }
            })
            ->addColumn('action', function($data) {
                return view('master-data-workday._action', [
                    'model'      => $data,
                    'edit_modal' => route('workDay.edit',$data->id),
                    'delete'     => route('workDay.destroy',$data->id),
                ]);

            })

            ->rawColumns(['active', 'action'])
            ->make(true);
        }
    }

    public function edit($id){
        {
            $workday            = Workday::find($id);
            $obj                = new StdClass();
            $obj->id            = $id;
            $obj->day_name      = $workday->day_name;
            $obj->active        = $workday->active;
            if($obj->active == true){
                $obj->active = 1;
            }else{
                $obj->active = 0;
            }
            $obj->start_hour    = $workday->start_hour;
            $obj->end_hour      = $workday->end_hour;
            $obj->url_update    = route('workDay.update',$workday->id);

            return response()->json($obj,200);
        }
    }

    public function update(Request $request){
        // dd($request);
        // $update = Workday::find($request->id);
        // $update->active = $request->active;
        // $update->start_hour = $request->start_hour;
        // $update->end_hour = $request->end_hour;
        // $update->save();
        DB::table('workday')
            ->where('id',$request->id)
            ->update([
                'active' => $request->active,
                'start_hour' => $request->start_hour,
                'end_hour' => $request->end_hour
            ]);

        return response()->json('success', 200);

    }
}
