<?php namespace App\Http\Controllers;

use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Permission;

class PermissionController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('permission.index');
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Permission::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('permission._action', [
                    'model' => $data,
                    'edit_modal' => route('permission.edit',$data->id),
                    'delete' => route('permission.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Permission::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $permission = Permission::firstorCreate([
            'name' => str_slug($request->name),
            'display_name' => $request->name,
            'description' => $request->description
        ]);
        return response()->json('success', 200);
    }

    public function edit($id)
    {
        $permission = Permission::find($id);
        $obj = new StdClass();
        $obj->id = $id;
        $obj->name = $permission->display_name;
		$obj->description = $permission->description;
		$obj->url_update = route('permission.update',$permission->id);
		
		return response()->json($obj,200);
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Permission::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $permission = Permission::find($id);
        $permission->name = str_slug($request->name);
        $permission->display_name = $request->name;
        $permission->description = $request->description;
        $permission->save();

        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $permission = Permission::findorFail($id)->delete();
        return response()->json(200);
    }
}
