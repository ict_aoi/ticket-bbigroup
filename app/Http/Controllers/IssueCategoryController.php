<?php
namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;

use App\Models\IssueCategory;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class IssueCategoryController extends Controller
{
    public function index()
    {
        return view('master-data-issue-category.index');
    }
    public function data()
    {
        if(request()->ajax())
        {
            $data = DB::table('issue_categories')
                    ->select('issue_categories.id', 'issue_categories.name', 'issue_categories.description', 'issue_categories.type', 'department.dept_name', 'sub_department.sub_dept_name')
                    ->leftjoin('department', 'department.id', 'issue_categories.department')
                    ->leftjoin('sub_department', 'sub_department.id', 'issue_categories.sub_department')
                    ->get();
            // dd($data);
            // $data = IssueCategory::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('master-data-issue-category._action', [
                    'model'      => $data,
                    'edit_modal' => route('issue.edit',$data->id),
                    'delete'     => route('issue.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    public function dataDepartment()
    {
       $department = DB::table('department')->get();
       return response()->json(['department' => $department]);
    }
    public function dataSubDepartment($id)
    {
       $sub_department = DB::table('sub_department')->where('id_dept_name','=',$id)->get();
    //    dd($sub_department);
       return response()->json(['sub_department' => $sub_department]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(IssueCategory::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Category sudah ada, silahkan cari nama category lain.'], 422);

        $permission = IssueCategory::firstorCreate([
            'name'        => $request->name,
            'description' => $request->description,
            'department'  => $request->department,
            'sub_department' =>$request->sub_department,
            'type'        => $request->type
        ]);
        return response()->json('success', 200);
    }

    public function edit($id)

    {
        $issue            = IssueCategory::find($id);
        // dd($issue);
        $obj              = new StdClass();
        $obj->id          = $id;
        $obj->name        = $issue->name;
        $obj->description = $issue->description;
        $obj->department  = $issue->department;
        $obj->sub_department  = $issue->sub_department;
        $obj->type          = $issue->type;
        $obj->url_update  = route('issue.update',$issue->id);
		return response()->json($obj,200);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(IssueCategory::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $permission = IssueCategory::find($id);
        $permission->name = str_slug($request->name);
        $permission->name = $request->name;
        $permission->department = $request->department;
        $permission->department = $request->department;
        $permission->sub_department = $request->sub_department;
        $permission->type = $request->type;
        $permission->save();

        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $permission = IssueCategory::findorFail($id)->delete();
        return response()->json(200);
    }
}
