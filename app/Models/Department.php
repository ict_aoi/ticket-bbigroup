<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;


class Department extends Model
{
    protected $table = 'department';
    protected $guarded = ['id'];
    protected $fillable = ['dept_name','deleted_at', 'unit'];
    protected $dates = ['created_at','deleted_at'];
}
