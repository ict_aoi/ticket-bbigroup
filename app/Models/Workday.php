<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Workday extends Model
{
    protected $table = 'workday';
    protected $guarded = ['id'];
    protected $fillable = ['day_name','active','start_hour', 'end_hour'];
    protected $dates = ['created_at','deleted_at'];
     protected $dateFormat = 'Y-m-d H:i:s';

}
