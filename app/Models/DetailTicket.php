<?php

namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class DetailTicket extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['user_id','comment','remark', 'status', 'progress', 'ticket_id', 'attachment', 'is_admin'];
    protected $dates = ['created_at','deleted_at'];

    public function Ticket()
    {
        return $this->belongsTo('App\Models\Ticket');
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
