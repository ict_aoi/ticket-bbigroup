<?php namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class IssueCategory extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','description','deleted_at', 'department', 'type', 'sub_department'];
    protected $dates = ['created_at','deleted_at'];


}
