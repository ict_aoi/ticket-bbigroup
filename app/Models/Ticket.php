<?php
namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['nik_pegawai','detail_issue_category_id','ticket_category',
    'title','description','attachment','is_confirm_guest','user_confirm_guest',
    'is_confirm_host','user_confirm_host','last_status', 'handled_by','code','name',
    'department','factory', 'priority', 'email_user', 'email_atasan', 'is_admin', 'ip_address', 'approval', 'approval_head', 'due_date', 'target_response' ];
    protected $dates = ['date_confirm_guest','date_confirm_host','created_at','updated_at','deleted_at' ];
}
