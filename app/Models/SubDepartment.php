<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class SubDepartment extends Model
{
    protected $table = 'sub_department';
    protected $guarded = ['id'];
    protected $fillable = ['id_dept_name','deleted_at', 'sub_dept_name'];
    protected $dates = ['created_at','deleted_at'];
}
