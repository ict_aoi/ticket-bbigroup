<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $table = 'priority';
    protected $guarded = ['id'];
    protected $fillable = ['priority','duration_reso', 'unit_reso','deleted_at', 'unit'];
    protected $dates = ['created_at','deleted_at'];

}
