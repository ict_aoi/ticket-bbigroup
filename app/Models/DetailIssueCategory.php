<?php namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class DetailIssueCategory extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['issue_category_id','name','description','deleted_at'];
    protected $dates = ['created_at','deleted_at'];

    public function IssueCategory()
    {
        return $this->belongsTo('App\Models\IssueCategory');
    }
}
