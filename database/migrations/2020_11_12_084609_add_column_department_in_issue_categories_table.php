<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDepartmentInIssueCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('issue_categories', function (Blueprint $table) {
            $table->string('department')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('issue_categories', function (Blueprint $table) {
            $table->dropColumn('department');
        });
    }
}
