<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('nik_pegawai');
            $table->char('detail_issue_category_id',36);
            $table->string('ticket_category');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('attachment')->nullable();
            $table->boolean('is_confirm_guest')->default('false');
            $table->datetime('date_confirm_guest')->nullable();
            $table->string('user_confirm_guest')->nullable();
            $table->boolean('is_confirm_host')->default('false');
            $table->datetime('date_confirm_host')->nullable();
            $table->string('user_confirm_host')->nullable();
            $table->string('last_status')->nullable();
            $table->string('handled_by')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('detail_issue_category_id')->references('id')->on('detail_issue_categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
