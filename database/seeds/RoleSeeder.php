<?php use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $permissions = Permission::select('id')->get();
        $role = new Role();
        $role->name = 'admin_ict';
        $role->display_name = 'admin ICT';
        $role->description  = 'super admin ICT';

        
        if($role->save()){
            $role->attachPermissions($permissions);
        }
    }
}
